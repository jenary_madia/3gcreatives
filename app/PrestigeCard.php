<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PrestigeCard extends Model
{
    protected $table = "prestige_cards";
    protected $guarded = [];

    public function validate($card_no,$status)
    {
        $result = $this->where([
            "card_no" => $card_no,
            "status" => $status
        ])->with('userInfo')->first();

        return $result;
    }

    public function userInfo()
    {
        return $this->hasOne('App\UserPersonalInfo', 'prestige_no_id', 'id')
            ->join('users', 'users.users_personal_info_id', 'users_personal_info.id')
            ->select(
                'users_personal_info.*',
                \DB::raw('TIMESTAMPDIFF(YEAR, users_personal_info.birthdate, CURDATE()) AS age'),
                'users.email'
            );
    }
}
