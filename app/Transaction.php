<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $guarded = [];
    
    public static function generateCodeNumber()
	{
		$date_today = date('Y-m');
		$last_ref_no = Transaction::max("reference_no");
		if(empty($last_ref_no)){
			goto FAILED;
		}
		list($r_year,$r_month,$r_ref) = explode('-',$last_ref_no);
		$r_date = "$r_year-$r_month";
		if ($r_date >= $date_today)
		{
			return "$r_date-".str_pad($r_ref + 1, 5, '0', STR_PAD_LEFT);
		}else{
			FAILED:
			return "$date_today-".str_pad('00000' + 1, 5, '0', STR_PAD_LEFT);
		}

	}
}
