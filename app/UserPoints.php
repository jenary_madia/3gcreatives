<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class UserPoints extends Model
{
    protected $table = "user_points";
    protected $guarded = [];

    public static function addPoints($user_id,$companions,$transaction_type,$points,$additional_points) {
    	$comp_count = count($companions);
    	if(in_array($transaction_type,['IC','DC'])) {
    		if($comp_count >= 4) {
	    		if ($comp_count % 2 == 0) {
	    			$dividend = $comp_count;
	    		}else{
	    			// To able to get the last even number after the cap
	    			for ($i=0; $i < $comp_count; $i++) { 
	    				if ($i % 2 == 0) {
	    					$dividend = $i;
	    				}
	    			}
	    		}
	    		$points = ($dividend / 2 - 1) * $additional_points + $points;
	    	}
    	}
    	UserPoints::where("user_id",$user_id)->update([
    		"total_points" => DB::raw("total_points + $points")
    	]);
    }

    public static function rollbackPoints($user_id,$points) {
    	UserPoints::where("user_id",$user_id)->update([
    		"total_points" => DB::raw("total_points - $points")
    	]);
    }
}
