<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use App\PrestigeCard;
use App\UserPersonalInfo;
use App\UserPoints;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Validator;
use Bestmomo\LaravelEmailConfirmation\Traits\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Crypt;


use DB;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';
    protected $prestigeCard;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(PrestigeCard $prestigeCard)
    {
        $this->prestigeCard = $prestigeCard;
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $prestige_card_id = Crypt::decryptString($data['prestige_card']);

        $prestige_card = PrestigeCard::where([
            'id' => $prestige_card_id,
            'status' => 0
        ]);

        if (! $prestige_card->count()) {
            return Response::json([
                "SUCCESS" => false,
                "MESSAGE" => "Prestige Card already used.",
            ], 200);
        }

        $prestige_card->update([
            "status" => 1
        ]);

        $user_info_id = UserPersonalInfo::create([
            "first_name" => $data['first_name'],
            "middle_name" => $data['middle_name'],
            "last_name" => $data['last_name'],
            "birthdate" => $data['birthdate'],
            "address" => $data['address'],
            "occupation" => $data['occupation'],
            "interest" => $data['interest'],
            "contact_no" => $data['contact_no'],
            "prestige_no_id" => $prestige_card_id
        ])->id;

        UserPoints::create([
            "user_id" => $user_info_id
        ]);

        DB::commit();

        return User::create([
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'role_id' => 4,
            'users_personal_info_id' => $user_info_id,
        ]);
    }

    public function validatePrestigeCard(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'prestige_card_no' => 'required',
        ]);

        if ($validator->fails()) {
            return Response::json([
                'SUCCESS' => false,
                'MESSAGE' => "Please enter your Prestige Card Number.",
            ], 200);
        }

        $prestige_card_no = $request->prestige_card_no;
        $prestige_card = $this->prestigeCard->validate($prestige_card_no);

        if (empty($prestige_card)) {
            return Response::json([
                "SUCCESS" => false,
                "MESSAGE" => "Invalid prestige card number.",
            ], 200);
        }

        return Response::json([
            "SUCCESS" => true,
            "PRESTIGE_CARD" => Crypt::encryptString($prestige_card->id),
            "MESSAGE" => "Your Prestige card is valid.",
        ], 200);
    }

    public function register(Request $request)
    {
        $validation = $this->validator($request->all());

        if ($validation->fails()) {
            return Response::json([
                "SUCCESS" => false,
                "ERRORS" => $validation->errors()->toArray(),
            ], 200);
        } else {
            $user = $this->create($request->all());
            $user->confirmation_code = str_random(30);
            $user->save();

            event(new Registered($user));

            $this->notifyUser($user);

            return Response::json([
                "SUCCESS" => true,
                "MESSAGE" => "Thanks for signing up! Please check your email.",
            ], 200);
        }
    }
}
