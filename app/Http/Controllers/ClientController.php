<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use DB;

class ClientController extends Controller
{
    /**
     * Display a listing clients
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients = User::where('role_id', 4)
            ->join('users_personal_info', 'users.users_personal_info_id', '=', 'users_personal_info.id')
            ->join('user_points', 'users.id', '=', 'user_points.user_id')
            ->join('prestige_cards', 'users_personal_info.prestige_no_id', '=', 'prestige_cards.id')
            ->select(DB::raw('SUM(user_points.total_points) as total_points'),
                'prestige_cards.card_no', 'users_personal_info.created_at',
                DB::raw('CONCAT_WS(SPACE(1), users_personal_info.first_name, users_personal_info.middle_name, users_personal_info.last_name) as full_name')
            )
            ->groupBy('user_points.user_id')
            ->get();

        return view('client.index', compact('clients'));
    }
}
