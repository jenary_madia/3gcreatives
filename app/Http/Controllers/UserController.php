<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use DB;

class UserController extends Controller
{
    public function index()
    {
        return view('user.profile');
    }

    public function list()
    {
        $users = User::join('users_personal_info', 'users_personal_info.id', 'users.users_personal_info_id')
            ->join('roles', 'roles.id', 'users.role_id')
            ->select('users.*', DB::raw("concat(users_personal_info.first_name,' ',users_personal_info.last_name) as full_name"), 'roles.type')
            ->where('users.role_id', '!=', '4')->get();

        return view('user.list', compact('users'));
    }
}
