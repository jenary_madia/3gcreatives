<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\Response;
use App\Http\Controllers\Controller;

class ApiController extends Controller
{
    /**
     * @var int Status Code
     */
    protected $statusCode = 200;

    /**
     * Getter method to return status code
     * 
     * @return mixed
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * Setter method to set status code.
     * It is returning current object
     * for chaining purposes
     * 
     * @param mixed $statuscode
     * @return obj current object
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statuscode;

        return $this;
    }

    /**
     * Function to return a generic response.
     * 
     * @param $data Data to be used in response.
     * @param array $headers Headers to be used in response.
     * @return mixed Return the response.
     */
    public function respond($data, $headers = [])
    {
        return Response::json($data, $this->getStatusCode(), $headers);
    }

    /**
     * Function to return a Not Found response.
     * 
     * @param string $message
     * @return mixed
     */
    public function respondNotFound($message = 'Not Found')
    {
        return $this->setStatusCode(Response::HTTP_NOT_FOUND)->respondWithError($message);
    }

    /**
     * Function to return an error response
     * 
     * @param $message
     * @return mixed
     */
    public function respondWithError($message)
    {
        return $this->respond([
            'error' => [
                'message' => $message,
                'status_code' => $this->getStatusCode()
            ]
        ]);
    }
}
