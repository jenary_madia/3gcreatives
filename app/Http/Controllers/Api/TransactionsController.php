<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\PrestigeCard;
use App\Transaction;
use App\TransactionCompanion;
use App\TransactionType;
use App\UserPoints;
use DB;
use Illuminate\Support\Carbon;
use Response;

class TransactionsController extends Controller
{
    public function addTransaction(Request $request) {
    	DB::beginTransaction();
    	try {
    		list($t_legend,$t_name) = explode("|", $request->transaction_type);
	    	$transaction_type = TransactionType::where([
	    		'legend' => $t_legend,
	    		'active' => 1
	    	])->first();
	    	$client_info = PrestigeCard::where([
	    		"card_no" => $request->prestige_card,
	    		"status" => 1,
	    	])->with('userInfo')->first()->userInfo;
	    	$transaction_id = Transaction::create([
				"transaction_type_id" => $transaction_type->id,
				"reference_no" => Transaction::generateCodeNumber(),
				"client_id" => $client_info->id,
				"agent_id" => null,
				"multiple_day" => $request->multiple_days ? 1 : 0,
				"date_from" => Carbon::parse($request->date_from)->format('Y-m-d'),
				"date_to" => $request->multiple_days ? Carbon::parse($request->date_to)->format('Y-m-d') : null,
				"airline_name" => $request->airline,
				"hotel_name" => $request->hotel,
				"destination" => $request->destination,
				"total_price" => str_replace(',', '', $request->price),
				"total_points" => $transaction_type->corresponding_points,
	    	])->id;
	    	$companions = [];
	    	foreach ($request->companions as $companion) {
	    		array_push($companions,[
					'transaction_id' => $transaction_id,
					'first_name' => $companion['first_name'],
					'middle_name' => $companion['middle_name'],
					'last_name' => $companion['last_name'],
					'age' => $companion['age'],
					'contact_no' => $companion['contact_no'],
					'email' => $companion['email']
	    		]);
	    	};
	    	TransactionCompanion::insert($companions);
	    	UserPoints::addPoints($client_info->id,$request->companions,$t_legend,$transaction_type->corresponding_points,$transaction_type->additional_points);
	    	DB::commit();
            return Response::json([
                'SUCCESS' => true,
                'MESSAGE' => 'Transaction successfully added',
            ], 200);
    	}catch (Exception $e) {
            DB::rollback();
            return Response::json([
                'SUCCESS' => false,
                'MESSAGE' => 'something went wrong',
            ], 200);
            
        }


    }

    public function update(Request $request) {
    	DB::beginTransaction();
    	try {
    		$transaction = Transaction::find($request->transaction_id);

	    	// Rollback
	    	UserPoints::rollbackPoints($transaction->client_id,$transaction->total_points);
	    	//update transaction

	    	list($t_legend,$t_name) = explode("|", $request->transaction_type);
	    	$transaction_type = TransactionType::where([
	    		'legend' => $t_legend,
	    		'active' => 1
	    	])->first();
	    	$transaction_params = [
				"transaction_type_id" => $transaction_type->id,
				"reference_no" => $transaction->reference_no,
				"client_id" => $transaction->client_id,
				"agent_id" => null,
				"multiple_day" => $request->multiple_days ? 1 : 0,
				"date_from" => Carbon::parse($request->date_from)->format('Y-m-d'),
				"date_to" => $request->multiple_days ? Carbon::parse($request->date_to)->format('Y-m-d') : null,
				"airline_name" => $request->airline,
				"hotel_name" => $request->hotel,
				"destination" => $request->destination,
				"total_price" => str_replace(',', '', $request->price),
				"total_points" => $transaction_type->corresponding_points,
	    	];

	    	$transaction->update($transaction_params);

    		//delete&insert new companions
	    	TransactionCompanion::where('transaction_id',$request->transaction_id)->delete();
	    	$companions = [];
	    	foreach ($request->companions as $companion) {
	    		array_push($companions,[
					'transaction_id' => $request->transaction_id,
					'first_name' => $companion['first_name'],
					'middle_name' => $companion['middle_name'],
					'last_name' => $companion['last_name'],
					'age' => $companion['age'],
					'contact_no' => $companion['contact_no'],
					'email' => $companion['email']
	    		]);
	    	};
	    	TransactionCompanion::insert($companions);

	    	//add user points
	    	UserPoints::addPoints($transaction->client_id,$request->companions,$t_legend,$transaction_type->corresponding_points,$transaction_type->additional_points);
	    	DB::commit();
            return Response::json([
                'SUCCESS' => true,
                'MESSAGE' => 'Transaction successfully updated',
            ], 200);
    	}catch (Exception $e) {
            DB::rollback();
            return Response::json([
                'SUCCESS' => false,
                'MESSAGE' => 'something went wrong',
            ], 200);
            
        }
    }

    public function rollback(Request $request) {
    	DB::beginTransaction();
    	try{
    		$transaction = Transaction::where("reference_no",$request->reference_no)->first();
    		$transaction->update([
    			"status" => 5
    		]);
	    	UserPoints::rollbackPoints($transaction->client_id,$transaction->total_points);
	    	DB::commit();
            return Response::json([
                'SUCCESS' => true,
                'MESSAGE' => 'Transaction successfully rollback',
            ], 200);
    	}catch (Exception $e) {
            DB::rollback();
            return Response::json([
                'SUCCESS' => false,
                'MESSAGE' => 'something went wrong',
            ], 200);
            
        }
    }

}
