<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\PrestigeCard;
use App\TransactionCompanion;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Crypt;

class DataController extends Controller
{
    public function __construct(PrestigeCard $prestigeCard)
    {
        $this->prestigeCard = $prestigeCard;
    }

    public function validatePrestigeCard(Request $request)
    {
        $prestige_status = $request->prestige_status;

        $validator = Validator::make($request->all(), [
            'prestige_card_no' => 'required',
        ]);

        if ($validator->fails()) {
            return Response::json([
                'SUCCESS' => false,
                'MESSAGE' => "Please enter your Prestige Card Number.",
            ], 200);
        }

        $prestige_card_no = $request->prestige_card_no;
        $prestige_card = $this->prestigeCard->validate($prestige_card_no,$prestige_status);

        if (empty($prestige_card)) {
            return Response::json([
                "SUCCESS" => false,
                "MESSAGE" => "Invalid prestige card number.",
            ], 200);
        }

        switch ($prestige_status) {
            case 1:
                if($request->action) { //getting existing companions in transaction for edting purposes
                    $companions = TransactionCompanion::where('transaction_id',$request->transaction_id)->get();
                    $response = [
                        "SUCCESS" => true,
                        "DATA" => $companions,
                        "MESSAGE" => "Your Prestige card is valid.",
                    ];
                }else{ // for adding transaction purposes
                    $response = [
                        "SUCCESS" => true,
                        "DATA" => $prestige_card,
                        "MESSAGE" => "Your Prestige card is valid.",
                    ];
                }
                
                break;
            case 0:
                $response = [
                    "SUCCESS" => true,
                    "PRESTIGE_CARD" => Crypt::encryptString($prestige_card->id),
                    "MESSAGE" => "Your Prestige card is valid.",
                ];
            break;
        }

        return Response::json($response, 200);
    }
}
