<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserSaveNewPasswordFormRequest;
use App\Http\Requests\UserUpdateProfileFormRequest;
use App\PrestigeCard;
use App\User;
use App\UserPersonalInfo;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Response;
use App\Http\Requests\AddNewUser;

class UserController extends Controller
{
    public function new(Request $request)
    {
        DB::beginTransaction();

        try {
            $rules = [
                "first_name" => "required",
                "middle_name" => "required",
                "last_name" => "required",
                "contact_no" => "required",
                "email" => "required|unique:users,email|max:255"
            ];
            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return Response::json([
                    'success' => false,
                    'message' => $validator->errors()->first(),
                ], 200);
            }
            $user_info_id = UserPersonalInfo::create([
                "first_name" => $request->first_name,
                "middle_name" => $request->middle_name,
                "last_name" => $request->last_name,
                "contact_no" => $request->contact_no,
            ])->id;

            User::create([
                'email' => $request->email,
                'password' => bcrypt($request->contact_no),
                'role_id' => $request->user_type,
                'confirmed' => 1,
                'users_personal_info_id' => $user_info_id,
            ]);

            DB::commit();
            return Response::json([
                'success' => true,
                'message' => 'User successfully added',
            ], 200);
        } catch (Exception $e) {
            DB::rollback();
            return Response::json([
                'success' => false,
                'message' => 'something went wrong',
            ], 200);
        }
    }

    /**
     * Display user profile
     *
     * @return \Illuminate\Http\Response
     */
    public function getUserProfile()
    {
        $user_personal_info_id = \Auth::user()->users_personal_info_id;

        $user_info = UserPersonalInfo::find($user_personal_info_id);

        $prestige = PrestigeCard::find($user_info->prestige_no_id);

        return Response::json([
            'id' => $user_info->id,
            'address' => $user_info->address,
            'birthdate' => $user_info->birthdate,
            'contact_no' => $user_info->contact_no,
            'created_at' => $user_info->created_at->toDateTimeString(),
            'updated_at' => $user_info->updated_at->toDateTimeString(),
            'first_name' => $user_info->first_name,
            'middle_name' => $user_info->middle_name,
            'last_name' => $user_info->last_name,
            'occupation' => $user_info->occupation,
            'prestige_no_id' => $user_info->prestige_no_id,
            'interest' => $user_info->interest,
            'card_no' => is_null($prestige) ? '' : $prestige->card_no,
        ], 200);
    }

    /**
     * Update user profile info
     *
     * @param  App\Http\Request\UserUpdateProfileFormRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function updateUserProfileInfo(UserUpdateProfileFormRequest $request)
    {
        $request->persist();

        return response()->json([
            'success' => true,
            'message' => 'Profile successfully updated!!!',
        ], 200);
    }

    public function saveNewPassword(UserSaveNewPasswordFormRequest $request)
    {
        $request->user()->update([
            'password' => bcrypt($request->new_password)
        ]);

        return Response::json([
            'success' => true,
            'message' => 'Successfully saved new password',
        ], 200);
    }

    /**
     * Get current password and user inputted match
     */
    public function getCurrentPassword()
    {
        if ($this->isCurrentPasswordMatched()) {
            return Response::json([
                'success' => true,
                'message' => 'Password match'
            ], 200);
        }

        return Response::json([
            'success' => false,
            'message' => 'Current Password does not match'
        ], 200);
    }

    /**
     * Compare current password and user inputted password
     * @return [type] [description]
     */
    private function isCurrentPasswordMatched()
    {
        $hashedPassword = \Auth::user()->password;
        $userInput = request()->current;

        return Hash::check($userInput, $hashedPassword);
    }
}
