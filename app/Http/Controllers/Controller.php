<?php

namespace App\Http\Controllers;

use Auth;
use App\UserPersonalInfo;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $user;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = UserPersonalInfo::leftJoin('user_points','users_personal_info.id','user_points.user_id')->where('users_personal_info.id',Auth::user()->users_personal_info_id)->first();
            view()->share([
                'user_full_name' => $this->getFullName(),
                'role_id' => $this->getRoleID(),
                'total_points' => $this->getTotalPoints(),
            ]);

            return $next($request);
        });
    }

    public function getFullName()
    {
        return sprintf(
            "%s %s",
            $this->user->first_name,
            $this->user->last_name
        );
    }

    private function getRoleID() 
    {
        return Auth::user()->role_id;
    }

    private function getTotalPoints() 
    {
        return Auth::user()->role_id == 4 ? $this->user->total_points : 0;
    }
}
