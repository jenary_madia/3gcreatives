<?php

namespace App\Http\Controllers;

use App\Transaction;
use App\TransactionCompanion;
use App\TransactionType;
use DB;
use Illuminate\Http\Request;
use Auth;

class TransactionsController extends Controller
{
    public function transactions($type = null)
    {
        $transactions = Transaction::join('transaction_types', 'transaction_types.id', 'transactions.transaction_type_id')
        ->join('users_personal_info', 'users_personal_info.id', 'transactions.client_id')
        ->select('transactions.*', 'transaction_types.name as type_name', DB::raw('concat(users_personal_info.last_name," ",users_personal_info.first_name," ",users_personal_info.middle_name) as client_name'))
        ->orderBy('transactions.id', 'DESC');
        if(! $type) {
            $transactions->where("transactions.client_id",Auth::user()->users_personal_info_id);
        }
        $transactions = $transactions->get();

        return view('transaction.index', compact('transactions'));
    }

    public function new()
    {
        $transaction_types = TransactionType::where('active', 1)->get();
        return view('transaction.new', compact('transaction_types'));
    }

    /**
     * Display the specified transaction.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Implementation here
        $companions = TransactionCompanion::where('transaction_id', $id)
            ->select('first_name', 'middle_name', 'last_name', 'age', 'contact_no', 'email')
            ->get();

        $transaction = Transaction::where('transactions.id', $id)
            ->join('users_personal_info', 'users_personal_info.id', '=', 'transactions.client_id')
            ->join('transaction_types', 'transactions.transaction_type_id', '=', 'transaction_types.id')
            ->join('prestige_cards', 'users_personal_info.prestige_no_id', '=', 'prestige_cards.id')
            ->select('card_no', 'name', 'total_price', 'date_from', 'date_to', 'destination', 'airline_name', 'hotel_name')
            ->first();

        return view('transaction.show', compact('companions', 'transaction'));
    }

    public function edit($transaction_id) {
        $transaction = Transaction::where('transactions.id', $transaction_id)
            ->join('users_personal_info', 'users_personal_info.id', '=', 'transactions.client_id')
            ->join('transaction_types', 'transactions.transaction_type_id', '=', 'transaction_types.id')
            ->join('prestige_cards', 'users_personal_info.prestige_no_id', '=', 'prestige_cards.id')
            ->select('multiple_day','transactions.id as transaction_id','legend','card_no', 'name', 'total_price', 'date_from', 'date_to', 'destination', 'airline_name', 'hotel_name')
            ->first();
        $transaction_types = TransactionType::where('active', 1)->get();

        return view('transaction.edit', compact('transaction','transaction_types'));
    }
}
