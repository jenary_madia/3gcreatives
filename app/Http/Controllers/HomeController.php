<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $header = "Be our next guest";
        $subheader = "Plan your next vacation today!";
        return view('landing-page.index', compact('header', 'subheader'));
    }

    public function services()
    {
        $header = "Business Opportunity";
        $subheader = "";
        return view('landing-page.services', compact('header', 'subheader','type'));
    }

    /**
     * Show about page.
     *
     * @return \Illuminate\Http\Response
     */
    public function about()
    {
        $header = "Who we are";
        $subheader = "";
        return view('landing-page.about', compact('header', 'subheader'));
    }

    public function perksRewards()
    {
        $header = "Perks and Rewards";
        $subheader = "";
        return view('landing-page.perks', compact('header', 'subheader'));
    }

    public function faq()
    {
        $header = "Frequently Asked Questions";
        $subheader = "";
        return view('landing-page.faq', compact('header', 'subheader'));
    }

    public function contact_us()
    {
        $header = "Get in touch";
        $subheader = "Message us for you inquiries";
        return view('landing-page.contact', compact('header', 'subheader'));
    }

    public function visaConsultation() {
        $header = "Visa Consultation";
        $subheader = "";
        return view('landing-page.visa-con', compact('header', 'subheader'));
    }

    /**
     * Send contact form
     * @return \Illuminate\Http\Response
     */
    public function contact(Request $request)
    {
        $validator = $this->validate($request, [
            'full_name' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'body_message' => 'required|min:10',
        ]);

        // if ($validator->fails()) {
        //     return response()->json([
        //         'status' => false,
        //         'message' => 'Message required 10 characters'
        //     ]);
        // }

        $full_name = $request->full_name;
        $email = $request->email;
        $phone = $request->phone;
        $body_message = $request->body_message;

        Mail::send('email.contact-form', [
            'full_name' => $full_name,
            'email' => $email,
            'phone' => $phone,
            'body_message' => $body_message
        ], function ($message) use ($email, $full_name) {
            $message->from($email, $full_name);
            $message->to('trigtravelandtoursagency@gmail.com');
        });

        return response()->json([
            'status' => true,
            'message' => 'Message sent successfully!'
        ], 200);
    }
}
