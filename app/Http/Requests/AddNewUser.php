<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;
class AddNewUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "first_name" => "required",
            "middle_name" => "required",
            "last_name" => "required",
            "contact_no" => "required",
            "email" => "required|unique:users,email|max:255"
        ];
    }

    public function messages()
    {
        return [
            "first_name.size" => "first name is required"
        ];
    }

    public function response(array $errors)
    {
        return Response::create([
            'success' => false,
            'message' => 'something went wrong',
            "errors" =>$errors,
        ], 200);
    }
}
