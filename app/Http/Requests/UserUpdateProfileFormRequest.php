<?php

namespace App\Http\Requests;

use App\UserPersonalInfo;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class UserUpdateProfileFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required',
            'middle_name' => 'required',
            'last_name' => 'required',
            'birthdate' => 'required',
            'address' => 'required'
        ];
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator  $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'success' => false,
            'message' => 'Fields with * are required'
        ], 200));
    }

    public function persist()
    {
        $id = \Auth::user()->users_personal_info_id;

        $userPersonalInfo = UserPersonalInfo::find($id);
        $userPersonalInfo->first_name = request()->first_name;
        $userPersonalInfo->middle_name = request()->middle_name;
        $userPersonalInfo->last_name = request()->last_name;
        $userPersonalInfo->contact_no = request()->contact_no;
        $userPersonalInfo->occupation = request()->occupation;
        $userPersonalInfo->birthdate = request()->birthdate;
        $userPersonalInfo->interest = request()->interest;
        $userPersonalInfo->address = request()->address;
        $userPersonalInfo->save();
    }
}
