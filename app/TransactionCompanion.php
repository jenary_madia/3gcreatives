<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionCompanion extends Model
{
	protected $table = 'transaction_companions';
    protected $guarded = [];
}
