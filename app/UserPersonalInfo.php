<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPersonalInfo extends Model
{
    protected $table = "users_personal_info";
    protected $guarded = [];
}
