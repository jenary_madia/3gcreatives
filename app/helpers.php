<?php

function set_active($path, $class = 'active open')
{
    return Request::is($path) ? $class : '';
}
