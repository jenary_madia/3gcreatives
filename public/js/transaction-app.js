/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 187);
/******/ })
/************************************************************************/
/******/ ({

/***/ 10:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(157);
if(typeof content === 'string') content = [[module.i, content, '']];
// Prepare cssTransformation
var transform;

var options = {}
options.transform = transform
// add the styles to the DOM
var update = __webpack_require__(155)(content, options);
if(content.locals) module.exports = content.locals;
// Hot Module Replacement
if(false) {
	// When the styles change, update the <style> tags
	if(!content.locals) {
		module.hot.accept("!!../../css-loader/index.js!./vuejs-noty.css", function() {
			var newContent = require("!!../../css-loader/index.js!./vuejs-noty.css");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
	}
	// When the module is disposed, remove the <style> tags
	module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 154:
/***/ (function(module, exports) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/
// css base code, injected by the css-loader
module.exports = function(useSourceMap) {
	var list = [];

	// return the list of modules as css string
	list.toString = function toString() {
		return this.map(function (item) {
			var content = cssWithMappingToString(item, useSourceMap);
			if(item[2]) {
				return "@media " + item[2] + "{" + content + "}";
			} else {
				return content;
			}
		}).join("");
	};

	// import a list of modules into the list
	list.i = function(modules, mediaQuery) {
		if(typeof modules === "string")
			modules = [[null, modules, ""]];
		var alreadyImportedModules = {};
		for(var i = 0; i < this.length; i++) {
			var id = this[i][0];
			if(typeof id === "number")
				alreadyImportedModules[id] = true;
		}
		for(i = 0; i < modules.length; i++) {
			var item = modules[i];
			// skip already imported module
			// this implementation is not 100% perfect for weird media query combinations
			//  when a module is imported multiple times with different media queries.
			//  I hope this will never occur (Hey this way we have smaller bundles)
			if(typeof item[0] !== "number" || !alreadyImportedModules[item[0]]) {
				if(mediaQuery && !item[2]) {
					item[2] = mediaQuery;
				} else if(mediaQuery) {
					item[2] = "(" + item[2] + ") and (" + mediaQuery + ")";
				}
				list.push(item);
			}
		}
	};
	return list;
};

function cssWithMappingToString(item, useSourceMap) {
	var content = item[1] || '';
	var cssMapping = item[3];
	if (!cssMapping) {
		return content;
	}

	if (useSourceMap && typeof btoa === 'function') {
		var sourceMapping = toComment(cssMapping);
		var sourceURLs = cssMapping.sources.map(function (source) {
			return '/*# sourceURL=' + cssMapping.sourceRoot + source + ' */'
		});

		return [content].concat(sourceURLs).concat([sourceMapping]).join('\n');
	}

	return [content].join('\n');
}

// Adapted from convert-source-map (MIT)
function toComment(sourceMap) {
	// eslint-disable-next-line no-undef
	var base64 = btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap))));
	var data = 'sourceMappingURL=data:application/json;charset=utf-8;base64,' + base64;

	return '/*# ' + data + ' */';
}


/***/ }),

/***/ 155:
/***/ (function(module, exports, __webpack_require__) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/

var stylesInDom = {};

var	memoize = function (fn) {
	var memo;

	return function () {
		if (typeof memo === "undefined") memo = fn.apply(this, arguments);
		return memo;
	};
};

var isOldIE = memoize(function () {
	// Test for IE <= 9 as proposed by Browserhacks
	// @see http://browserhacks.com/#hack-e71d8692f65334173fee715c222cb805
	// Tests for existence of standard globals is to allow style-loader
	// to operate correctly into non-standard environments
	// @see https://github.com/webpack-contrib/style-loader/issues/177
	return window && document && document.all && !window.atob;
});

var getElement = (function (fn) {
	var memo = {};

	return function(selector) {
		if (typeof memo[selector] === "undefined") {
			memo[selector] = fn.call(this, selector);
		}

		return memo[selector]
	};
})(function (target) {
	return document.querySelector(target)
});

var singleton = null;
var	singletonCounter = 0;
var	stylesInsertedAtTop = [];

var	fixUrls = __webpack_require__(156);

module.exports = function(list, options) {
	if (typeof DEBUG !== "undefined" && DEBUG) {
		if (typeof document !== "object") throw new Error("The style-loader cannot be used in a non-browser environment");
	}

	options = options || {};

	options.attrs = typeof options.attrs === "object" ? options.attrs : {};

	// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
	// tags it will allow on a page
	if (!options.singleton) options.singleton = isOldIE();

	// By default, add <style> tags to the <head> element
	if (!options.insertInto) options.insertInto = "head";

	// By default, add <style> tags to the bottom of the target
	if (!options.insertAt) options.insertAt = "bottom";

	var styles = listToStyles(list, options);

	addStylesToDom(styles, options);

	return function update (newList) {
		var mayRemove = [];

		for (var i = 0; i < styles.length; i++) {
			var item = styles[i];
			var domStyle = stylesInDom[item.id];

			domStyle.refs--;
			mayRemove.push(domStyle);
		}

		if(newList) {
			var newStyles = listToStyles(newList, options);
			addStylesToDom(newStyles, options);
		}

		for (var i = 0; i < mayRemove.length; i++) {
			var domStyle = mayRemove[i];

			if(domStyle.refs === 0) {
				for (var j = 0; j < domStyle.parts.length; j++) domStyle.parts[j]();

				delete stylesInDom[domStyle.id];
			}
		}
	};
};

function addStylesToDom (styles, options) {
	for (var i = 0; i < styles.length; i++) {
		var item = styles[i];
		var domStyle = stylesInDom[item.id];

		if(domStyle) {
			domStyle.refs++;

			for(var j = 0; j < domStyle.parts.length; j++) {
				domStyle.parts[j](item.parts[j]);
			}

			for(; j < item.parts.length; j++) {
				domStyle.parts.push(addStyle(item.parts[j], options));
			}
		} else {
			var parts = [];

			for(var j = 0; j < item.parts.length; j++) {
				parts.push(addStyle(item.parts[j], options));
			}

			stylesInDom[item.id] = {id: item.id, refs: 1, parts: parts};
		}
	}
}

function listToStyles (list, options) {
	var styles = [];
	var newStyles = {};

	for (var i = 0; i < list.length; i++) {
		var item = list[i];
		var id = options.base ? item[0] + options.base : item[0];
		var css = item[1];
		var media = item[2];
		var sourceMap = item[3];
		var part = {css: css, media: media, sourceMap: sourceMap};

		if(!newStyles[id]) styles.push(newStyles[id] = {id: id, parts: [part]});
		else newStyles[id].parts.push(part);
	}

	return styles;
}

function insertStyleElement (options, style) {
	var target = getElement(options.insertInto)

	if (!target) {
		throw new Error("Couldn't find a style target. This probably means that the value for the 'insertInto' parameter is invalid.");
	}

	var lastStyleElementInsertedAtTop = stylesInsertedAtTop[stylesInsertedAtTop.length - 1];

	if (options.insertAt === "top") {
		if (!lastStyleElementInsertedAtTop) {
			target.insertBefore(style, target.firstChild);
		} else if (lastStyleElementInsertedAtTop.nextSibling) {
			target.insertBefore(style, lastStyleElementInsertedAtTop.nextSibling);
		} else {
			target.appendChild(style);
		}
		stylesInsertedAtTop.push(style);
	} else if (options.insertAt === "bottom") {
		target.appendChild(style);
	} else {
		throw new Error("Invalid value for parameter 'insertAt'. Must be 'top' or 'bottom'.");
	}
}

function removeStyleElement (style) {
	if (style.parentNode === null) return false;
	style.parentNode.removeChild(style);

	var idx = stylesInsertedAtTop.indexOf(style);
	if(idx >= 0) {
		stylesInsertedAtTop.splice(idx, 1);
	}
}

function createStyleElement (options) {
	var style = document.createElement("style");

	options.attrs.type = "text/css";

	addAttrs(style, options.attrs);
	insertStyleElement(options, style);

	return style;
}

function createLinkElement (options) {
	var link = document.createElement("link");

	options.attrs.type = "text/css";
	options.attrs.rel = "stylesheet";

	addAttrs(link, options.attrs);
	insertStyleElement(options, link);

	return link;
}

function addAttrs (el, attrs) {
	Object.keys(attrs).forEach(function (key) {
		el.setAttribute(key, attrs[key]);
	});
}

function addStyle (obj, options) {
	var style, update, remove, result;

	// If a transform function was defined, run it on the css
	if (options.transform && obj.css) {
	    result = options.transform(obj.css);

	    if (result) {
	    	// If transform returns a value, use that instead of the original css.
	    	// This allows running runtime transformations on the css.
	    	obj.css = result;
	    } else {
	    	// If the transform function returns a falsy value, don't add this css.
	    	// This allows conditional loading of css
	    	return function() {
	    		// noop
	    	};
	    }
	}

	if (options.singleton) {
		var styleIndex = singletonCounter++;

		style = singleton || (singleton = createStyleElement(options));

		update = applyToSingletonTag.bind(null, style, styleIndex, false);
		remove = applyToSingletonTag.bind(null, style, styleIndex, true);

	} else if (
		obj.sourceMap &&
		typeof URL === "function" &&
		typeof URL.createObjectURL === "function" &&
		typeof URL.revokeObjectURL === "function" &&
		typeof Blob === "function" &&
		typeof btoa === "function"
	) {
		style = createLinkElement(options);
		update = updateLink.bind(null, style, options);
		remove = function () {
			removeStyleElement(style);

			if(style.href) URL.revokeObjectURL(style.href);
		};
	} else {
		style = createStyleElement(options);
		update = applyToTag.bind(null, style);
		remove = function () {
			removeStyleElement(style);
		};
	}

	update(obj);

	return function updateStyle (newObj) {
		if (newObj) {
			if (
				newObj.css === obj.css &&
				newObj.media === obj.media &&
				newObj.sourceMap === obj.sourceMap
			) {
				return;
			}

			update(obj = newObj);
		} else {
			remove();
		}
	};
}

var replaceText = (function () {
	var textStore = [];

	return function (index, replacement) {
		textStore[index] = replacement;

		return textStore.filter(Boolean).join('\n');
	};
})();

function applyToSingletonTag (style, index, remove, obj) {
	var css = remove ? "" : obj.css;

	if (style.styleSheet) {
		style.styleSheet.cssText = replaceText(index, css);
	} else {
		var cssNode = document.createTextNode(css);
		var childNodes = style.childNodes;

		if (childNodes[index]) style.removeChild(childNodes[index]);

		if (childNodes.length) {
			style.insertBefore(cssNode, childNodes[index]);
		} else {
			style.appendChild(cssNode);
		}
	}
}

function applyToTag (style, obj) {
	var css = obj.css;
	var media = obj.media;

	if(media) {
		style.setAttribute("media", media)
	}

	if(style.styleSheet) {
		style.styleSheet.cssText = css;
	} else {
		while(style.firstChild) {
			style.removeChild(style.firstChild);
		}

		style.appendChild(document.createTextNode(css));
	}
}

function updateLink (link, options, obj) {
	var css = obj.css;
	var sourceMap = obj.sourceMap;

	/*
		If convertToAbsoluteUrls isn't defined, but sourcemaps are enabled
		and there is no publicPath defined then lets turn convertToAbsoluteUrls
		on by default.  Otherwise default to the convertToAbsoluteUrls option
		directly
	*/
	var autoFixUrls = options.convertToAbsoluteUrls === undefined && sourceMap;

	if (options.convertToAbsoluteUrls || autoFixUrls) {
		css = fixUrls(css);
	}

	if (sourceMap) {
		// http://stackoverflow.com/a/26603875
		css += "\n/*# sourceMappingURL=data:application/json;base64," + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + " */";
	}

	var blob = new Blob([css], { type: "text/css" });

	var oldSrc = link.href;

	link.href = URL.createObjectURL(blob);

	if(oldSrc) URL.revokeObjectURL(oldSrc);
}


/***/ }),

/***/ 156:
/***/ (function(module, exports) {


/**
 * When source maps are enabled, `style-loader` uses a link element with a data-uri to
 * embed the css on the page. This breaks all relative urls because now they are relative to a
 * bundle instead of the current page.
 *
 * One solution is to only use full urls, but that may be impossible.
 *
 * Instead, this function "fixes" the relative urls to be absolute according to the current page location.
 *
 * A rudimentary test suite is located at `test/fixUrls.js` and can be run via the `npm test` command.
 *
 */

module.exports = function (css) {
  // get current location
  var location = typeof window !== "undefined" && window.location;

  if (!location) {
    throw new Error("fixUrls requires window.location");
  }

	// blank or null?
	if (!css || typeof css !== "string") {
	  return css;
  }

  var baseUrl = location.protocol + "//" + location.host;
  var currentDir = baseUrl + location.pathname.replace(/\/[^\/]*$/, "/");

	// convert each url(...)
	/*
	This regular expression is just a way to recursively match brackets within
	a string.

	 /url\s*\(  = Match on the word "url" with any whitespace after it and then a parens
	   (  = Start a capturing group
	     (?:  = Start a non-capturing group
	         [^)(]  = Match anything that isn't a parentheses
	         |  = OR
	         \(  = Match a start parentheses
	             (?:  = Start another non-capturing groups
	                 [^)(]+  = Match anything that isn't a parentheses
	                 |  = OR
	                 \(  = Match a start parentheses
	                     [^)(]*  = Match anything that isn't a parentheses
	                 \)  = Match a end parentheses
	             )  = End Group
              *\) = Match anything and then a close parens
          )  = Close non-capturing group
          *  = Match anything
       )  = Close capturing group
	 \)  = Match a close parens

	 /gi  = Get all matches, not the first.  Be case insensitive.
	 */
	var fixedCss = css.replace(/url\s*\(((?:[^)(]|\((?:[^)(]+|\([^)(]*\))*\))*)\)/gi, function(fullMatch, origUrl) {
		// strip quotes (if they exist)
		var unquotedOrigUrl = origUrl
			.trim()
			.replace(/^"(.*)"$/, function(o, $1){ return $1; })
			.replace(/^'(.*)'$/, function(o, $1){ return $1; });

		// already a full url? no change
		if (/^(#|data:|http:\/\/|https:\/\/|file:\/\/\/)/i.test(unquotedOrigUrl)) {
		  return fullMatch;
		}

		// convert the url to a full url
		var newUrl;

		if (unquotedOrigUrl.indexOf("//") === 0) {
		  	//TODO: should we add protocol?
			newUrl = unquotedOrigUrl;
		} else if (unquotedOrigUrl.indexOf("/") === 0) {
			// path should be relative to the base url
			newUrl = baseUrl + unquotedOrigUrl; // already starts with '/'
		} else {
			// path should be relative to current directory
			newUrl = currentDir + unquotedOrigUrl.replace(/^\.\//, ""); // Strip leading './'
		}

		// send back the fixed url(...)
		return "url(" + JSON.stringify(newUrl) + ")";
	});

	// send back the fixed css
	return fixedCss;
};


/***/ }),

/***/ 157:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(154)(undefined);
// imports


// module
exports.push([module.i, ".noty_layout_mixin, #noty_layout__top, #noty_layout__topLeft, #noty_layout__topCenter, #noty_layout__topRight, #noty_layout__bottom, #noty_layout__bottomLeft, #noty_layout__bottomCenter, #noty_layout__bottomRight, #noty_layout__center, #noty_layout__centerLeft, #noty_layout__centerRight {\n  position: fixed;\n  margin: 0;\n  padding: 0;\n  z-index: 9999999;\n  -webkit-transform: translateZ(0) scale(1, 1);\n          transform: translateZ(0) scale(1, 1);\n  -webkit-backface-visibility: hidden;\n          backface-visibility: hidden;\n  -webkit-font-smoothing: subpixel-antialiased;\n  filter: blur(0);\n  -webkit-filter: blur(0);\n  max-width: 90%; }\n\n#noty_layout__top {\n  top: 0;\n  left: 5%;\n  width: 90%; }\n\n#noty_layout__topLeft {\n  top: 20px;\n  left: 20px;\n  width: 325px; }\n\n#noty_layout__topCenter {\n  top: 5%;\n  left: 50%;\n  width: 325px;\n  -webkit-transform: translate(-webkit-calc(-50% - .5px)) translateZ(0) scale(1, 1);\n          transform: translate(calc(-50% - .5px)) translateZ(0) scale(1, 1); }\n\n#noty_layout__topRight {\n  top: 20px;\n  right: 20px;\n  width: 325px; }\n\n#noty_layout__bottom {\n  bottom: 0;\n  left: 5%;\n  width: 90%; }\n\n#noty_layout__bottomLeft {\n  bottom: 20px;\n  left: 20px;\n  width: 325px; }\n\n#noty_layout__bottomCenter {\n  bottom: 5%;\n  left: 50%;\n  width: 325px;\n  -webkit-transform: translate(-webkit-calc(-50% - .5px)) translateZ(0) scale(1, 1);\n          transform: translate(calc(-50% - .5px)) translateZ(0) scale(1, 1); }\n\n#noty_layout__bottomRight {\n  bottom: 20px;\n  right: 20px;\n  width: 325px; }\n\n#noty_layout__center {\n  top: 50%;\n  left: 50%;\n  width: 325px;\n  -webkit-transform: translate(-webkit-calc(-50% - .5px), -webkit-calc(-50% - .5px)) translateZ(0) scale(1, 1);\n          transform: translate(calc(-50% - .5px), calc(-50% - .5px)) translateZ(0) scale(1, 1); }\n\n#noty_layout__centerLeft {\n  top: 50%;\n  left: 20px;\n  width: 325px;\n  -webkit-transform: translate(0, -webkit-calc(-50% - .5px)) translateZ(0) scale(1, 1);\n          transform: translate(0, calc(-50% - .5px)) translateZ(0) scale(1, 1); }\n\n#noty_layout__centerRight {\n  top: 50%;\n  right: 20px;\n  width: 325px;\n  -webkit-transform: translate(0, -webkit-calc(-50% - .5px)) translateZ(0) scale(1, 1);\n          transform: translate(0, calc(-50% - .5px)) translateZ(0) scale(1, 1); }\n\n.noty_progressbar {\n  display: none; }\n\n.noty_has_timeout .noty_progressbar {\n  display: block;\n  position: absolute;\n  left: 0;\n  bottom: 0;\n  height: 3px;\n  width: 100%;\n  background-color: #646464;\n  opacity: 0.2;\n  filter: alpha(opacity=10); }\n\n.noty_bar {\n  -webkit-backface-visibility: hidden;\n  -webkit-transform: translate(0, 0) translateZ(0) scale(1, 1);\n  -ms-transform: translate(0, 0) scale(1, 1);\n      transform: translate(0, 0) scale(1, 1);\n  -webkit-font-smoothing: subpixel-antialiased;\n  overflow: hidden; }\n\n.noty_effects_open {\n  opacity: 0;\n  -webkit-transform: translate(50%);\n      -ms-transform: translate(50%);\n          transform: translate(50%);\n  -webkit-animation: noty_anim_in 0.5s cubic-bezier(0.68, -0.55, 0.265, 1.55);\n          animation: noty_anim_in 0.5s cubic-bezier(0.68, -0.55, 0.265, 1.55);\n  -webkit-animation-fill-mode: forwards;\n          animation-fill-mode: forwards; }\n\n.noty_effects_close {\n  -webkit-animation: noty_anim_out 0.5s cubic-bezier(0.68, -0.55, 0.265, 1.55);\n          animation: noty_anim_out 0.5s cubic-bezier(0.68, -0.55, 0.265, 1.55);\n  -webkit-animation-fill-mode: forwards;\n          animation-fill-mode: forwards; }\n\n.noty_fix_effects_height {\n  -webkit-animation: noty_anim_height 75ms ease-out;\n          animation: noty_anim_height 75ms ease-out; }\n\n.noty_close_with_click {\n  cursor: pointer; }\n\n.noty_close_button {\n  position: absolute;\n  top: 2px;\n  right: 2px;\n  font-weight: bold;\n  width: 20px;\n  height: 20px;\n  text-align: center;\n  line-height: 20px;\n  background-color: rgba(0, 0, 0, 0.05);\n  border-radius: 2px;\n  cursor: pointer;\n  -webkit-transition: all .2s ease-out;\n  transition: all .2s ease-out; }\n\n.noty_close_button:hover {\n  background-color: rgba(0, 0, 0, 0.1); }\n\n.noty_modal {\n  position: fixed;\n  width: 100%;\n  height: 100%;\n  background-color: #000;\n  z-index: 10000;\n  opacity: .3;\n  left: 0;\n  top: 0; }\n\n.noty_modal.noty_modal_open {\n  opacity: 0;\n  -webkit-animation: noty_modal_in .3s ease-out;\n          animation: noty_modal_in .3s ease-out; }\n\n.noty_modal.noty_modal_close {\n  -webkit-animation: noty_modal_out .3s ease-out;\n          animation: noty_modal_out .3s ease-out;\n  -webkit-animation-fill-mode: forwards;\n          animation-fill-mode: forwards; }\n\n@-webkit-keyframes noty_modal_in {\n  100% {\n    opacity: .3; } }\n\n@keyframes noty_modal_in {\n  100% {\n    opacity: .3; } }\n\n@-webkit-keyframes noty_modal_out {\n  100% {\n    opacity: 0; } }\n\n@keyframes noty_modal_out {\n  100% {\n    opacity: 0; } }\n\n@keyframes noty_modal_out {\n  100% {\n    opacity: 0; } }\n\n@-webkit-keyframes noty_anim_in {\n  100% {\n    -webkit-transform: translate(0);\n            transform: translate(0);\n    opacity: 1; } }\n\n@keyframes noty_anim_in {\n  100% {\n    -webkit-transform: translate(0);\n            transform: translate(0);\n    opacity: 1; } }\n\n@-webkit-keyframes noty_anim_out {\n  100% {\n    -webkit-transform: translate(50%);\n            transform: translate(50%);\n    opacity: 0; } }\n\n@keyframes noty_anim_out {\n  100% {\n    -webkit-transform: translate(50%);\n            transform: translate(50%);\n    opacity: 0; } }\n\n@-webkit-keyframes noty_anim_height {\n  100% {\n    height: 0; } }\n\n@keyframes noty_anim_height {\n  100% {\n    height: 0; } }\n\n.noty_theme__relax.noty_bar {\n  margin: 4px 0;\n  overflow: hidden;\n  border-radius: 2px;\n  position: relative; }\n  .noty_theme__relax.noty_bar .noty_body {\n    padding: 10px; }\n  .noty_theme__relax.noty_bar .noty_buttons {\n    border-top: 1px solid #e7e7e7;\n    padding: 5px 10px; }\n\n.noty_theme__relax.noty_type__alert,\n.noty_theme__relax.noty_type__notification {\n  background-color: #fff;\n  border: 1px solid #dedede;\n  color: #444; }\n\n.noty_theme__relax.noty_type__warning {\n  background-color: #FFEAA8;\n  border: 1px solid #FFC237;\n  color: #826200; }\n  .noty_theme__relax.noty_type__warning .noty_buttons {\n    border-color: #dfaa30; }\n\n.noty_theme__relax.noty_type__error {\n  background-color: #FF8181;\n  border: 1px solid #e25353;\n  color: #FFF; }\n  .noty_theme__relax.noty_type__error .noty_buttons {\n    border-color: darkred; }\n\n.noty_theme__relax.noty_type__info,\n.noty_theme__relax.noty_type__information {\n  background-color: #78C5E7;\n  border: 1px solid #3badd6;\n  color: #FFF; }\n  .noty_theme__relax.noty_type__info .noty_buttons,\n  .noty_theme__relax.noty_type__information .noty_buttons {\n    border-color: #0B90C4; }\n\n.noty_theme__relax.noty_type__success {\n  background-color: #BCF5BC;\n  border: 1px solid #7cdd77;\n  color: darkgreen; }\n  .noty_theme__relax.noty_type__success .noty_buttons {\n    border-color: #50C24E; }\n\n.noty_theme__metroui.noty_bar {\n  margin: 4px 0;\n  overflow: hidden;\n  position: relative;\n  box-shadow: rgba(0, 0, 0, 0.298039) 0 0 5px 0; }\n  .noty_theme__metroui.noty_bar .noty_progressbar {\n    position: absolute;\n    left: 0;\n    bottom: 0;\n    height: 3px;\n    width: 100%;\n    background-color: #000;\n    opacity: 0.2;\n    filter: alpha(opacity=20); }\n  .noty_theme__metroui.noty_bar .noty_body {\n    padding: 1.25em;\n    font-size: 14px; }\n  .noty_theme__metroui.noty_bar .noty_buttons {\n    padding: 0 10px .5em 10px; }\n\n.noty_theme__metroui.noty_type__alert,\n.noty_theme__metroui.noty_type__notification {\n  background-color: #fff;\n  color: #1d1d1d; }\n\n.noty_theme__metroui.noty_type__warning {\n  background-color: #FA6800;\n  color: #fff; }\n\n.noty_theme__metroui.noty_type__error {\n  background-color: #CE352C;\n  color: #FFF; }\n\n.noty_theme__metroui.noty_type__info,\n.noty_theme__metroui.noty_type__information {\n  background-color: #1BA1E2;\n  color: #FFF; }\n\n.noty_theme__metroui.noty_type__success {\n  background-color: #60A917;\n  color: #fff; }\n\n.noty_theme__mint.noty_bar {\n  margin: 4px 0;\n  overflow: hidden;\n  border-radius: 2px;\n  position: relative; }\n  .noty_theme__mint.noty_bar .noty_body {\n    padding: 10px;\n    font-size: 14px; }\n  .noty_theme__mint.noty_bar .noty_buttons {\n    padding: 10px; }\n\n.noty_theme__mint.noty_type__alert,\n.noty_theme__mint.noty_type__notification {\n  background-color: #fff;\n  border-bottom: 1px solid #D1D1D1;\n  color: #2F2F2F; }\n\n.noty_theme__mint.noty_type__warning {\n  background-color: #FFAE42;\n  border-bottom: 1px solid #E89F3C;\n  color: #fff; }\n\n.noty_theme__mint.noty_type__error {\n  background-color: #DE636F;\n  border-bottom: 1px solid #CA5A65;\n  color: #fff; }\n\n.noty_theme__mint.noty_type__info,\n.noty_theme__mint.noty_type__information {\n  background-color: #7F7EFF;\n  border-bottom: 1px solid #7473E8;\n  color: #fff; }\n\n.noty_theme__mint.noty_type__success {\n  background-color: #AFC765;\n  border-bottom: 1px solid #A0B55C;\n  color: #fff; }\n\n.noty_theme__sunset.noty_bar {\n  margin: 4px 0;\n  overflow: hidden;\n  border-radius: 2px;\n  position: relative; }\n  .noty_theme__sunset.noty_bar .noty_body {\n    padding: 10px;\n    font-size: 14px;\n    text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.1); }\n  .noty_theme__sunset.noty_bar .noty_buttons {\n    padding: 10px; }\n\n.noty_theme__sunset.noty_type__alert,\n.noty_theme__sunset.noty_type__notification {\n  background-color: #073B4C;\n  color: #fff; }\n  .noty_theme__sunset.noty_type__alert .noty_progressbar,\n  .noty_theme__sunset.noty_type__notification .noty_progressbar {\n    background-color: #fff; }\n\n.noty_theme__sunset.noty_type__warning {\n  background-color: #FFD166;\n  color: #fff; }\n\n.noty_theme__sunset.noty_type__error {\n  background-color: #EF476F;\n  color: #fff; }\n  .noty_theme__sunset.noty_type__error .noty_progressbar {\n    opacity: .4; }\n\n.noty_theme__sunset.noty_type__info,\n.noty_theme__sunset.noty_type__information {\n  background-color: #118AB2;\n  color: #fff; }\n  .noty_theme__sunset.noty_type__info .noty_progressbar,\n  .noty_theme__sunset.noty_type__information .noty_progressbar {\n    opacity: .6; }\n\n.noty_theme__sunset.noty_type__success {\n  background-color: #06D6A0;\n  color: #fff; }\n\n.noty_theme__bootstrap-v3.noty_bar {\n  margin: 4px 0;\n  overflow: hidden;\n  position: relative;\n  border: 1px solid transparent;\n  border-radius: 4px; }\n  .noty_theme__bootstrap-v3.noty_bar .noty_body {\n    padding: 15px; }\n  .noty_theme__bootstrap-v3.noty_bar .noty_buttons {\n    padding: 10px; }\n  .noty_theme__bootstrap-v3.noty_bar .noty_close_button {\n    font-size: 21px;\n    font-weight: 700;\n    line-height: 1;\n    color: #000;\n    text-shadow: 0 1px 0 #fff;\n    filter: alpha(opacity=20);\n    opacity: .2;\n    background: transparent; }\n  .noty_theme__bootstrap-v3.noty_bar .noty_close_button:hover {\n    background: transparent;\n    text-decoration: none;\n    cursor: pointer;\n    filter: alpha(opacity=50);\n    opacity: .5; }\n\n.noty_theme__bootstrap-v3.noty_type__alert,\n.noty_theme__bootstrap-v3.noty_type__notification {\n  background-color: #fff;\n  color: inherit; }\n\n.noty_theme__bootstrap-v3.noty_type__warning {\n  background-color: #fcf8e3;\n  color: #8a6d3b;\n  border-color: #faebcc; }\n\n.noty_theme__bootstrap-v3.noty_type__error {\n  background-color: #f2dede;\n  color: #a94442;\n  border-color: #ebccd1; }\n\n.noty_theme__bootstrap-v3.noty_type__info,\n.noty_theme__bootstrap-v3.noty_type__information {\n  background-color: #d9edf7;\n  color: #31708f;\n  border-color: #bce8f1; }\n\n.noty_theme__bootstrap-v3.noty_type__success {\n  background-color: #dff0d8;\n  color: #3c763d;\n  border-color: #d6e9c6; }\n\n.noty_theme__bootstrap-v4.noty_bar {\n  margin: 4px 0;\n  overflow: hidden;\n  position: relative;\n  border: 1px solid transparent;\n  border-radius: .25rem; }\n  .noty_theme__bootstrap-v4.noty_bar .noty_body {\n    padding: .75rem 1.25rem; }\n  .noty_theme__bootstrap-v4.noty_bar .noty_buttons {\n    padding: 10px; }\n  .noty_theme__bootstrap-v4.noty_bar .noty_close_button {\n    font-size: 1.5rem;\n    font-weight: 700;\n    line-height: 1;\n    color: #000;\n    text-shadow: 0 1px 0 #fff;\n    filter: alpha(opacity=20);\n    opacity: .5;\n    background: transparent; }\n  .noty_theme__bootstrap-v4.noty_bar .noty_close_button:hover {\n    background: transparent;\n    text-decoration: none;\n    cursor: pointer;\n    filter: alpha(opacity=50);\n    opacity: .75; }\n\n.noty_theme__bootstrap-v4.noty_type__alert,\n.noty_theme__bootstrap-v4.noty_type__notification {\n  background-color: #fff;\n  color: inherit; }\n\n.noty_theme__bootstrap-v4.noty_type__warning {\n  background-color: #fcf8e3;\n  color: #8a6d3b;\n  border-color: #faebcc; }\n\n.noty_theme__bootstrap-v4.noty_type__error {\n  background-color: #f2dede;\n  color: #a94442;\n  border-color: #ebccd1; }\n\n.noty_theme__bootstrap-v4.noty_type__info,\n.noty_theme__bootstrap-v4.noty_type__information {\n  background-color: #d9edf7;\n  color: #31708f;\n  border-color: #bce8f1; }\n\n.noty_theme__bootstrap-v4.noty_type__success {\n  background-color: #dff0d8;\n  color: #3c763d;\n  border-color: #d6e9c6; }\n\n.noty_theme__semanticui.noty_bar {\n  margin: 4px 0;\n  overflow: hidden;\n  position: relative;\n  border: 1px solid transparent;\n  font-size: 1em;\n  border-radius: .28571429rem;\n  box-shadow: 0 0 0 1px rgba(34, 36, 38, 0.22) inset, 0 0 0 0 transparent; }\n  .noty_theme__semanticui.noty_bar .noty_body {\n    padding: 1em 1.5em;\n    line-height: 1.4285em; }\n  .noty_theme__semanticui.noty_bar .noty_buttons {\n    padding: 10px; }\n\n.noty_theme__semanticui.noty_type__alert,\n.noty_theme__semanticui.noty_type__notification {\n  background-color: #f8f8f9;\n  color: rgba(0, 0, 0, 0.87); }\n\n.noty_theme__semanticui.noty_type__warning {\n  background-color: #fffaf3;\n  color: #573a08;\n  box-shadow: 0 0 0 1px #c9ba9b inset, 0 0 0 0 transparent; }\n\n.noty_theme__semanticui.noty_type__error {\n  background-color: #fff6f6;\n  color: #9f3a38;\n  box-shadow: 0 0 0 1px #e0b4b4 inset, 0 0 0 0 transparent; }\n\n.noty_theme__semanticui.noty_type__info,\n.noty_theme__semanticui.noty_type__information {\n  background-color: #f8ffff;\n  color: #276f86;\n  box-shadow: 0 0 0 1px #a9d5de inset, 0 0 0 0 transparent; }\n\n.noty_theme__semanticui.noty_type__success {\n  background-color: #fcfff5;\n  color: #2c662d;\n  box-shadow: 0 0 0 1px #a3c293 inset, 0 0 0 0 transparent; }\n\n.noty_theme__nest.noty_bar {\n  margin: 0 0 15px 0;\n  overflow: hidden;\n  border-radius: 2px;\n  position: relative;\n  box-shadow: rgba(0, 0, 0, 0.098039) 5px 4px 10px 0; }\n  .noty_theme__nest.noty_bar .noty_body {\n    padding: 10px;\n    font-size: 14px;\n    text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.1); }\n  .noty_theme__nest.noty_bar .noty_buttons {\n    padding: 10px; }\n\n.noty_layout .noty_theme__nest.noty_bar {\n  z-index: 5; }\n\n.noty_layout .noty_theme__nest.noty_bar:nth-child(2) {\n  position: absolute;\n  top: 0;\n  margin-top: 4px;\n  margin-right: -4px;\n  margin-left: 4px;\n  z-index: 4;\n  width: 100%; }\n\n.noty_layout .noty_theme__nest.noty_bar:nth-child(3) {\n  position: absolute;\n  top: 0;\n  margin-top: 8px;\n  margin-right: -8px;\n  margin-left: 8px;\n  z-index: 3;\n  width: 100%; }\n\n.noty_layout .noty_theme__nest.noty_bar:nth-child(4) {\n  position: absolute;\n  top: 0;\n  margin-top: 12px;\n  margin-right: -12px;\n  margin-left: 12px;\n  z-index: 2;\n  width: 100%; }\n\n.noty_layout .noty_theme__nest.noty_bar:nth-child(5) {\n  position: absolute;\n  top: 0;\n  margin-top: 16px;\n  margin-right: -16px;\n  margin-left: 16px;\n  z-index: 1;\n  width: 100%; }\n\n.noty_layout .noty_theme__nest.noty_bar:nth-child(n+6) {\n  position: absolute;\n  top: 0;\n  margin-top: 20px;\n  margin-right: -20px;\n  margin-left: 20px;\n  z-index: -1;\n  width: 100%; }\n\n#noty_layout__bottomLeft .noty_theme__nest.noty_bar:nth-child(2),\n#noty_layout__topLeft .noty_theme__nest.noty_bar:nth-child(2) {\n  margin-top: 4px;\n  margin-left: -4px;\n  margin-right: 4px; }\n\n#noty_layout__bottomLeft .noty_theme__nest.noty_bar:nth-child(3),\n#noty_layout__topLeft .noty_theme__nest.noty_bar:nth-child(3) {\n  margin-top: 8px;\n  margin-left: -8px;\n  margin-right: 8px; }\n\n#noty_layout__bottomLeft .noty_theme__nest.noty_bar:nth-child(4),\n#noty_layout__topLeft .noty_theme__nest.noty_bar:nth-child(4) {\n  margin-top: 12px;\n  margin-left: -12px;\n  margin-right: 12px; }\n\n#noty_layout__bottomLeft .noty_theme__nest.noty_bar:nth-child(5),\n#noty_layout__topLeft .noty_theme__nest.noty_bar:nth-child(5) {\n  margin-top: 16px;\n  margin-left: -16px;\n  margin-right: 16px; }\n\n#noty_layout__bottomLeft .noty_theme__nest.noty_bar:nth-child(n+6),\n#noty_layout__topLeft .noty_theme__nest.noty_bar:nth-child(n+6) {\n  margin-top: 20px;\n  margin-left: -20px;\n  margin-right: 20px; }\n\n.noty_theme__nest.noty_type__alert,\n.noty_theme__nest.noty_type__notification {\n  background-color: #073B4C;\n  color: #fff; }\n  .noty_theme__nest.noty_type__alert .noty_progressbar,\n  .noty_theme__nest.noty_type__notification .noty_progressbar {\n    background-color: #fff; }\n\n.noty_theme__nest.noty_type__warning {\n  background-color: #FFD166;\n  color: #fff; }\n\n.noty_theme__nest.noty_type__error {\n  background-color: #EF476F;\n  color: #fff; }\n  .noty_theme__nest.noty_type__error .noty_progressbar {\n    opacity: .4; }\n\n.noty_theme__nest.noty_type__info,\n.noty_theme__nest.noty_type__information {\n  background-color: #118AB2;\n  color: #fff; }\n  .noty_theme__nest.noty_type__info .noty_progressbar,\n  .noty_theme__nest.noty_type__information .noty_progressbar {\n    opacity: .6; }\n\n.noty_theme__nest.noty_type__success {\n  background-color: #06D6A0;\n  color: #fff; }\n", ""]);

// exports


/***/ }),

/***/ 162:
/***/ (function(module, exports, __webpack_require__) {

(function(e,t){ true?module.exports=t():"function"==typeof define&&define.amd?define([],t):"object"==typeof exports?exports.VMoney=t():e.VMoney=t()})(this,function(){return function(e){function t(r){if(n[r])return n[r].exports;var i=n[r]={i:r,l:!1,exports:{}};return e[r].call(i.exports,i,i.exports,t),i.l=!0,i.exports}var n={};return t.m=e,t.c=n,t.i=function(e){return e},t.d=function(e,n,r){t.o(e,n)||Object.defineProperty(e,n,{configurable:!1,enumerable:!0,get:r})},t.n=function(e){var n=e&&e.__esModule?function(){return e.default}:function(){return e};return t.d(n,"a",n),n},t.o=function(e,t){return Object.prototype.hasOwnProperty.call(e,t)},t.p=".",t(t.s=9)}([function(e,t,n){"use strict";t.a={prefix:"",suffix:"",thousands:",",decimal:".",precision:2}},function(e,t,n){"use strict";var r=n(2),i=n(5),u=n(0);t.a=function(e,t){if(t.value){var o=n.i(i.a)(u.a,t.value);if("INPUT"!==e.tagName.toLocaleUpperCase()){var a=e.getElementsByTagName("input");1!==a.length||(e=a[0])}e.oninput=function(){var t=e.value.length-e.selectionEnd;e.value=n.i(r.a)(e.value,o),t=Math.max(t,o.suffix.length),t=e.value.length-t,t=Math.max(t,o.prefix.length+1),n.i(r.b)(e,t),e.dispatchEvent(n.i(r.c)("change"))},e.onfocus=function(){n.i(r.b)(e,e.value.length-o.suffix.length)},e.oninput(),e.dispatchEvent(n.i(r.c)("input"))}}},function(e,t,n){"use strict";function r(e){var t=arguments.length>1&&void 0!==arguments[1]?arguments[1]:m.a;"number"==typeof e&&(e=e.toFixed(o(t.precision)));var n=e.indexOf("-")>=0?"-":"",r=u(e),i=c(r,t.precision),a=d(i).split("."),p=a[0],l=a[1];return p=f(p,t.thousands),t.prefix+n+s(p,l,t.decimal)+t.suffix}function i(e,t){var n=e.indexOf("-")>=0?-1:1,r=u(e),i=c(r,t);return parseFloat(i)*n}function u(e){return d(e).replace(/\D+/g,"")||"0"}function o(e){return a(0,e,20)}function a(e,t,n){return Math.max(e,Math.min(t,n))}function c(e,t){var n=Math.pow(10,t);return(parseFloat(e)/n).toFixed(o(t))}function f(e,t){return e.replace(/(\d)(?=(?:\d{3})+\b)/gm,"$1"+t)}function s(e,t,n){return t?e+n+t:e}function d(e){return e?e.toString():""}function p(e,t){var n=function(){e.setSelectionRange(t,t)};e===document.activeElement&&(n(),setTimeout(n,1))}function l(e){var t=document.createEvent("Event");return t.initEvent(e,!0,!0),t}var m=n(0);n.d(t,"a",function(){return r}),n.d(t,"d",function(){return i}),n.d(t,"b",function(){return p}),n.d(t,"c",function(){return l})},function(e,t,n){"use strict";function r(e,t){t&&Object.keys(t).map(function(e){a.a[e]=t[e]}),e.directive("money",o.a),e.component("money",u.a)}Object.defineProperty(t,"__esModule",{value:!0});var i=n(6),u=n.n(i),o=n(1),a=n(0);n.d(t,"Money",function(){return u.a}),n.d(t,"VMoney",function(){return o.a}),n.d(t,"options",function(){return a.a}),n.d(t,"VERSION",function(){return c});var c="0.8.1";t.default=r,"undefined"!=typeof window&&window.Vue&&window.Vue.use(r)},function(e,t,n){"use strict";Object.defineProperty(t,"__esModule",{value:!0});var r=n(1),i=n(0),u=n(2);t.default={name:"Money",props:{value:{required:!0,type:[Number,String],default:0},masked:{type:Boolean,default:!1},precision:{type:Number,default:function(){return i.a.precision}},decimal:{type:String,default:function(){return i.a.decimal}},thousands:{type:String,default:function(){return i.a.thousands}},prefix:{type:String,default:function(){return i.a.prefix}},suffix:{type:String,default:function(){return i.a.suffix}}},directives:{money:r.a},data:function(){return{formattedValue:""}},watch:{value:{immediate:!0,handler:function(e,t){var r=n.i(u.a)(e,this.$props);r!==this.formattedValue&&(this.formattedValue=r)}}},methods:{change:function(e){this.$emit("input",this.masked?e.target.value:n.i(u.d)(e.target.value,this.precision))}}}},function(e,t,n){"use strict";t.a=function(e,t){return e=e||{},t=t||{},Object.keys(e).concat(Object.keys(t)).reduce(function(n,r){return n[r]=void 0===t[r]?e[r]:t[r],n},{})}},function(e,t,n){var r=n(7)(n(4),n(8),null,null);e.exports=r.exports},function(e,t){e.exports=function(e,t,n,r){var i,u=e=e||{},o=typeof e.default;"object"!==o&&"function"!==o||(i=e,u=e.default);var a="function"==typeof u?u.options:u;if(t&&(a.render=t.render,a.staticRenderFns=t.staticRenderFns),n&&(a._scopeId=n),r){var c=a.computed||(a.computed={});Object.keys(r).forEach(function(e){var t=r[e];c[e]=function(){return t}})}return{esModule:i,exports:u,options:a}}},function(e,t){e.exports={render:function(){var e=this,t=e.$createElement;return(e._self._c||t)("input",{directives:[{name:"money",rawName:"v-money",value:{precision:e.precision,decimal:e.decimal,thousands:e.thousands,prefix:e.prefix,suffix:e.suffix},expression:"{precision, decimal, thousands, prefix, suffix}"}],staticClass:"v-money",attrs:{type:"tel"},domProps:{value:e.formattedValue},on:{change:e.change}})},staticRenderFns:[]}},function(e,t,n){e.exports=n(3)}])});

/***/ }),

/***/ 187:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(188);


/***/ }),

/***/ 188:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "transactionApp", function() { return transactionApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_transaction_add_transaction_vue__ = __webpack_require__(189);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_transaction_add_transaction_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__components_transaction_add_transaction_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_transaction_edit_transaction_vue__ = __webpack_require__(192);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_transaction_edit_transaction_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__components_transaction_edit_transaction_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_vuejs_noty_dist_vuejs_noty_css__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_vuejs_noty_dist_vuejs_noty_css___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_vuejs_noty_dist_vuejs_noty_css__);



var transactionApp = new Vue({
    el: '#div-transaction',
    components: {
        addTransaction: __WEBPACK_IMPORTED_MODULE_0__components_transaction_add_transaction_vue___default.a,
        editTransaction: __WEBPACK_IMPORTED_MODULE_1__components_transaction_edit_transaction_vue___default.a
    },
    data: {
        step: 1
    },
    mounted: function mounted() {
        $('#transaction-list').DataTable({
            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ records",
                "infoEmpty": "No records found",
                "infoFiltered": "(filtered1 from _MAX_ total records)",
                "lengthMenu": "Show _MENU_",
                "search": "Search:",
                "zeroRecords": "No matching records found",
                "paginate": {
                    "previous": "Prev",
                    "next": "Next",
                    "last": "Last",
                    "first": "First"
                }
            },

            "lengthMenu": [[5, 15, 20, -1], [5, 15, 20, "All"] // change per page values here
            ],
            "aaSorting": [[0, 'desc']],
            "pageLength": 5,
            "pagingType": "bootstrap_full_number",
            "columnDefs": [{
                "className": "dt-right"
                //"targets": [2]
            }]
        });
    },

    methods: {
        updateStep: function updateStep(data) {
            Vue.set(this, 'step', data);
        },
        rollback: function rollback(reference_no) {
            var self = this;
            alertify.confirm('Confirmation', 'Are you sure you want to rollback this transaction?', function () {
                axios.post(base_url + '/transaction/rollback', {
                    reference_no: reference_no
                }, { headers: {
                        "Cache-Control": "no-cache, no-store, must-revalidate",
                        "Pragma": "no-cache",
                        "Expires": "0",
                        "dataType": "json"
                    } }).then(function (response) {
                    self.$noty.success('Transaction successfully rolled back', {
                        theme: 'mint',
                        killer: true,
                        timeout: 3000,
                        layout: 'topRight'
                    });
                    setTimeout(function () {
                        location.reload();
                    }, 1000);
                }).catch(function (error) {
                    console.log(error);
                });
            }, function () {});
        }
    }
});

/***/ }),

/***/ 189:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(3)
/* script */
var __vue_script__ = __webpack_require__(190)
/* template */
var __vue_template__ = __webpack_require__(191)
/* template functional */
  var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/components/transaction/add-transaction.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {  return key !== "default" && key.substr(0, 2) !== "__"})) {  console.error("named exports are not supported in *.vue files.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-09574619", Component.options)
  } else {
    hotAPI.reload("data-v-09574619", Component.options)
' + '  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 190:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_v_money__ = __webpack_require__(162);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_v_money___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_v_money__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_vuejs_noty_dist_vuejs_noty_css__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_vuejs_noty_dist_vuejs_noty_css___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_vuejs_noty_dist_vuejs_noty_css__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
  name: "addTransaction",
  props: ["transaction_types"],
  mounted: function mounted() {
    var self = this;
    $(document).on('focus', '.date-picker', function () {
      $(this).datepicker({
        orientation: "left",
        autoclose: true,
        format: "MM d, yyyy"
      }).on('changeDate', function () {
        self.updateDate($(this).data('model'), $(this).val());
      });
    });
  },
  data: function data() {
    return {
      step: 1,
      prestige_card: "",
      transaction_type: "",
      multiple_days: false,
      date_from: "",
      date_to: "",
      destination: "",
      airline: "",
      hotel: "",
      price: "",
      money: {
        decimal: '.',
        thousands: '',
        prefix: '',
        suffix: '',
        precision: 2,
        masked: false /* doesn't work with directive */
      },
      directives: {
        money: __WEBPACK_IMPORTED_MODULE_0_v_money__["VMoney"]
      },
      companions: []
    };
  },

  computed: {
    isAirlineNeeded: function isAirlineNeeded() {
      var type_legend = this.transaction_type.split("|")[0];
      return !(type_legend == 'H' || type_legend == 'V');
    },
    isHotelNeeded: function isHotelNeeded() {
      var type_legend = this.transaction_type.split("|")[0];
      return !(type_legend == 'V' || type_legend == 'A');
    },
    isDateToRequired: function isDateToRequired() {
      return this.multiple_days == true;
    },
    isAirlineRequired: function isAirlineRequired() {
      var type_legend = this.transaction_type.split("|")[0];
      return type_legend == 'A';
    },
    isHotelRequired: function isHotelRequired() {
      var type_legend = this.transaction_type.split("|")[0];
      return type_legend == 'H' || type_legend == 'D' || type_legend == 'I' || type_legend == 'IC' || type_legend == 'DC' || type_legend == '';
    }
  },
  methods: {
    updateDate: function updateDate(model, value) {
      Vue.set(this, model, value);
    },
    checkCard: function checkCard() {
      var self = this;

      this.fetch('/validate-prestige-card', {
        prestige_card_no: this.prestige_card,
        prestige_status: 1
      }).then(function (response) {
        if (response.data.SUCCESS) {
          var result = response.data.DATA;
          self.companions = [];
          self.companions.push({
            first_name: result.user_info['first_name'],
            middle_name: result.user_info['middle_name'],
            last_name: result.user_info['last_name'],
            age: result.user_info['age'],
            contact_no: result.user_info['contact_no'],
            email: result.user_info['email']
          });

          self.proceed('new-transaction');
        } else {
          self.$noty.error(response.data.MESSAGE, {
            theme: 'mint',
            killer: true,
            timeout: 3000,
            layout: 'topRight'
          });
        }
      }).catch(function (error) {
        console.log(error.message);
      });
    },
    checkCompanion: function checkCompanion() {
      this.proceed('new-transaction');
    },
    addCompanion: function addCompanion() {
      this.companions.push({
        first_name: "",
        middle_name: "",
        last_name: "",
        age: "",
        contact_no: "",
        email: ""
      });
    },
    removeCompanion: function removeCompanion(key) {
      this.companions.splice(key, 1);
    },
    proceed: function proceed(scope) {
      var _this = this;

      this.$validator.validateAll(scope).then(function (result) {
        if (result) {
          _this.next();
        }
      });
    },
    next: function next() {
      Vue.set(this, 'step', parseInt(this.step) + 1);
    },
    back: function back() {
      Vue.set(this, 'step', parseInt(this.step) - 1);
    },
    submit: function submit() {
      var self = this;
      this.fetch('/transaction/new', {

        prestige_card: self.prestige_card,
        transaction_type: self.transaction_type,
        multiple_days: self.multiple_days,
        date_from: self.date_from,
        date_to: self.date_to,
        destination: self.destination,
        airline: self.airline,
        hotel: self.hotel,
        price: self.price,
        companions: self.companions

      }).then(function (response) {
        console.log(response.data);
        if (response.data.SUCCESS) {
          self.$noty.success(response.data.MESSAGE, {
            theme: 'mint',
            killer: true,
            timeout: 3000,
            layout: 'topRight'
          });
          setTimeout(function () {
            window.location = base_url + '/transaction/list/all';
          }, 1000);
        } else {
          self.$noty.error(response.data.MESSAGE, {
            theme: 'mint',
            killer: true,
            timeout: 3000,
            layout: 'topRight'
          });
        }
      }).catch(function (error) {
        console.log(error.message);
      });
    },
    fetch: function fetch(actionUrl, params) {
      return axios.post(base_url + actionUrl, params, { headers: {
          "Cache-Control": "no-cache, no-store, must-revalidate",
          "Pragma": "no-cache",
          "Expires": "0",
          "dataType": "json"
        } });
    }
  },

  watch: {
    // clear the value of date_to field when multiple_days is checked
    multiple_days: function multiple_days(newValue) {
      if (!newValue) {
        this.date_to = '';
      }
    },
    transaction_type: function transaction_type(newValue) {
      if (newValue == 'V') {
        this.hotel = '';
        this.airline = '';
      }
    },

    step: {
      handler: function handler(val) {
        this.$emit("add_step", val);
      }
    }
  }
});

/***/ }),

/***/ 191:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "form",
    {
      attrs: {
        role: "form",
        method: "post",
        "data-vv-scope": "new-transaction"
      }
    },
    [
      _vm.step == 1
        ? [
            _c(
              "div",
              {
                staticClass: "portlet light bordered col-md-4 col-md-offset-3"
              },
              [
                _c("div", { staticClass: "portlet-body form" }, [
                  _c("div", { staticClass: "form-body" }, [
                    _c(
                      "div",
                      {
                        staticClass: "form-group",
                        class: {
                          "has-error": _vm.ferrors.has(
                            "new-transaction.prestige_card"
                          )
                        }
                      },
                      [
                        _c("label", { attrs: { for: "prestige_card" } }, [
                          _vm._v("Prestige Card Number")
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "input-group" }, [
                          _c("input", {
                            directives: [
                              {
                                name: "mask",
                                rawName: "v-mask",
                                value: "####-####-####",
                                expression: "'####-####-####'"
                              },
                              {
                                name: "validate",
                                rawName: "v-validate",
                                value: {
                                  required: true,
                                  regex: /^\d{4}-\d{4}-\d{4}$/
                                },
                                expression:
                                  "{ required: true, regex: /^\\d{4}-\\d{4}-\\d{4}$/ }"
                              },
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.prestige_card,
                                expression: "prestige_card"
                              }
                            ],
                            staticClass: "form-control",
                            attrs: {
                              type: "text",
                              id: "prestige_card",
                              name: "prestige_card",
                              "data-vv-as": "Prestige Card Number",
                              placeholder: "XXXX-XXXX-XXXX",
                              autofocus: ""
                            },
                            domProps: { value: _vm.prestige_card },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.prestige_card = $event.target.value
                              }
                            }
                          }),
                          _vm._v(" "),
                          _vm._m(0)
                        ]),
                        _vm._v(" "),
                        _c(
                          "span",
                          {
                            directives: [
                              {
                                name: "show",
                                rawName: "v-show",
                                value: _vm.ferrors.has(
                                  "new-transaction.prestige_card"
                                ),
                                expression:
                                  "ferrors.has('new-transaction.prestige_card')"
                              }
                            ],
                            staticClass: "help-block"
                          },
                          [
                            _vm._v(
                              "\n              " +
                                _vm._s(
                                  _vm.ferrors.first(
                                    "new-transaction.prestige_card"
                                  )
                                ) +
                                "\n            "
                            )
                          ]
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c("br"),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        staticClass: "form-group",
                        class: {
                          "has-error": _vm.ferrors.has(
                            "new-transaction.transaction_type"
                          )
                        }
                      },
                      [
                        _c("label", { attrs: { for: "transaction_type" } }, [
                          _vm._v("Transaction Type")
                        ]),
                        _vm._v(" "),
                        _c(
                          "select",
                          {
                            directives: [
                              {
                                name: "validate",
                                rawName: "v-validate",
                                value: "required",
                                expression: "'required'"
                              },
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.transaction_type,
                                expression: "transaction_type"
                              }
                            ],
                            staticClass: "form-control",
                            attrs: {
                              name: "transaction_type",
                              id: "",
                              "data-vv-as": "Transaction Type"
                            },
                            on: {
                              change: function($event) {
                                var $$selectedVal = Array.prototype.filter
                                  .call($event.target.options, function(o) {
                                    return o.selected
                                  })
                                  .map(function(o) {
                                    var val = "_value" in o ? o._value : o.value
                                    return val
                                  })
                                _vm.transaction_type = $event.target.multiple
                                  ? $$selectedVal
                                  : $$selectedVal[0]
                              }
                            }
                          },
                          [
                            _c(
                              "option",
                              { attrs: { disabled: "", value: "" } },
                              [_vm._v("Please select Transaction Type")]
                            ),
                            _vm._v(" "),
                            _vm._l(_vm.transaction_types, function(trans_type) {
                              return _c("option", {
                                domProps: {
                                  value:
                                    trans_type.legend + "|" + trans_type.name,
                                  textContent: _vm._s(trans_type.name)
                                }
                              })
                            })
                          ],
                          2
                        ),
                        _vm._v(" "),
                        _c(
                          "span",
                          {
                            directives: [
                              {
                                name: "show",
                                rawName: "v-show",
                                value: _vm.ferrors.has(
                                  "new-transaction.transaction_type"
                                ),
                                expression:
                                  "ferrors.has('new-transaction.transaction_type')"
                              }
                            ],
                            staticClass: "help-block"
                          },
                          [
                            _vm._v(
                              "\n              " +
                                _vm._s(
                                  _vm.ferrors.first(
                                    "new-transaction.transaction_type"
                                  )
                                ) +
                                "\n            "
                            )
                          ]
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        staticClass: "form-group",
                        class: {
                          "has-error": _vm.ferrors.has(
                            "new-transaction.date_from"
                          )
                        }
                      },
                      [
                        _c("label", { staticClass: "mt-checkbox" }, [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.multiple_days,
                                expression: "multiple_days"
                              }
                            ],
                            attrs: { type: "checkbox" },
                            domProps: {
                              checked: Array.isArray(_vm.multiple_days)
                                ? _vm._i(_vm.multiple_days, null) > -1
                                : _vm.multiple_days
                            },
                            on: {
                              change: function($event) {
                                var $$a = _vm.multiple_days,
                                  $$el = $event.target,
                                  $$c = $$el.checked ? true : false
                                if (Array.isArray($$a)) {
                                  var $$v = null,
                                    $$i = _vm._i($$a, $$v)
                                  if ($$el.checked) {
                                    $$i < 0 &&
                                      (_vm.multiple_days = $$a.concat([$$v]))
                                  } else {
                                    $$i > -1 &&
                                      (_vm.multiple_days = $$a
                                        .slice(0, $$i)
                                        .concat($$a.slice($$i + 1)))
                                  }
                                } else {
                                  _vm.multiple_days = $$c
                                }
                              }
                            }
                          }),
                          _vm._v(" Multiple Days\n                "),
                          _c("span")
                        ]),
                        _vm._v(" "),
                        _c("br"),
                        _vm._v(" "),
                        _c("label", { attrs: { for: "date_from" } }, [
                          _vm._v("Date From")
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "input-group" }, [
                          _c("input", {
                            directives: [
                              {
                                name: "validate",
                                rawName: "v-validate",
                                value: {
                                  required: true,
                                  date_format: "MMMM D, YYYY"
                                },
                                expression:
                                  "{ required: true, date_format:'MMMM D, YYYY' }"
                              },
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.date_from,
                                expression: "date_from"
                              }
                            ],
                            staticClass:
                              "form-control form-control-inline date-picker",
                            attrs: {
                              id: "date_from",
                              name: "date_from",
                              "data-vv-as": "Date From",
                              "data-model": "date_from",
                              size: "16",
                              type: "text",
                              value: ""
                            },
                            domProps: { value: _vm.date_from },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.date_from = $event.target.value
                              }
                            }
                          }),
                          _vm._v(" "),
                          _vm._m(1)
                        ]),
                        _vm._v(" "),
                        _c(
                          "span",
                          {
                            directives: [
                              {
                                name: "show",
                                rawName: "v-show",
                                value: _vm.ferrors.has(
                                  "new-transaction.date_from"
                                ),
                                expression:
                                  "ferrors.has('new-transaction.date_from')"
                              }
                            ],
                            staticClass: "help-block"
                          },
                          [
                            _vm._v(
                              "\n              " +
                                _vm._s(
                                  _vm.ferrors.first("new-transaction.date_from")
                                ) +
                                "\n            "
                            )
                          ]
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        staticClass: "form-group",
                        class: {
                          "has-error": _vm.ferrors.has(
                            "new-transaction.date_to"
                          )
                        }
                      },
                      [
                        _c("label", { attrs: { for: "date_to" } }, [
                          _vm._v("Date To")
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "input-group" }, [
                          _c("input", {
                            directives: [
                              {
                                name: "validate",
                                rawName: "v-validate",
                                value: {
                                  rules: {
                                    required: this.isDateToRequired,
                                    after: _vm.date_from,
                                    date_format: "MMMM D, YYYY"
                                  }
                                },
                                expression:
                                  "{ rules: { required: this.isDateToRequired, after:date_from, date_format: 'MMMM D, YYYY' }}"
                              },
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.date_to,
                                expression: "date_to"
                              }
                            ],
                            staticClass:
                              "form-control form-control-inline date-picker",
                            attrs: {
                              id: "date_to",
                              name: "date_to",
                              "data-model": "date_to",
                              disabled: !_vm.multiple_days,
                              size: "16",
                              type: "text",
                              value: "",
                              "data-vv-as": "Date To"
                            },
                            domProps: { value: _vm.date_to },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.date_to = $event.target.value
                              }
                            }
                          }),
                          _vm._v(" "),
                          _vm._m(2)
                        ]),
                        _vm._v(" "),
                        _c(
                          "span",
                          {
                            directives: [
                              {
                                name: "show",
                                rawName: "v-show",
                                value: _vm.ferrors.has(
                                  "new-transaction.date_to"
                                ),
                                expression:
                                  "ferrors.has('new-transaction.date_to')"
                              }
                            ],
                            staticClass: "help-block"
                          },
                          [
                            _vm._v(
                              "\n              " +
                                _vm._s(
                                  _vm.ferrors.first("new-transaction.date_to")
                                ) +
                                "\n            "
                            )
                          ]
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        staticClass: "form-group",
                        class: {
                          "has-error": _vm.ferrors.has(
                            "new-transaction.destination"
                          )
                        }
                      },
                      [
                        _c("label", { attrs: { for: "destination" } }, [
                          _vm._v("Destination")
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "input-group" }, [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.destination,
                                expression: "destination"
                              },
                              {
                                name: "validate",
                                rawName: "v-validate",
                                value: "required",
                                expression: "'required'"
                              }
                            ],
                            staticClass: "form-control",
                            attrs: {
                              type: "text",
                              id: "destination",
                              name: "destination",
                              "data-vv-as": "Destination"
                            },
                            domProps: { value: _vm.destination },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.destination = $event.target.value
                              }
                            }
                          }),
                          _vm._v(" "),
                          _vm._m(3)
                        ]),
                        _vm._v(" "),
                        _c(
                          "span",
                          {
                            directives: [
                              {
                                name: "show",
                                rawName: "v-show",
                                value: _vm.ferrors.has(
                                  "new-transaction.destination"
                                ),
                                expression:
                                  "ferrors.has('new-transaction.destination')"
                              }
                            ],
                            staticClass: "help-block"
                          },
                          [
                            _vm._v(
                              "\n              " +
                                _vm._s(
                                  _vm.ferrors.first(
                                    "new-transaction.destination"
                                  )
                                ) +
                                "\n            "
                            )
                          ]
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        staticClass: "form-group",
                        class: {
                          "has-error": _vm.ferrors.has(
                            "new-transaction.airline"
                          )
                        }
                      },
                      [
                        _c("label", { attrs: { for: "airline" } }, [
                          _vm._v("Airline")
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "input-group" }, [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.airline,
                                expression: "airline"
                              },
                              {
                                name: "validate",
                                rawName: "v-validate",
                                value: {
                                  rules: {
                                    required: this.isAirlineNeeded,
                                    alpha_spaces: true,
                                    min: 2
                                  }
                                },
                                expression:
                                  "{ rules: { required: this.isAirlineNeeded, alpha_spaces: true, min: 2 } }"
                              }
                            ],
                            staticClass: "form-control",
                            attrs: {
                              type: "text",
                              disabled: !_vm.isAirlineNeeded,
                              id: "airline",
                              name: "airline",
                              "data-vv-as": "Airline"
                            },
                            domProps: { value: _vm.airline },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.airline = $event.target.value
                              }
                            }
                          }),
                          _vm._v(" "),
                          _vm._m(4)
                        ]),
                        _vm._v(" "),
                        _c(
                          "span",
                          {
                            directives: [
                              {
                                name: "show",
                                rawName: "v-show",
                                value: _vm.ferrors.has(
                                  "new-transaction.airline"
                                ),
                                expression:
                                  "ferrors.has('new-transaction.airline')"
                              }
                            ],
                            staticClass: "help-block"
                          },
                          [
                            _vm._v(
                              "\n                " +
                                _vm._s(
                                  _vm.ferrors.first("new-transaction.airline")
                                ) +
                                "\n              "
                            )
                          ]
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        staticClass: "form-group",
                        class: {
                          "has-error": _vm.ferrors.has("new-transaction.hotel")
                        }
                      },
                      [
                        _c("label", { attrs: { for: "hotel" } }, [
                          _vm._v("Hotel")
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "input-group" }, [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.hotel,
                                expression: "hotel"
                              },
                              {
                                name: "validate",
                                rawName: "v-validate",
                                value: {
                                  rules: { required: this.isHotelRequired }
                                },
                                expression:
                                  "{ rules: { required: this.isHotelRequired } }"
                              }
                            ],
                            staticClass: "form-control",
                            attrs: {
                              type: "text",
                              disabled: !_vm.isHotelNeeded,
                              id: "hotel",
                              name: "hotel",
                              "data-vv-as": "Hotel"
                            },
                            domProps: { value: _vm.hotel },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.hotel = $event.target.value
                              }
                            }
                          }),
                          _vm._v(" "),
                          _vm._m(5)
                        ]),
                        _vm._v(" "),
                        _c(
                          "span",
                          {
                            directives: [
                              {
                                name: "show",
                                rawName: "v-show",
                                value: _vm.ferrors.has("new-transaction.hotel"),
                                expression:
                                  "ferrors.has('new-transaction.hotel')"
                              }
                            ],
                            staticClass: "help-block"
                          },
                          [
                            _vm._v(
                              "\n                " +
                                _vm._s(
                                  _vm.ferrors.first("new-transaction.hotel")
                                ) +
                                "\n              "
                            )
                          ]
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        staticClass: "form-group",
                        class: {
                          "has-error": _vm.ferrors.has("new-transaction.price")
                        }
                      },
                      [
                        _c("label", { attrs: { for: "price" } }, [
                          _vm._v("Price")
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "input-group" }, [
                          _c("input", {
                            directives: [
                              {
                                name: "money",
                                rawName: "v-money",
                                value: _vm.money,
                                expression: "money"
                              },
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.price,
                                expression: "price"
                              },
                              {
                                name: "validate",
                                rawName: "v-validate",
                                value: "required|min_value:1.00",
                                expression: "'required|min_value:1.00'"
                              }
                            ],
                            staticClass: "form-control",
                            attrs: {
                              type: "text",
                              id: "price",
                              name: "price",
                              "data-vv-as": "Price"
                            },
                            domProps: { value: _vm.price },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.price = $event.target.value
                              }
                            }
                          }),
                          _vm._v(" "),
                          _vm._m(6)
                        ]),
                        _vm._v(" "),
                        _c(
                          "span",
                          {
                            directives: [
                              {
                                name: "show",
                                rawName: "v-show",
                                value: _vm.ferrors.has("new-transaction.price"),
                                expression:
                                  "ferrors.has('new-transaction.price')"
                              }
                            ],
                            staticClass: "help-block"
                          },
                          [
                            _vm._v(
                              "\n                " +
                                _vm._s(
                                  _vm.ferrors.first("new-transaction.price")
                                ) +
                                "\n              "
                            )
                          ]
                        )
                      ]
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "form-actions text-center" }, [
                    _c(
                      "button",
                      {
                        staticClass: "btn default",
                        attrs: { type: "button" },
                        on: {
                          click: function($event) {
                            $event.preventDefault()
                            _vm.back($event)
                          }
                        }
                      },
                      [_vm._v("Back")]
                    ),
                    _vm._v(" "),
                    _c(
                      "button",
                      {
                        staticClass: "btn blue",
                        attrs: { type: "button" },
                        on: {
                          click: function($event) {
                            $event.preventDefault()
                            _vm.checkCard($event)
                          }
                        }
                      },
                      [_vm._v("Next")]
                    )
                  ])
                ])
              ]
            )
          ]
        : _vm._e(),
      _vm._v(" "),
      _vm.step == 2
        ? [
            _c(
              "div",
              {
                staticClass: "portlet light bordered col-md-10 col-md-offset-1"
              },
              [
                _c("div", { staticClass: "portlet-title" }, [
                  _vm._m(7),
                  _vm._v(" "),
                  _c("div", { staticClass: "actions" }, [
                    _c(
                      "a",
                      {
                        staticClass: "btn btn-circle btn-icon-only btn-default",
                        on: {
                          click: function($event) {
                            $event.preventDefault()
                            _vm.addCompanion($event)
                          }
                        }
                      },
                      [_c("i", { staticClass: "fa fa-plus" })]
                    )
                  ])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "portlet-body form" }, [
                  _c(
                    "table",
                    {
                      staticClass:
                        "table table-bordered table-striped table-condensed flip-content table-companions"
                    },
                    [
                      _vm._m(8),
                      _vm._v(" "),
                      _c(
                        "tbody",
                        _vm._l(_vm.companions, function(companion, index) {
                          return _c("tr", [
                            _c(
                              "td",
                              {
                                class: {
                                  "has-error": _vm.ferrors.has(
                                    "new-transaction.companion_first_name"
                                  )
                                }
                              },
                              [
                                _c("input", {
                                  directives: [
                                    {
                                      name: "validate",
                                      rawName: "v-validate",
                                      value: "required",
                                      expression: "'required'"
                                    },
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: companion.first_name,
                                      expression: "companion.first_name"
                                    }
                                  ],
                                  staticClass: "form-control",
                                  attrs: {
                                    type: "text",
                                    name: "companion_first_name",
                                    "data-vv-as": "First Name"
                                  },
                                  domProps: { value: companion.first_name },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        companion,
                                        "first_name",
                                        $event.target.value
                                      )
                                    }
                                  }
                                }),
                                _vm._v(" "),
                                _c(
                                  "span",
                                  {
                                    directives: [
                                      {
                                        name: "show",
                                        rawName: "v-show",
                                        value: _vm.ferrors.has(
                                          "new-transaction.companion_first_name"
                                        ),
                                        expression:
                                          "ferrors.has('new-transaction.companion_first_name')"
                                      }
                                    ],
                                    staticClass: "help-block"
                                  },
                                  [
                                    _vm._v(
                                      "\n                  " +
                                        _vm._s(
                                          _vm.ferrors.first(
                                            "new-transaction.companion_first_name"
                                          )
                                        ) +
                                        "\n                "
                                    )
                                  ]
                                )
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "td",
                              {
                                class: {
                                  "has-error": _vm.ferrors.has(
                                    "new-transaction.companion_middle_name"
                                  )
                                }
                              },
                              [
                                _c("input", {
                                  directives: [
                                    {
                                      name: "validate",
                                      rawName: "v-validate",
                                      value: "required",
                                      expression: "'required'"
                                    },
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: companion.middle_name,
                                      expression: "companion.middle_name"
                                    }
                                  ],
                                  staticClass: "form-control",
                                  attrs: {
                                    type: "text",
                                    name: "companion_middle_name",
                                    "data-vv-as": "Middle Name"
                                  },
                                  domProps: { value: companion.middle_name },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        companion,
                                        "middle_name",
                                        $event.target.value
                                      )
                                    }
                                  }
                                }),
                                _vm._v(" "),
                                _c(
                                  "span",
                                  {
                                    directives: [
                                      {
                                        name: "show",
                                        rawName: "v-show",
                                        value: _vm.ferrors.has(
                                          "new-transaction.companion_middle_name"
                                        ),
                                        expression:
                                          "ferrors.has('new-transaction.companion_middle_name')"
                                      }
                                    ],
                                    staticClass: "help-block"
                                  },
                                  [
                                    _vm._v(
                                      "\n                  " +
                                        _vm._s(
                                          _vm.ferrors.first(
                                            "new-transaction.companion_middle_name"
                                          )
                                        ) +
                                        "\n                "
                                    )
                                  ]
                                )
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "td",
                              {
                                class: {
                                  "has-error": _vm.ferrors.has(
                                    "new-transaction.companion_last_name"
                                  )
                                }
                              },
                              [
                                _c("input", {
                                  directives: [
                                    {
                                      name: "validate",
                                      rawName: "v-validate",
                                      value: "required",
                                      expression: "'required'"
                                    },
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: companion.last_name,
                                      expression: "companion.last_name"
                                    }
                                  ],
                                  staticClass: "form-control",
                                  attrs: {
                                    type: "text",
                                    name: "companion_last_name",
                                    "data-vv-as": "Last Name"
                                  },
                                  domProps: { value: companion.last_name },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        companion,
                                        "last_name",
                                        $event.target.value
                                      )
                                    }
                                  }
                                }),
                                _vm._v(" "),
                                _c(
                                  "span",
                                  {
                                    directives: [
                                      {
                                        name: "show",
                                        rawName: "v-show",
                                        value: _vm.ferrors.has(
                                          "new-transaction.companion_last_name"
                                        ),
                                        expression:
                                          "ferrors.has('new-transaction.companion_last_name')"
                                      }
                                    ],
                                    staticClass: "help-block"
                                  },
                                  [
                                    _vm._v(
                                      "\n                  " +
                                        _vm._s(
                                          _vm.ferrors.first(
                                            "new-transaction.companion_last_name"
                                          )
                                        ) +
                                        "\n                "
                                    )
                                  ]
                                )
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "td",
                              {
                                class: {
                                  "has-error": _vm.ferrors.has(
                                    "new-transaction.companion_age"
                                  )
                                }
                              },
                              [
                                _c("input", {
                                  directives: [
                                    {
                                      name: "validate",
                                      rawName: "v-validate",
                                      value: "min_value:1",
                                      expression: "'min_value:1'"
                                    },
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: companion.age,
                                      expression: "companion.age"
                                    }
                                  ],
                                  staticClass: "form-control",
                                  class: {
                                    "has-error": _vm.ferrors.has(
                                      "new-transaction.companion_age"
                                    )
                                  },
                                  attrs: {
                                    type: "number",
                                    name: "companion_age",
                                    "data-vv-as": "Age"
                                  },
                                  domProps: { value: companion.age },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        companion,
                                        "age",
                                        $event.target.value
                                      )
                                    }
                                  }
                                }),
                                _vm._v(" "),
                                _c(
                                  "span",
                                  {
                                    directives: [
                                      {
                                        name: "show",
                                        rawName: "v-show",
                                        value: _vm.ferrors.has(
                                          "new-transaction.companion_age"
                                        ),
                                        expression:
                                          "ferrors.has('new-transaction.companion_age')"
                                      }
                                    ],
                                    staticClass: "help-block"
                                  },
                                  [
                                    _vm._v(
                                      "\n                  " +
                                        _vm._s(
                                          _vm.ferrors.first(
                                            "new-transaction.companion_age"
                                          )
                                        ) +
                                        "\n                "
                                    )
                                  ]
                                )
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "td",
                              {
                                class: {
                                  "has-error": _vm.ferrors.has(
                                    "new-transaction.companion_contact_no"
                                  )
                                }
                              },
                              [
                                _c("input", {
                                  directives: [
                                    {
                                      name: "validate",
                                      rawName: "v-validate",
                                      value: {
                                        required: true,
                                        regex: /^(09|\+639)\d{9}$/
                                      },
                                      expression:
                                        "{ required: true, regex: /^(09|\\+639)\\d{9}$/ }"
                                    },
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: companion.contact_no,
                                      expression: "companion.contact_no"
                                    }
                                  ],
                                  staticClass: "form-control",
                                  attrs: {
                                    type: "text",
                                    name: "companion_contact_no",
                                    "data-vv-as": "Contact Number"
                                  },
                                  domProps: { value: companion.contact_no },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        companion,
                                        "contact_no",
                                        $event.target.value
                                      )
                                    }
                                  }
                                }),
                                _vm._v(" "),
                                _c(
                                  "span",
                                  {
                                    directives: [
                                      {
                                        name: "show",
                                        rawName: "v-show",
                                        value: _vm.ferrors.has(
                                          "new-transaction.companion_contact_no"
                                        ),
                                        expression:
                                          "ferrors.has('new-transaction.companion_contact_no')"
                                      }
                                    ],
                                    staticClass: "help-block"
                                  },
                                  [
                                    _vm._v(
                                      "\n                  " +
                                        _vm._s(
                                          _vm.ferrors.first(
                                            "new-transaction.companion_contact_no"
                                          )
                                        ) +
                                        "\n                "
                                    )
                                  ]
                                )
                              ]
                            ),
                            _vm._v(" "),
                            _c("td", [
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: companion.email,
                                    expression: "companion.email"
                                  }
                                ],
                                staticClass: "form-control",
                                attrs: { type: "text" },
                                domProps: { value: companion.email },
                                on: {
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      companion,
                                      "email",
                                      $event.target.value
                                    )
                                  }
                                }
                              })
                            ]),
                            _vm._v(" "),
                            _c("td", [
                              _c(
                                "a",
                                {
                                  staticClass:
                                    "btn btn-circle btn-icon-only btn-danger",
                                  on: {
                                    click: function($event) {
                                      $event.preventDefault()
                                      _vm.removeCompanion(index)
                                    }
                                  }
                                },
                                [_c("i", { staticClass: "fa fa-remove" })]
                              )
                            ])
                          ])
                        })
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c("div", { staticClass: "form-actions text-center" }, [
                    _c(
                      "button",
                      {
                        staticClass: "btn default",
                        attrs: { type: "button" },
                        on: {
                          click: function($event) {
                            $event.preventDefault()
                            _vm.back($event)
                          }
                        }
                      },
                      [_vm._v("Back")]
                    ),
                    _vm._v(" "),
                    _c(
                      "button",
                      {
                        staticClass: "btn blue",
                        attrs: { type: "button" },
                        on: {
                          click: function($event) {
                            $event.preventDefault()
                            _vm.checkCompanion($event)
                          }
                        }
                      },
                      [_vm._v("Next")]
                    )
                  ])
                ])
              ]
            )
          ]
        : _vm._e(),
      _vm._v(" "),
      _vm.step == 3
        ? [
            _c(
              "div",
              { staticClass: "portlet light col-md-8 col-md-offset-2" },
              [
                _vm._m(9),
                _vm._v(" "),
                _c("div", { staticClass: "portlet-body row" }, [
                  _c("div", { staticClass: "col-md-4" }, [
                    _c("ul", { staticClass: "list-unstyled" }, [
                      _c("li", [
                        _c("span", [
                          _vm._v("Prestige Card : " + _vm._s(_vm.prestige_card))
                        ])
                      ]),
                      _vm._v(" "),
                      _c("li", [
                        _c("span", [
                          _vm._v(
                            "Transaction Type : " +
                              _vm._s(_vm.transaction_type.split("|")[1])
                          )
                        ])
                      ]),
                      _vm._v(" "),
                      _c("li", [
                        _c("span", [_vm._v("Price : " + _vm._s(_vm.price))])
                      ])
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-md-4" }),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-md-4" }, [
                    _c("ul", { staticClass: "list-unstyled" }, [
                      _c("li", [
                        _c("span", [
                          _vm._v("Date From : " + _vm._s(_vm.date_from))
                        ])
                      ]),
                      _vm._v(" "),
                      _vm.multiple_days
                        ? _c("li", [
                            _c("span", [
                              _vm._v("Date To : " + _vm._s(_vm.date_to))
                            ])
                          ])
                        : _vm._e(),
                      _vm._v(" "),
                      _c("li", [
                        _c("span", [
                          _vm._v("Destination : " + _vm._s(_vm.destination))
                        ])
                      ]),
                      _vm._v(" "),
                      _vm.isHotelNeeded
                        ? _c("li", [
                            _c("span", [_vm._v("Hotel : " + _vm._s(_vm.hotel))])
                          ])
                        : _vm._e(),
                      _vm._v(" "),
                      _vm.isAirlineNeeded
                        ? _c("li", [
                            _c("span", [
                              _vm._v("Airline : " + _vm._s(_vm.airline))
                            ])
                          ])
                        : _vm._e()
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-md-12" }, [
                    _c("h4", [_vm._v(" Companions : ")]),
                    _vm._v(" "),
                    _c(
                      "table",
                      {
                        staticClass:
                          "table table-bordered table-striped table-condensed flip-content table-companions"
                      },
                      [
                        _vm._m(10),
                        _vm._v(" "),
                        _c(
                          "tbody",
                          _vm._l(_vm.companions, function(companion, index) {
                            return _c("tr", [
                              _c("td", [
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: companion.first_name,
                                      expression: "companion.first_name"
                                    }
                                  ],
                                  staticClass: "form-control",
                                  attrs: { type: "text", readonly: "" },
                                  domProps: { value: companion.first_name },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        companion,
                                        "first_name",
                                        $event.target.value
                                      )
                                    }
                                  }
                                })
                              ]),
                              _vm._v(" "),
                              _c("td", [
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: companion.middle_name,
                                      expression: "companion.middle_name"
                                    }
                                  ],
                                  staticClass: "form-control",
                                  attrs: { type: "text", readonly: "" },
                                  domProps: { value: companion.middle_name },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        companion,
                                        "middle_name",
                                        $event.target.value
                                      )
                                    }
                                  }
                                })
                              ]),
                              _vm._v(" "),
                              _c("td", [
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: companion.last_name,
                                      expression: "companion.last_name"
                                    }
                                  ],
                                  staticClass: "form-control",
                                  attrs: { type: "text", readonly: "" },
                                  domProps: { value: companion.last_name },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        companion,
                                        "last_name",
                                        $event.target.value
                                      )
                                    }
                                  }
                                })
                              ]),
                              _vm._v(" "),
                              _c("td", [
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: companion.age,
                                      expression: "companion.age"
                                    }
                                  ],
                                  staticClass: "form-control",
                                  attrs: { type: "text", readonly: "" },
                                  domProps: { value: companion.age },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        companion,
                                        "age",
                                        $event.target.value
                                      )
                                    }
                                  }
                                })
                              ]),
                              _vm._v(" "),
                              _c("td", [
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: companion.contact_no,
                                      expression: "companion.contact_no"
                                    }
                                  ],
                                  staticClass: "form-control",
                                  attrs: { type: "text", readonly: "" },
                                  domProps: { value: companion.contact_no },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        companion,
                                        "contact_no",
                                        $event.target.value
                                      )
                                    }
                                  }
                                })
                              ]),
                              _vm._v(" "),
                              _c("td", [
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: companion.email,
                                      expression: "companion.email"
                                    }
                                  ],
                                  staticClass: "form-control",
                                  attrs: { type: "text", readonly: "" },
                                  domProps: { value: companion.email },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        companion,
                                        "email",
                                        $event.target.value
                                      )
                                    }
                                  }
                                })
                              ])
                            ])
                          })
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-actions text-center" }, [
                      _c(
                        "button",
                        {
                          staticClass: "btn default",
                          attrs: { type: "button" },
                          on: {
                            click: function($event) {
                              $event.preventDefault()
                              _vm.back($event)
                            }
                          }
                        },
                        [_vm._v("Back")]
                      ),
                      _vm._v(" "),
                      _c(
                        "button",
                        {
                          staticClass: "btn blue",
                          attrs: { type: "button" },
                          on: {
                            click: function($event) {
                              $event.preventDefault()
                              _vm.submit($event)
                            }
                          }
                        },
                        [_vm._v("Submit")]
                      )
                    ])
                  ])
                ])
              ]
            )
          ]
        : _vm._e()
    ],
    2
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", { staticClass: "input-group-addon" }, [
      _c("i", { staticClass: "fa fa-credit-card font-blue" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", { staticClass: "input-group-addon" }, [
      _c("i", { staticClass: "fa fa-calendar font-blue" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", { staticClass: "input-group-addon" }, [
      _c("i", { staticClass: "fa fa-calendar font-blue" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", { staticClass: "input-group-addon" }, [
      _c("i", { staticClass: "fa fa-road font-blue" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", { staticClass: "input-group-addon" }, [
      _c("i", { staticClass: "fa fa-plane font-blue" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", { staticClass: "input-group-addon" }, [
      _c("i", { staticClass: "fa fa-hotel font-blue" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", { staticClass: "input-group-addon" }, [
      _c("i", { staticClass: "fa fa-money font-blue" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "caption" }, [
      _c("i", { staticClass: "icon-social-dribbble font-green" }),
      _vm._v(" "),
      _c("span", { staticClass: "caption-subject font-green bold uppercase" }, [
        _vm._v("Companions")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", [_vm._v("First Name")]),
        _vm._v(" "),
        _c("th", [_vm._v("Middle Name")]),
        _vm._v(" "),
        _c("th", [_vm._v("Last Name")]),
        _vm._v(" "),
        _c("th", [_vm._v("Age")]),
        _vm._v(" "),
        _c("th", [_vm._v("Contact Number")]),
        _vm._v(" "),
        _c("th", [_vm._v("Email")]),
        _vm._v(" "),
        _c("th")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "portlet-title" }, [
      _c("div", { staticClass: "caption" }, [
        _c("i", { staticClass: "icon-paper-plane font-green-haze" }),
        _vm._v(" "),
        _c(
          "span",
          { staticClass: "caption-subject bold font-green-haze uppercase" },
          [_vm._v(" Transaction Details ")]
        )
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", [_vm._v("First Name")]),
        _vm._v(" "),
        _c("th", [_vm._v("Middle Name")]),
        _vm._v(" "),
        _c("th", [_vm._v("Last Name")]),
        _vm._v(" "),
        _c("th", [_vm._v("Age")]),
        _vm._v(" "),
        _c("th", [_vm._v("Contact Number")]),
        _vm._v(" "),
        _c("th", [_vm._v("Email")])
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-09574619", module.exports)
  }
}

/***/ }),

/***/ 192:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(3)
/* script */
var __vue_script__ = __webpack_require__(193)
/* template */
var __vue_template__ = __webpack_require__(194)
/* template functional */
  var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/components/transaction/edit-transaction.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {  return key !== "default" && key.substr(0, 2) !== "__"})) {  console.error("named exports are not supported in *.vue files.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-67785c78", Component.options)
  } else {
    hotAPI.reload("data-v-67785c78", Component.options)
' + '  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 193:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_v_money__ = __webpack_require__(162);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_v_money___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_v_money__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_vuejs_noty_dist_vuejs_noty_css__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_vuejs_noty_dist_vuejs_noty_css___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_vuejs_noty_dist_vuejs_noty_css__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
  name: "editTransaction",
  props: ["transaction_types", "transaction_details"],
  mounted: function mounted() {
    var self = this;
    $(document).on('focus', '.date-picker', function () {
      $(this).datepicker({
        orientation: "left",
        autoclose: true,
        format: "MM d, yyyy"
      }).on('changeDate', function () {
        self.updateDate($(this).data('model'), $(this).val());
      });
    });
  },
  data: function data() {
    return {
      step: 1,
      prestige_card: this.transaction_details.card_no,
      transaction_type: this.transaction_details.legend + '|' + this.transaction_details.name,
      multiple_days: this.transaction_details.multiple_day == 1 ? true : false,
      date_from: moment(this.transaction_details.date_from, 'YYYY-MM-DD').format("MMMM D, YYYY"),
      date_to: this.transaction_details.multiple_day == 1 ? moment(this.transaction_details.date_to, 'YYYY-MM-DD').format("MMMM D, YYYY") : "",
      destination: this.transaction_details.destination,
      airline: this.transaction_details.airline_name,
      hotel: this.transaction_details.hotel_name,
      price: this.transaction_details.total_price,
      money: {
        decimal: '.',
        thousands: '',
        prefix: '',
        suffix: '',
        precision: 2,
        masked: false /* doesn't work with directive */
      },
      directives: {
        money: __WEBPACK_IMPORTED_MODULE_0_v_money__["VMoney"]
      },
      companions: []
    };
  },

  computed: {
    isAirlineNeeded: function isAirlineNeeded() {
      var type_legend = this.transaction_type.split("|")[0];
      return !(type_legend == 'H' || type_legend == 'V');
    },
    isHotelNeeded: function isHotelNeeded() {
      var type_legend = this.transaction_type.split("|")[0];
      return !(type_legend == 'V' || type_legend == 'A');
    },
    isDateToRequired: function isDateToRequired() {
      return this.multiple_days == true;
    },
    isAirlineRequired: function isAirlineRequired() {
      var type_legend = this.transaction_type.split("|")[0];
      return type_legend == 'A';
    },
    isHotelRequired: function isHotelRequired() {
      var type_legend = this.transaction_type.split("|")[0];
      return type_legend == 'H' || type_legend == 'D' || type_legend == 'I' || type_legend == 'IC' || type_legend == 'DC' || type_legend == '';
    }
  },
  methods: {
    updateDate: function updateDate(model, value) {
      Vue.set(this, model, value);
    },
    checkCard: function checkCard() {
      var self = this;

      this.fetch('/validate-prestige-card', {
        prestige_card_no: this.prestige_card,
        prestige_status: 1,
        transaction_id: this.transaction_details.transaction_id,
        action: 'get-transaction-companions'
      }).then(function (response) {
        if (response.data.SUCCESS) {
          var companions = response.data.DATA;
          Vue.set(self, 'companions', []);
          for (var i = 0; i < companions.length; i++) {
            self.companions.push({
              first_name: companions[i]['first_name'],
              middle_name: companions[i]['middle_name'],
              last_name: companions[i]['last_name'],
              age: companions[i]['age'],
              contact_no: companions[i]['contact_no'],
              email: companions[i]['email']
            });
          }

          self.proceed('new-transaction');
        } else {
          self.$noty.error(response.data.MESSAGE, {
            theme: 'mint',
            killer: true,
            timeout: 3000,
            layout: 'topRight'
          });
        }
      }).catch(function (error) {
        console.log(error.message);
      });
    },
    checkCompanion: function checkCompanion() {
      this.proceed('new-transaction');
    },
    addCompanion: function addCompanion() {
      this.companions.push({
        first_name: "",
        middle_name: "",
        last_name: "",
        age: "",
        contact_no: "",
        email: ""
      });
    },
    removeCompanion: function removeCompanion(key) {
      this.companions.splice(key, 1);
    },
    proceed: function proceed(scope) {
      var _this = this;

      this.$validator.validateAll(scope).then(function (result) {
        if (result) {
          _this.next();
        }
      });
    },
    next: function next() {
      Vue.set(this, 'step', parseInt(this.step) + 1);
    },
    back: function back() {
      Vue.set(this, 'step', parseInt(this.step) - 1);
    },
    submit: function submit() {
      var self = this;
      this.fetch('/transaction/update', {
        prestige_card: self.prestige_card,
        transaction_type: self.transaction_type,
        multiple_days: self.multiple_days,
        date_from: self.date_from,
        date_to: self.date_to,
        destination: self.destination,
        airline: self.airline,
        hotel: self.hotel,
        price: self.price,
        companions: self.companions,
        transaction_id: self.transaction_details.transaction_id

      }).then(function (response) {
        console.log(response.data);
        if (response.data.SUCCESS) {
          self.$noty.success(response.data.MESSAGE, {
            theme: 'mint',
            killer: true,
            timeout: 3000,
            layout: 'topRight'
          });
          setTimeout(function () {
            window.location = base_url + '/transaction/list/all';
          }, 1000);
        } else {
          self.$noty.error(response.data.MESSAGE, {
            theme: 'mint',
            killer: true,
            timeout: 3000,
            layout: 'topRight'
          });
        }
      }).catch(function (error) {
        console.log(error.message);
      });
    },
    fetch: function fetch(actionUrl, params) {
      return axios.post(base_url + actionUrl, params, { headers: {
          "Cache-Control": "no-cache, no-store, must-revalidate",
          "Pragma": "no-cache",
          "Expires": "0",
          "dataType": "json"
        } });
    }
  },

  watch: {
    // clear the value of date_to field when multiple_days is checked
    multiple_days: function multiple_days(newValue) {
      if (!newValue) {
        this.date_to = '';
      }
    },
    transaction_type: function transaction_type(newValue) {
      if (newValue == 'V') {
        this.hotel = '';
        this.airline = '';
      }
    },

    step: {
      handler: function handler(val) {
        this.$emit("add_step", val);
      }
    }
  }
});

/***/ }),

/***/ 194:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "form",
    {
      attrs: {
        role: "form",
        method: "post",
        "data-vv-scope": "new-transaction"
      }
    },
    [
      _vm.step == 1
        ? [
            _c(
              "div",
              {
                staticClass: "portlet light bordered col-md-4 col-md-offset-3"
              },
              [
                _c("div", { staticClass: "portlet-body form" }, [
                  _c("div", { staticClass: "form-body" }, [
                    _c(
                      "div",
                      {
                        staticClass: "form-group",
                        class: {
                          "has-error": _vm.ferrors.has(
                            "new-transaction.prestige_card"
                          )
                        }
                      },
                      [
                        _c("label", { attrs: { for: "prestige_card" } }, [
                          _vm._v("Prestige Card Number")
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "input-group" }, [
                          _c("input", {
                            directives: [
                              {
                                name: "mask",
                                rawName: "v-mask",
                                value: "####-####-####",
                                expression: "'####-####-####'"
                              },
                              {
                                name: "validate",
                                rawName: "v-validate",
                                value: {
                                  required: true,
                                  regex: /^\d{4}-\d{4}-\d{4}$/
                                },
                                expression:
                                  "{ required: true, regex: /^\\d{4}-\\d{4}-\\d{4}$/ }"
                              },
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.prestige_card,
                                expression: "prestige_card"
                              }
                            ],
                            staticClass: "form-control",
                            attrs: {
                              type: "text",
                              id: "prestige_card",
                              name: "prestige_card",
                              "data-vv-as": "Prestige Card Number",
                              placeholder: "XXXX-XXXX-XXXX",
                              autofocus: ""
                            },
                            domProps: { value: _vm.prestige_card },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.prestige_card = $event.target.value
                              }
                            }
                          }),
                          _vm._v(" "),
                          _vm._m(0)
                        ]),
                        _vm._v(" "),
                        _c(
                          "span",
                          {
                            directives: [
                              {
                                name: "show",
                                rawName: "v-show",
                                value: _vm.ferrors.has(
                                  "new-transaction.prestige_card"
                                ),
                                expression:
                                  "ferrors.has('new-transaction.prestige_card')"
                              }
                            ],
                            staticClass: "help-block"
                          },
                          [
                            _vm._v(
                              "\n              " +
                                _vm._s(
                                  _vm.ferrors.first(
                                    "new-transaction.prestige_card"
                                  )
                                ) +
                                "\n            "
                            )
                          ]
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c("br"),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        staticClass: "form-group",
                        class: {
                          "has-error": _vm.ferrors.has(
                            "new-transaction.transaction_type"
                          )
                        }
                      },
                      [
                        _c("label", { attrs: { for: "transaction_type" } }, [
                          _vm._v("Transaction Type")
                        ]),
                        _vm._v(" "),
                        _c(
                          "select",
                          {
                            directives: [
                              {
                                name: "validate",
                                rawName: "v-validate",
                                value: "required",
                                expression: "'required'"
                              },
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.transaction_type,
                                expression: "transaction_type"
                              }
                            ],
                            staticClass: "form-control",
                            attrs: {
                              name: "transaction_type",
                              id: "",
                              "data-vv-as": "Transaction Type"
                            },
                            on: {
                              change: function($event) {
                                var $$selectedVal = Array.prototype.filter
                                  .call($event.target.options, function(o) {
                                    return o.selected
                                  })
                                  .map(function(o) {
                                    var val = "_value" in o ? o._value : o.value
                                    return val
                                  })
                                _vm.transaction_type = $event.target.multiple
                                  ? $$selectedVal
                                  : $$selectedVal[0]
                              }
                            }
                          },
                          [
                            _c(
                              "option",
                              { attrs: { disabled: "", value: "" } },
                              [_vm._v("Please select Transaction Type")]
                            ),
                            _vm._v(" "),
                            _vm._l(_vm.transaction_types, function(trans_type) {
                              return _c("option", {
                                domProps: {
                                  value:
                                    trans_type.legend + "|" + trans_type.name,
                                  textContent: _vm._s(trans_type.name)
                                }
                              })
                            })
                          ],
                          2
                        ),
                        _vm._v(" "),
                        _c(
                          "span",
                          {
                            directives: [
                              {
                                name: "show",
                                rawName: "v-show",
                                value: _vm.ferrors.has(
                                  "new-transaction.transaction_type"
                                ),
                                expression:
                                  "ferrors.has('new-transaction.transaction_type')"
                              }
                            ],
                            staticClass: "help-block"
                          },
                          [
                            _vm._v(
                              "\n              " +
                                _vm._s(
                                  _vm.ferrors.first(
                                    "new-transaction.transaction_type"
                                  )
                                ) +
                                "\n            "
                            )
                          ]
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        staticClass: "form-group",
                        class: {
                          "has-error": _vm.ferrors.has(
                            "new-transaction.date_from"
                          )
                        }
                      },
                      [
                        _c("label", { staticClass: "mt-checkbox" }, [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.multiple_days,
                                expression: "multiple_days"
                              }
                            ],
                            attrs: { type: "checkbox" },
                            domProps: {
                              checked: Array.isArray(_vm.multiple_days)
                                ? _vm._i(_vm.multiple_days, null) > -1
                                : _vm.multiple_days
                            },
                            on: {
                              change: function($event) {
                                var $$a = _vm.multiple_days,
                                  $$el = $event.target,
                                  $$c = $$el.checked ? true : false
                                if (Array.isArray($$a)) {
                                  var $$v = null,
                                    $$i = _vm._i($$a, $$v)
                                  if ($$el.checked) {
                                    $$i < 0 &&
                                      (_vm.multiple_days = $$a.concat([$$v]))
                                  } else {
                                    $$i > -1 &&
                                      (_vm.multiple_days = $$a
                                        .slice(0, $$i)
                                        .concat($$a.slice($$i + 1)))
                                  }
                                } else {
                                  _vm.multiple_days = $$c
                                }
                              }
                            }
                          }),
                          _vm._v(" Multiple Days\n                "),
                          _c("span")
                        ]),
                        _vm._v(" "),
                        _c("br"),
                        _vm._v(" "),
                        _c("label", { attrs: { for: "date_from" } }, [
                          _vm._v("Date From")
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "input-group" }, [
                          _c("input", {
                            directives: [
                              {
                                name: "validate",
                                rawName: "v-validate",
                                value: {
                                  required: true,
                                  date_format: "MMMM D, YYYY"
                                },
                                expression:
                                  "{ required: true, date_format:'MMMM D, YYYY' }"
                              },
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.date_from,
                                expression: "date_from"
                              }
                            ],
                            staticClass:
                              "form-control form-control-inline date-picker",
                            attrs: {
                              id: "date_from",
                              name: "date_from",
                              "data-vv-as": "Date From",
                              "data-model": "date_from",
                              size: "16",
                              type: "text",
                              value: ""
                            },
                            domProps: { value: _vm.date_from },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.date_from = $event.target.value
                              }
                            }
                          }),
                          _vm._v(" "),
                          _vm._m(1)
                        ]),
                        _vm._v(" "),
                        _c(
                          "span",
                          {
                            directives: [
                              {
                                name: "show",
                                rawName: "v-show",
                                value: _vm.ferrors.has(
                                  "new-transaction.date_from"
                                ),
                                expression:
                                  "ferrors.has('new-transaction.date_from')"
                              }
                            ],
                            staticClass: "help-block"
                          },
                          [
                            _vm._v(
                              "\n              " +
                                _vm._s(
                                  _vm.ferrors.first("new-transaction.date_from")
                                ) +
                                "\n            "
                            )
                          ]
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        staticClass: "form-group",
                        class: {
                          "has-error": _vm.ferrors.has(
                            "new-transaction.date_to"
                          )
                        }
                      },
                      [
                        _c("label", { attrs: { for: "date_to" } }, [
                          _vm._v("Date To")
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "input-group" }, [
                          _c("input", {
                            directives: [
                              {
                                name: "validate",
                                rawName: "v-validate",
                                value: {
                                  rules: {
                                    required: this.isDateToRequired,
                                    after: _vm.date_from,
                                    date_format: "MMMM D, YYYY"
                                  }
                                },
                                expression:
                                  "{ rules: { required: this.isDateToRequired, after:date_from, date_format: 'MMMM D, YYYY' }}"
                              },
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.date_to,
                                expression: "date_to"
                              }
                            ],
                            staticClass:
                              "form-control form-control-inline date-picker",
                            attrs: {
                              id: "date_to",
                              name: "date_to",
                              "data-model": "date_to",
                              disabled: !_vm.multiple_days,
                              size: "16",
                              type: "text",
                              value: "",
                              "data-vv-as": "Date To"
                            },
                            domProps: { value: _vm.date_to },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.date_to = $event.target.value
                              }
                            }
                          }),
                          _vm._v(" "),
                          _vm._m(2)
                        ]),
                        _vm._v(" "),
                        _c(
                          "span",
                          {
                            directives: [
                              {
                                name: "show",
                                rawName: "v-show",
                                value: _vm.ferrors.has(
                                  "new-transaction.date_to"
                                ),
                                expression:
                                  "ferrors.has('new-transaction.date_to')"
                              }
                            ],
                            staticClass: "help-block"
                          },
                          [
                            _vm._v(
                              "\n              " +
                                _vm._s(
                                  _vm.ferrors.first("new-transaction.date_to")
                                ) +
                                "\n            "
                            )
                          ]
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        staticClass: "form-group",
                        class: {
                          "has-error": _vm.ferrors.has(
                            "new-transaction.destination"
                          )
                        }
                      },
                      [
                        _c("label", { attrs: { for: "destination" } }, [
                          _vm._v("Destination")
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "input-group" }, [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.destination,
                                expression: "destination"
                              },
                              {
                                name: "validate",
                                rawName: "v-validate",
                                value: "required",
                                expression: "'required'"
                              }
                            ],
                            staticClass: "form-control",
                            attrs: {
                              type: "text",
                              id: "destination",
                              name: "destination",
                              "data-vv-as": "Destination"
                            },
                            domProps: { value: _vm.destination },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.destination = $event.target.value
                              }
                            }
                          }),
                          _vm._v(" "),
                          _vm._m(3)
                        ]),
                        _vm._v(" "),
                        _c(
                          "span",
                          {
                            directives: [
                              {
                                name: "show",
                                rawName: "v-show",
                                value: _vm.ferrors.has(
                                  "new-transaction.destination"
                                ),
                                expression:
                                  "ferrors.has('new-transaction.destination')"
                              }
                            ],
                            staticClass: "help-block"
                          },
                          [
                            _vm._v(
                              "\n              " +
                                _vm._s(
                                  _vm.ferrors.first(
                                    "new-transaction.destination"
                                  )
                                ) +
                                "\n            "
                            )
                          ]
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        staticClass: "form-group",
                        class: {
                          "has-error": _vm.ferrors.has(
                            "new-transaction.airline"
                          )
                        }
                      },
                      [
                        _c("label", { attrs: { for: "airline" } }, [
                          _vm._v("Airline")
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "input-group" }, [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.airline,
                                expression: "airline"
                              },
                              {
                                name: "validate",
                                rawName: "v-validate",
                                value: {
                                  rules: {
                                    required: this.isAirlineNeeded,
                                    alpha_spaces: true,
                                    min: 2
                                  }
                                },
                                expression:
                                  "{ rules: { required: this.isAirlineNeeded, alpha_spaces: true, min: 2 } }"
                              }
                            ],
                            staticClass: "form-control",
                            attrs: {
                              type: "text",
                              disabled: !_vm.isAirlineNeeded,
                              id: "airline",
                              name: "airline",
                              "data-vv-as": "Airline"
                            },
                            domProps: { value: _vm.airline },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.airline = $event.target.value
                              }
                            }
                          }),
                          _vm._v(" "),
                          _vm._m(4)
                        ]),
                        _vm._v(" "),
                        _c(
                          "span",
                          {
                            directives: [
                              {
                                name: "show",
                                rawName: "v-show",
                                value: _vm.ferrors.has(
                                  "new-transaction.airline"
                                ),
                                expression:
                                  "ferrors.has('new-transaction.airline')"
                              }
                            ],
                            staticClass: "help-block"
                          },
                          [
                            _vm._v(
                              "\n                " +
                                _vm._s(
                                  _vm.ferrors.first("new-transaction.airline")
                                ) +
                                "\n              "
                            )
                          ]
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        staticClass: "form-group",
                        class: {
                          "has-error": _vm.ferrors.has("new-transaction.hotel")
                        }
                      },
                      [
                        _c("label", { attrs: { for: "hotel" } }, [
                          _vm._v("Hotel")
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "input-group" }, [
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.hotel,
                                expression: "hotel"
                              },
                              {
                                name: "validate",
                                rawName: "v-validate",
                                value: {
                                  rules: { required: this.isHotelRequired }
                                },
                                expression:
                                  "{ rules: { required: this.isHotelRequired } }"
                              }
                            ],
                            staticClass: "form-control",
                            attrs: {
                              type: "text",
                              disabled: !_vm.isHotelNeeded,
                              id: "hotel",
                              name: "hotel",
                              "data-vv-as": "Hotel"
                            },
                            domProps: { value: _vm.hotel },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.hotel = $event.target.value
                              }
                            }
                          }),
                          _vm._v(" "),
                          _vm._m(5)
                        ]),
                        _vm._v(" "),
                        _c(
                          "span",
                          {
                            directives: [
                              {
                                name: "show",
                                rawName: "v-show",
                                value: _vm.ferrors.has("new-transaction.hotel"),
                                expression:
                                  "ferrors.has('new-transaction.hotel')"
                              }
                            ],
                            staticClass: "help-block"
                          },
                          [
                            _vm._v(
                              "\n                " +
                                _vm._s(
                                  _vm.ferrors.first("new-transaction.hotel")
                                ) +
                                "\n              "
                            )
                          ]
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        staticClass: "form-group",
                        class: {
                          "has-error": _vm.ferrors.has("new-transaction.price")
                        }
                      },
                      [
                        _c("label", { attrs: { for: "price" } }, [
                          _vm._v("Price")
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "input-group" }, [
                          _c("input", {
                            directives: [
                              {
                                name: "money",
                                rawName: "v-money",
                                value: _vm.money,
                                expression: "money"
                              },
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.price,
                                expression: "price"
                              },
                              {
                                name: "validate",
                                rawName: "v-validate",
                                value: "required|min_value:1.00",
                                expression: "'required|min_value:1.00'"
                              }
                            ],
                            staticClass: "form-control",
                            attrs: {
                              type: "text",
                              id: "price",
                              name: "price",
                              "data-vv-as": "Price"
                            },
                            domProps: { value: _vm.price },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.price = $event.target.value
                              }
                            }
                          }),
                          _vm._v(" "),
                          _vm._m(6)
                        ]),
                        _vm._v(" "),
                        _c(
                          "span",
                          {
                            directives: [
                              {
                                name: "show",
                                rawName: "v-show",
                                value: _vm.ferrors.has("new-transaction.price"),
                                expression:
                                  "ferrors.has('new-transaction.price')"
                              }
                            ],
                            staticClass: "help-block"
                          },
                          [
                            _vm._v(
                              "\n                " +
                                _vm._s(
                                  _vm.ferrors.first("new-transaction.price")
                                ) +
                                "\n              "
                            )
                          ]
                        )
                      ]
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "form-actions text-center" }, [
                    _c(
                      "button",
                      {
                        staticClass: "btn default",
                        attrs: { type: "button" },
                        on: {
                          click: function($event) {
                            $event.preventDefault()
                            _vm.back($event)
                          }
                        }
                      },
                      [_vm._v("Back")]
                    ),
                    _vm._v(" "),
                    _c(
                      "button",
                      {
                        staticClass: "btn blue",
                        attrs: { type: "button" },
                        on: {
                          click: function($event) {
                            $event.preventDefault()
                            _vm.checkCard($event)
                          }
                        }
                      },
                      [_vm._v("Next")]
                    )
                  ])
                ])
              ]
            )
          ]
        : _vm._e(),
      _vm._v(" "),
      _vm.step == 2
        ? [
            _c(
              "div",
              {
                staticClass: "portlet light bordered col-md-10 col-md-offset-1"
              },
              [
                _c("div", { staticClass: "portlet-title" }, [
                  _vm._m(7),
                  _vm._v(" "),
                  _c("div", { staticClass: "actions" }, [
                    _c(
                      "a",
                      {
                        staticClass: "btn btn-circle btn-icon-only btn-default",
                        on: {
                          click: function($event) {
                            $event.preventDefault()
                            _vm.addCompanion($event)
                          }
                        }
                      },
                      [_c("i", { staticClass: "fa fa-plus" })]
                    )
                  ])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "portlet-body form" }, [
                  _c(
                    "table",
                    {
                      staticClass:
                        "table table-bordered table-striped table-condensed flip-content table-companions"
                    },
                    [
                      _vm._m(8),
                      _vm._v(" "),
                      _c(
                        "tbody",
                        _vm._l(_vm.companions, function(companion, index) {
                          return _c("tr", [
                            _c(
                              "td",
                              {
                                class: {
                                  "has-error": _vm.ferrors.has(
                                    "new-transaction.companion_first_name"
                                  )
                                }
                              },
                              [
                                _c("input", {
                                  directives: [
                                    {
                                      name: "validate",
                                      rawName: "v-validate",
                                      value: "required",
                                      expression: "'required'"
                                    },
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: companion.first_name,
                                      expression: "companion.first_name"
                                    }
                                  ],
                                  staticClass: "form-control",
                                  attrs: {
                                    type: "text",
                                    name: "companion_first_name",
                                    "data-vv-as": "First Name"
                                  },
                                  domProps: { value: companion.first_name },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        companion,
                                        "first_name",
                                        $event.target.value
                                      )
                                    }
                                  }
                                }),
                                _vm._v(" "),
                                _c(
                                  "span",
                                  {
                                    directives: [
                                      {
                                        name: "show",
                                        rawName: "v-show",
                                        value: _vm.ferrors.has(
                                          "new-transaction.companion_first_name"
                                        ),
                                        expression:
                                          "ferrors.has('new-transaction.companion_first_name')"
                                      }
                                    ],
                                    staticClass: "help-block"
                                  },
                                  [
                                    _vm._v(
                                      "\n                  " +
                                        _vm._s(
                                          _vm.ferrors.first(
                                            "new-transaction.companion_first_name"
                                          )
                                        ) +
                                        "\n                "
                                    )
                                  ]
                                )
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "td",
                              {
                                class: {
                                  "has-error": _vm.ferrors.has(
                                    "new-transaction.companion_middle_name"
                                  )
                                }
                              },
                              [
                                _c("input", {
                                  directives: [
                                    {
                                      name: "validate",
                                      rawName: "v-validate",
                                      value: "required",
                                      expression: "'required'"
                                    },
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: companion.middle_name,
                                      expression: "companion.middle_name"
                                    }
                                  ],
                                  staticClass: "form-control",
                                  attrs: {
                                    type: "text",
                                    name: "companion_middle_name",
                                    "data-vv-as": "Middle Name"
                                  },
                                  domProps: { value: companion.middle_name },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        companion,
                                        "middle_name",
                                        $event.target.value
                                      )
                                    }
                                  }
                                }),
                                _vm._v(" "),
                                _c(
                                  "span",
                                  {
                                    directives: [
                                      {
                                        name: "show",
                                        rawName: "v-show",
                                        value: _vm.ferrors.has(
                                          "new-transaction.companion_middle_name"
                                        ),
                                        expression:
                                          "ferrors.has('new-transaction.companion_middle_name')"
                                      }
                                    ],
                                    staticClass: "help-block"
                                  },
                                  [
                                    _vm._v(
                                      "\n                  " +
                                        _vm._s(
                                          _vm.ferrors.first(
                                            "new-transaction.companion_middle_name"
                                          )
                                        ) +
                                        "\n                "
                                    )
                                  ]
                                )
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "td",
                              {
                                class: {
                                  "has-error": _vm.ferrors.has(
                                    "new-transaction.companion_last_name"
                                  )
                                }
                              },
                              [
                                _c("input", {
                                  directives: [
                                    {
                                      name: "validate",
                                      rawName: "v-validate",
                                      value: "required",
                                      expression: "'required'"
                                    },
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: companion.last_name,
                                      expression: "companion.last_name"
                                    }
                                  ],
                                  staticClass: "form-control",
                                  attrs: {
                                    type: "text",
                                    name: "companion_last_name",
                                    "data-vv-as": "Last Name"
                                  },
                                  domProps: { value: companion.last_name },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        companion,
                                        "last_name",
                                        $event.target.value
                                      )
                                    }
                                  }
                                }),
                                _vm._v(" "),
                                _c(
                                  "span",
                                  {
                                    directives: [
                                      {
                                        name: "show",
                                        rawName: "v-show",
                                        value: _vm.ferrors.has(
                                          "new-transaction.companion_last_name"
                                        ),
                                        expression:
                                          "ferrors.has('new-transaction.companion_last_name')"
                                      }
                                    ],
                                    staticClass: "help-block"
                                  },
                                  [
                                    _vm._v(
                                      "\n                  " +
                                        _vm._s(
                                          _vm.ferrors.first(
                                            "new-transaction.companion_last_name"
                                          )
                                        ) +
                                        "\n                "
                                    )
                                  ]
                                )
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "td",
                              {
                                class: {
                                  "has-error": _vm.ferrors.has(
                                    "new-transaction.companion_age"
                                  )
                                }
                              },
                              [
                                _c("input", {
                                  directives: [
                                    {
                                      name: "validate",
                                      rawName: "v-validate",
                                      value: "min_value:1",
                                      expression: "'min_value:1'"
                                    },
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: companion.age,
                                      expression: "companion.age"
                                    }
                                  ],
                                  staticClass: "form-control",
                                  class: {
                                    "has-error": _vm.ferrors.has(
                                      "new-transaction.companion_age"
                                    )
                                  },
                                  attrs: {
                                    type: "number",
                                    name: "companion_age",
                                    "data-vv-as": "Age"
                                  },
                                  domProps: { value: companion.age },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        companion,
                                        "age",
                                        $event.target.value
                                      )
                                    }
                                  }
                                }),
                                _vm._v(" "),
                                _c(
                                  "span",
                                  {
                                    directives: [
                                      {
                                        name: "show",
                                        rawName: "v-show",
                                        value: _vm.ferrors.has(
                                          "new-transaction.companion_age"
                                        ),
                                        expression:
                                          "ferrors.has('new-transaction.companion_age')"
                                      }
                                    ],
                                    staticClass: "help-block"
                                  },
                                  [
                                    _vm._v(
                                      "\n                  " +
                                        _vm._s(
                                          _vm.ferrors.first(
                                            "new-transaction.companion_age"
                                          )
                                        ) +
                                        "\n                "
                                    )
                                  ]
                                )
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "td",
                              {
                                class: {
                                  "has-error": _vm.ferrors.has(
                                    "new-transaction.companion_contact_no"
                                  )
                                }
                              },
                              [
                                _c("input", {
                                  directives: [
                                    {
                                      name: "validate",
                                      rawName: "v-validate",
                                      value: {
                                        required: true,
                                        regex: /^(09|\+639)\d{9}$/
                                      },
                                      expression:
                                        "{ required: true, regex: /^(09|\\+639)\\d{9}$/ }"
                                    },
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: companion.contact_no,
                                      expression: "companion.contact_no"
                                    }
                                  ],
                                  staticClass: "form-control",
                                  attrs: {
                                    type: "text",
                                    name: "companion_contact_no",
                                    "data-vv-as": "Contact Number"
                                  },
                                  domProps: { value: companion.contact_no },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        companion,
                                        "contact_no",
                                        $event.target.value
                                      )
                                    }
                                  }
                                }),
                                _vm._v(" "),
                                _c(
                                  "span",
                                  {
                                    directives: [
                                      {
                                        name: "show",
                                        rawName: "v-show",
                                        value: _vm.ferrors.has(
                                          "new-transaction.companion_contact_no"
                                        ),
                                        expression:
                                          "ferrors.has('new-transaction.companion_contact_no')"
                                      }
                                    ],
                                    staticClass: "help-block"
                                  },
                                  [
                                    _vm._v(
                                      "\n                  " +
                                        _vm._s(
                                          _vm.ferrors.first(
                                            "new-transaction.companion_contact_no"
                                          )
                                        ) +
                                        "\n                "
                                    )
                                  ]
                                )
                              ]
                            ),
                            _vm._v(" "),
                            _c("td", [
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: companion.email,
                                    expression: "companion.email"
                                  }
                                ],
                                staticClass: "form-control",
                                attrs: { type: "text" },
                                domProps: { value: companion.email },
                                on: {
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      companion,
                                      "email",
                                      $event.target.value
                                    )
                                  }
                                }
                              })
                            ]),
                            _vm._v(" "),
                            _c("td", [
                              _c(
                                "a",
                                {
                                  staticClass:
                                    "btn btn-circle btn-icon-only btn-danger",
                                  on: {
                                    click: function($event) {
                                      $event.preventDefault()
                                      _vm.removeCompanion(index)
                                    }
                                  }
                                },
                                [_c("i", { staticClass: "fa fa-remove" })]
                              )
                            ])
                          ])
                        })
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c("div", { staticClass: "form-actions text-center" }, [
                    _c(
                      "button",
                      {
                        staticClass: "btn default",
                        attrs: { type: "button" },
                        on: {
                          click: function($event) {
                            $event.preventDefault()
                            _vm.back($event)
                          }
                        }
                      },
                      [_vm._v("Back")]
                    ),
                    _vm._v(" "),
                    _c(
                      "button",
                      {
                        staticClass: "btn blue",
                        attrs: { type: "button" },
                        on: {
                          click: function($event) {
                            $event.preventDefault()
                            _vm.checkCompanion($event)
                          }
                        }
                      },
                      [_vm._v("Next")]
                    )
                  ])
                ])
              ]
            )
          ]
        : _vm._e(),
      _vm._v(" "),
      _vm.step == 3
        ? [
            _c(
              "div",
              { staticClass: "portlet light col-md-8 col-md-offset-2" },
              [
                _vm._m(9),
                _vm._v(" "),
                _c("div", { staticClass: "portlet-body row" }, [
                  _c("div", { staticClass: "col-md-4" }, [
                    _c("ul", { staticClass: "list-unstyled" }, [
                      _c("li", [
                        _c("span", [
                          _vm._v("Prestige Card : " + _vm._s(_vm.prestige_card))
                        ])
                      ]),
                      _vm._v(" "),
                      _c("li", [
                        _c("span", [
                          _vm._v(
                            "Transaction Type : " +
                              _vm._s(_vm.transaction_type.split("|")[1])
                          )
                        ])
                      ]),
                      _vm._v(" "),
                      _c("li", [
                        _c("span", [_vm._v("Price : " + _vm._s(_vm.price))])
                      ])
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-md-4" }),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-md-4" }, [
                    _c("ul", { staticClass: "list-unstyled" }, [
                      _c("li", [
                        _c("span", [
                          _vm._v("Date From : " + _vm._s(_vm.date_from))
                        ])
                      ]),
                      _vm._v(" "),
                      _vm.multiple_days
                        ? _c("li", [
                            _c("span", [
                              _vm._v("Date To : " + _vm._s(_vm.date_to))
                            ])
                          ])
                        : _vm._e(),
                      _vm._v(" "),
                      _c("li", [
                        _c("span", [
                          _vm._v("Destination : " + _vm._s(_vm.destination))
                        ])
                      ]),
                      _vm._v(" "),
                      _vm.isHotelNeeded
                        ? _c("li", [
                            _c("span", [_vm._v("Hotel : " + _vm._s(_vm.hotel))])
                          ])
                        : _vm._e(),
                      _vm._v(" "),
                      _vm.isAirlineNeeded
                        ? _c("li", [
                            _c("span", [
                              _vm._v("Airline : " + _vm._s(_vm.airline))
                            ])
                          ])
                        : _vm._e()
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-md-12" }, [
                    _c("h4", [_vm._v(" Companions : ")]),
                    _vm._v(" "),
                    _c(
                      "table",
                      {
                        staticClass:
                          "table table-bordered table-striped table-condensed flip-content table-companions"
                      },
                      [
                        _vm._m(10),
                        _vm._v(" "),
                        _c(
                          "tbody",
                          _vm._l(_vm.companions, function(companion, index) {
                            return _c("tr", [
                              _c("td", [
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: companion.first_name,
                                      expression: "companion.first_name"
                                    }
                                  ],
                                  staticClass: "form-control",
                                  attrs: { type: "text", readonly: "" },
                                  domProps: { value: companion.first_name },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        companion,
                                        "first_name",
                                        $event.target.value
                                      )
                                    }
                                  }
                                })
                              ]),
                              _vm._v(" "),
                              _c("td", [
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: companion.middle_name,
                                      expression: "companion.middle_name"
                                    }
                                  ],
                                  staticClass: "form-control",
                                  attrs: { type: "text", readonly: "" },
                                  domProps: { value: companion.middle_name },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        companion,
                                        "middle_name",
                                        $event.target.value
                                      )
                                    }
                                  }
                                })
                              ]),
                              _vm._v(" "),
                              _c("td", [
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: companion.last_name,
                                      expression: "companion.last_name"
                                    }
                                  ],
                                  staticClass: "form-control",
                                  attrs: { type: "text", readonly: "" },
                                  domProps: { value: companion.last_name },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        companion,
                                        "last_name",
                                        $event.target.value
                                      )
                                    }
                                  }
                                })
                              ]),
                              _vm._v(" "),
                              _c("td", [
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: companion.age,
                                      expression: "companion.age"
                                    }
                                  ],
                                  staticClass: "form-control",
                                  attrs: { type: "text", readonly: "" },
                                  domProps: { value: companion.age },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        companion,
                                        "age",
                                        $event.target.value
                                      )
                                    }
                                  }
                                })
                              ]),
                              _vm._v(" "),
                              _c("td", [
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: companion.contact_no,
                                      expression: "companion.contact_no"
                                    }
                                  ],
                                  staticClass: "form-control",
                                  attrs: { type: "text", readonly: "" },
                                  domProps: { value: companion.contact_no },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        companion,
                                        "contact_no",
                                        $event.target.value
                                      )
                                    }
                                  }
                                })
                              ]),
                              _vm._v(" "),
                              _c("td", [
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: companion.email,
                                      expression: "companion.email"
                                    }
                                  ],
                                  staticClass: "form-control",
                                  attrs: { type: "text", readonly: "" },
                                  domProps: { value: companion.email },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        companion,
                                        "email",
                                        $event.target.value
                                      )
                                    }
                                  }
                                })
                              ])
                            ])
                          })
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-actions text-center" }, [
                      _c(
                        "button",
                        {
                          staticClass: "btn default",
                          attrs: { type: "button" },
                          on: {
                            click: function($event) {
                              $event.preventDefault()
                              _vm.back($event)
                            }
                          }
                        },
                        [_vm._v("Back")]
                      ),
                      _vm._v(" "),
                      _c(
                        "button",
                        {
                          staticClass: "btn blue",
                          attrs: { type: "button" },
                          on: {
                            click: function($event) {
                              $event.preventDefault()
                              _vm.submit($event)
                            }
                          }
                        },
                        [_vm._v("Submit")]
                      )
                    ])
                  ])
                ])
              ]
            )
          ]
        : _vm._e()
    ],
    2
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", { staticClass: "input-group-addon" }, [
      _c("i", { staticClass: "fa fa-credit-card font-blue" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", { staticClass: "input-group-addon" }, [
      _c("i", { staticClass: "fa fa-calendar font-blue" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", { staticClass: "input-group-addon" }, [
      _c("i", { staticClass: "fa fa-calendar font-blue" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", { staticClass: "input-group-addon" }, [
      _c("i", { staticClass: "fa fa-road font-blue" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", { staticClass: "input-group-addon" }, [
      _c("i", { staticClass: "fa fa-plane font-blue" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", { staticClass: "input-group-addon" }, [
      _c("i", { staticClass: "fa fa-hotel font-blue" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", { staticClass: "input-group-addon" }, [
      _c("i", { staticClass: "fa fa-money font-blue" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "caption" }, [
      _c("i", { staticClass: "icon-social-dribbble font-green" }),
      _vm._v(" "),
      _c("span", { staticClass: "caption-subject font-green bold uppercase" }, [
        _vm._v("Companions")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", [_vm._v("First Name")]),
        _vm._v(" "),
        _c("th", [_vm._v("Middle Name")]),
        _vm._v(" "),
        _c("th", [_vm._v("Last Name")]),
        _vm._v(" "),
        _c("th", [_vm._v("Age")]),
        _vm._v(" "),
        _c("th", [_vm._v("Contact Number")]),
        _vm._v(" "),
        _c("th", [_vm._v("Email")]),
        _vm._v(" "),
        _c("th")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "portlet-title" }, [
      _c("div", { staticClass: "caption" }, [
        _c("i", { staticClass: "icon-paper-plane font-green-haze" }),
        _vm._v(" "),
        _c(
          "span",
          { staticClass: "caption-subject bold font-green-haze uppercase" },
          [_vm._v(" Transaction Details ")]
        )
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", [_vm._v("First Name")]),
        _vm._v(" "),
        _c("th", [_vm._v("Middle Name")]),
        _vm._v(" "),
        _c("th", [_vm._v("Last Name")]),
        _vm._v(" "),
        _c("th", [_vm._v("Age")]),
        _vm._v(" "),
        _c("th", [_vm._v("Contact Number")]),
        _vm._v(" "),
        _c("th", [_vm._v("Email")])
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-67785c78", module.exports)
  }
}

/***/ }),

/***/ 3:
/***/ (function(module, exports) {

/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file.
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier /* server only */
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = injectStyles
  }

  if (hook) {
    var functional = options.functional
    var existing = functional
      ? options.render
      : options.beforeCreate

    if (!functional) {
      // inject component registration as beforeCreate hook
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    } else {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functioal component in vue file
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return existing(h, context)
      }
    }
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ })

/******/ });