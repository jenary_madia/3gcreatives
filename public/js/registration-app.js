/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 173);
/******/ })
/************************************************************************/
/******/ ({

/***/ 173:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(174);


/***/ }),

/***/ 174:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "registrationApp", function() { return registrationApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_registration_prestige_card__ = __webpack_require__(175);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_registration_prestige_card___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__components_registration_prestige_card__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_registration_personal_info__ = __webpack_require__(178);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_registration_personal_info___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__components_registration_personal_info__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_registration_account_details__ = __webpack_require__(181);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_registration_account_details___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__components_registration_account_details__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_registration_note_success__ = __webpack_require__(184);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_registration_note_success___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__components_registration_note_success__);
var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };






var registrationApp = new Vue({
  el: '#registration-app',

  components: {
    PrestigeCard: __WEBPACK_IMPORTED_MODULE_0__components_registration_prestige_card___default.a,
    PersonalInfo: __WEBPACK_IMPORTED_MODULE_1__components_registration_personal_info___default.a,
    AccountDetails: __WEBPACK_IMPORTED_MODULE_2__components_registration_account_details___default.a,
    NoteSuccess: __WEBPACK_IMPORTED_MODULE_3__components_registration_note_success___default.a
  },

  data: {
    step: 1,
    registration_details: {
      prestige_card: "",
      first_name: "",
      middle_name: "",
      last_name: "",
      birthdate: "",
      address: "",
      occupation: "",
      interest: "",
      contact_no: "",
      email: "",
      password: "",
      password_confirmation: ""
    },
    on_process: false,
    show_alert: false,
    message: "",
    registration_errors: {}
  },

  methods: {
    proceed: function proceed(response) {
      Vue.set(this, 'step', response.next_step);
      if (response.next_step == 2) {
        Vue.set(this.registration_details, 'prestige_card', response.prestige_card);
      }
    },
    signup: function signup() {
      var _this = this;

      var self = this;
      Vue.set(this, 'on_process', true);
      axios.post('/register', this.registration_details).then(function (response) {
        console.log(response.data);
        console.log(_typeof(response.data));
        var response_data = response.data;
        if (response_data.SUCCESS) {
          self.showMessage(1, response_data.MESSAGE);
        } else {
          self.showMessage(2, '', response_data.ERRORS);
          console.log(response_data.ERRORS);
        }
        Vue.set(_this, 'on_process', false);
      }).catch(function (error) {
        Vue.set(_this, 'on_process', false);
        self.showError(error.message);
      });
    },
    showMessage: function showMessage(type, message) {
      var errors = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : [];

      if (type == 1) {
        // success
        Vue.set(this, "message", message);
        Vue.set(this, "step", 4);
      } else {
        Vue.set(this, "show_alert", true);
        Vue.set(this, "registration_errors", errors);
      }
    },
    go_back: function go_back(step) {
      Vue.set(this, "step", step);
    }
  },

  mounted: function mounted() {}
});

/***/ }),

/***/ 175:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(3)
/* script */
var __vue_script__ = __webpack_require__(176)
/* template */
var __vue_template__ = __webpack_require__(177)
/* template functional */
  var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/components/registration/prestige-card.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {  return key !== "default" && key.substr(0, 2) !== "__"})) {  console.error("named exports are not supported in *.vue files.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-928cdac8", Component.options)
  } else {
    hotAPI.reload("data-v-928cdac8", Component.options)
' + '  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 176:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "PrestigeCard",
  data: function data() {
    return {
      prestige_card_no: "",

      // FOR ERROR HANDLING
      has_error: false,
      error_message: "",
      // END ERROR HANDLING

      on_process: false,
      login_link: ""
    };
  },

  methods: {
    validateCardNo: function validateCardNo() {
      var _this = this;

      var self = this;
      Vue.set(this, 'on_process', true);
      if (this.prestige_card_no == "") {
        this.showError("Please enter your Prestige Card Number.");
        Vue.set(this, 'on_process', false);
        return false;
      }
      axios.post('/validate-prestige-card', {
        prestige_card_no: this.prestige_card_no,
        prestige_status: 0
      }).then(function (response) {
        var response_data = response.data;
        if (response_data.SUCCESS) {
          self.$emit('proceed', {
            next_step: 2,
            prestige_card: response_data.PRESTIGE_CARD
          });
        } else {
          self.showError(response_data.MESSAGE);
        }
        Vue.set(_this, 'on_process', false);
      }).catch(function (error) {
        self.showError(error.message);
        Vue.set(_this, 'on_process', false);
      });
    },
    showError: function showError(message) {
      Vue.set(this, 'has_error', true);
      Vue.set(this, 'error_message', message);
    }
  },
  mounted: function mounted() {
    Vue.set(this, 'login_link', login_link);
  }
});

/***/ }),

/***/ 177:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "form",
    { attrs: { action: "registration.html", method: "post" } },
    [
      _c("p", [_vm._v(" Enter your prestige number below: ")]),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "form-group", class: { "has-error": _vm.has_error } },
        [
          _c(
            "label",
            { staticClass: "control-label visible-ie8 visible-ie9" },
            [_vm._v("Prestige Number")]
          ),
          _vm._v(" "),
          _c(
            "div",
            {
              directives: [
                {
                  name: "show",
                  rawName: "v-show",
                  value: _vm.has_error,
                  expression: "has_error"
                }
              ],
              staticClass: "alert alert-danger"
            },
            [
              _c("span", {
                domProps: { textContent: _vm._s(_vm.error_message) }
              })
            ]
          ),
          _vm._v(" "),
          _c("div", { staticClass: "input-icon" }, [
            _c("i", { staticClass: "fa fa-credit-card" }),
            _vm._v(" "),
            _c("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.prestige_card_no,
                  expression: "prestige_card_no"
                }
              ],
              staticClass: "form-control placeholder-no-fix",
              attrs: { type: "text", placeholder: "XXX-XXX-0000" },
              domProps: { value: _vm.prestige_card_no },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.prestige_card_no = $event.target.value
                }
              }
            })
          ])
        ]
      ),
      _vm._v(" "),
      _c("div", { staticClass: "form-actions" }, [
        _c(
          "a",
          {
            staticClass: "btn red btn-outline",
            attrs: { href: _vm.login_link, type: "button" }
          },
          [_vm._v(" Go to Login ")]
        ),
        _vm._v(" "),
        _c(
          "button",
          {
            staticClass: "btn green pull-right",
            staticStyle: { width: "100px" },
            attrs: { disabled: _vm.on_process, id: "register-submit-btn" },
            on: {
              click: function($event) {
                $event.preventDefault()
                _vm.validateCardNo($event)
              }
            }
          },
          [
            !_vm.on_process
              ? _c("span", [_vm._v("CONTINUE")])
              : _c("i", { staticClass: "fa fa-spinner fa-pulse fa-fw" })
          ]
        )
      ])
    ]
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-928cdac8", module.exports)
  }
}

/***/ }),

/***/ 178:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(3)
/* script */
var __vue_script__ = __webpack_require__(179)
/* template */
var __vue_template__ = __webpack_require__(180)
/* template functional */
  var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/components/registration/personal-info.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {  return key !== "default" && key.substr(0, 2) !== "__"})) {  console.error("named exports are not supported in *.vue files.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-e7c4c046", Component.options)
  } else {
    hotAPI.reload("data-v-e7c4c046", Component.options)
' + '  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 179:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "PersonalInfo",
  props: ["registration_details"],
  mounted: function mounted() {
    // $('#birthdate').datepicker();
    var self = this;
    $(document).on('focus', '.datepicker', function () {
      $("#birthdate").datepicker({
        format: "yyyy-mm-dd"
      }).on('changeDate', function () {
        Vue.set(self.registration_details, "birthdate", $('#birthdate').val());
      });
    });
  },

  methods: {
    showError: function showError(message) {
      Vue.set(this, 'has_error', true);
      Vue.set(this, 'error_message', message);
    },
    proceed: function proceed(scope) {
      var _this = this;

      this.$validator.validateAll(scope).then(function (result) {
        if (result) {
          _this.$emit('proceed', { next_step: 3 });
        }
      });
    },
    go_back: function go_back() {
      this.$emit('go_back', 1);
    }
  }
});

/***/ }),

/***/ 180:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "form",
    {
      attrs: { method: "post", "data-vv-scope": "personal-info" },
      on: {
        submit: function($event) {
          $event.preventDefault()
          _vm.proceed("personal-info")
        }
      }
    },
    [
      _c("h3", [_vm._v("Sign Up")]),
      _vm._v(" "),
      _c("p", [_vm._v(" Enter your personal details below: ")]),
      _vm._v(" "),
      _c(
        "div",
        {
          staticClass: "form-group",
          class: { "has-error": _vm.ferrors.has("personal-info.last_name") }
        },
        [
          _c(
            "label",
            { staticClass: "control-label visible-ie8 visible-ie9" },
            [_vm._v("Last Name")]
          ),
          _vm._v(" "),
          _c("div", { staticClass: "input-icon" }, [
            _c("i", { staticClass: "fa fa-font" }),
            _vm._v(" "),
            _c("input", {
              directives: [
                {
                  name: "validate",
                  rawName: "v-validate",
                  value: "required",
                  expression: "'required'"
                },
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.registration_details.last_name,
                  expression: "registration_details.last_name"
                }
              ],
              staticClass: "form-control placeholder-no-fix",
              attrs: {
                "data-vv-as": "Last Name",
                type: "text",
                placeholder: "Last Name",
                name: "last_name"
              },
              domProps: { value: _vm.registration_details.last_name },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.$set(
                    _vm.registration_details,
                    "last_name",
                    $event.target.value
                  )
                }
              }
            })
          ]),
          _vm._v(" "),
          _c(
            "span",
            {
              directives: [
                {
                  name: "show",
                  rawName: "v-show",
                  value: _vm.ferrors.has("personal-info.last_name"),
                  expression: "ferrors.has('personal-info.last_name')"
                }
              ],
              staticClass: "help-block"
            },
            [
              _vm._v(
                "\n      " +
                  _vm._s(_vm.ferrors.first("personal-info.last_name")) +
                  "\n    "
              )
            ]
          )
        ]
      ),
      _vm._v(" "),
      _c(
        "div",
        {
          staticClass: "form-group",
          class: { "has-error": _vm.ferrors.has("personal-info.first_name") }
        },
        [
          _c(
            "label",
            { staticClass: "control-label visible-ie8 visible-ie9" },
            [_vm._v("First Name")]
          ),
          _vm._v(" "),
          _c("div", { staticClass: "input-icon" }, [
            _c("i", { staticClass: "fa fa-font" }),
            _vm._v(" "),
            _c("input", {
              directives: [
                {
                  name: "validate",
                  rawName: "v-validate",
                  value: "required",
                  expression: "'required'"
                },
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.registration_details.first_name,
                  expression: "registration_details.first_name"
                }
              ],
              staticClass: "form-control placeholder-no-fix",
              attrs: {
                "data-vv-as": "First Name",
                type: "text",
                placeholder: "First Name",
                name: "first_name"
              },
              domProps: { value: _vm.registration_details.first_name },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.$set(
                    _vm.registration_details,
                    "first_name",
                    $event.target.value
                  )
                }
              }
            }),
            _vm._v(" "),
            _c(
              "span",
              {
                directives: [
                  {
                    name: "show",
                    rawName: "v-show",
                    value: _vm.ferrors.has("personal-info.first_name"),
                    expression: "ferrors.has('personal-info.first_name')"
                  }
                ],
                staticClass: "help-block"
              },
              [_vm._v(_vm._s(_vm.ferrors.first("personal-info.first_name")))]
            )
          ])
        ]
      ),
      _vm._v(" "),
      _c(
        "div",
        {
          staticClass: "form-group",
          class: { "has-error": _vm.ferrors.has("personal-info.middle_name") }
        },
        [
          _c(
            "label",
            { staticClass: "control-label visible-ie8 visible-ie9" },
            [_vm._v("Middle Name")]
          ),
          _vm._v(" "),
          _c("div", { staticClass: "input-icon" }, [
            _c("i", { staticClass: "fa fa-font" }),
            _vm._v(" "),
            _c("input", {
              directives: [
                {
                  name: "validate",
                  rawName: "v-validate",
                  value: "required",
                  expression: "'required'"
                },
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.registration_details.middle_name,
                  expression: "registration_details.middle_name"
                }
              ],
              staticClass: "form-control placeholder-no-fix",
              attrs: {
                "data-vv-as": "Middle Name",
                type: "text",
                placeholder: "Middle Name",
                name: "middle_name"
              },
              domProps: { value: _vm.registration_details.middle_name },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.$set(
                    _vm.registration_details,
                    "middle_name",
                    $event.target.value
                  )
                }
              }
            }),
            _vm._v(" "),
            _c(
              "span",
              {
                directives: [
                  {
                    name: "show",
                    rawName: "v-show",
                    value: _vm.ferrors.has("personal-info.middle_name"),
                    expression: "ferrors.has('personal-info.middle_name')"
                  }
                ],
                staticClass: "help-block"
              },
              [_vm._v(_vm._s(_vm.ferrors.first("personal-info.middle_name")))]
            )
          ])
        ]
      ),
      _vm._v(" "),
      _c(
        "div",
        {
          staticClass: "form-group",
          class: { "has-error": _vm.ferrors.has("personal-info.birthdate") }
        },
        [
          _c(
            "label",
            { staticClass: "control-label visible-ie8 visible-ie9" },
            [_vm._v("Birthdate")]
          ),
          _vm._v(" "),
          _c("div", { staticClass: "input-icon" }, [
            _c("i", { staticClass: "fa fa-calendar" }),
            _vm._v(" "),
            _c("input", {
              directives: [
                {
                  name: "validate",
                  rawName: "v-validate",
                  value: "required|date_format:YYYY-MM-DD",
                  expression: "'required|date_format:YYYY-MM-DD'"
                },
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.registration_details.birthdate,
                  expression: "registration_details.birthdate"
                }
              ],
              staticClass: "form-control placeholder-no-fix datepicker",
              attrs: {
                "data-vv-as": "Birthdate",
                type: "text",
                placeholder: "Birthdate",
                id: "birthdate",
                name: "birthdate"
              },
              domProps: { value: _vm.registration_details.birthdate },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.$set(
                    _vm.registration_details,
                    "birthdate",
                    $event.target.value
                  )
                }
              }
            }),
            _vm._v(" "),
            _c(
              "span",
              {
                directives: [
                  {
                    name: "show",
                    rawName: "v-show",
                    value: _vm.ferrors.has("personal-info.birthdate"),
                    expression: "ferrors.has('personal-info.birthdate')"
                  }
                ],
                staticClass: "help-block"
              },
              [_vm._v(_vm._s(_vm.ferrors.first("personal-info.birthdate")))]
            )
          ])
        ]
      ),
      _vm._v(" "),
      _c(
        "div",
        {
          staticClass: "form-group",
          class: { "has-error": _vm.ferrors.has("personal-info.address") }
        },
        [
          _c(
            "label",
            { staticClass: "control-label visible-ie8 visible-ie9" },
            [_vm._v("Address")]
          ),
          _vm._v(" "),
          _c("div", { staticClass: "input-icon" }, [
            _c("i", { staticClass: "fa fa-check" }),
            _vm._v(" "),
            _c("input", {
              directives: [
                {
                  name: "validate",
                  rawName: "v-validate",
                  value: "required",
                  expression: "'required'"
                },
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.registration_details.address,
                  expression: "registration_details.address"
                }
              ],
              staticClass: "form-control placeholder-no-fix",
              attrs: {
                "data-vv-as": "Address",
                type: "text",
                placeholder: "Address",
                name: "address"
              },
              domProps: { value: _vm.registration_details.address },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.$set(
                    _vm.registration_details,
                    "address",
                    $event.target.value
                  )
                }
              }
            }),
            _vm._v(" "),
            _c(
              "span",
              {
                directives: [
                  {
                    name: "show",
                    rawName: "v-show",
                    value: _vm.ferrors.has("personal-info.address"),
                    expression: "ferrors.has('personal-info.address')"
                  }
                ],
                staticClass: "help-block"
              },
              [_vm._v(_vm._s(_vm.ferrors.first("personal-info.address")))]
            )
          ])
        ]
      ),
      _vm._v(" "),
      _c(
        "div",
        {
          staticClass: "form-group",
          class: { "has-error": _vm.ferrors.has("personal-info.occupation") }
        },
        [
          _c(
            "label",
            { staticClass: "control-label visible-ie8 visible-ie9" },
            [_vm._v("Occupation")]
          ),
          _vm._v(" "),
          _c("div", { staticClass: "input-icon" }, [
            _c("i", { staticClass: "fa fa-black-tie" }),
            _vm._v(" "),
            _c("input", {
              directives: [
                {
                  name: "validate",
                  rawName: "v-validate",
                  value: "required",
                  expression: "'required'"
                },
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.registration_details.occupation,
                  expression: "registration_details.occupation"
                }
              ],
              staticClass: "form-control placeholder-no-fix",
              attrs: {
                "data-vv-as": "Occupation",
                type: "text",
                placeholder: "Occupation",
                name: "occupation"
              },
              domProps: { value: _vm.registration_details.occupation },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.$set(
                    _vm.registration_details,
                    "occupation",
                    $event.target.value
                  )
                }
              }
            }),
            _vm._v(" "),
            _c(
              "span",
              {
                directives: [
                  {
                    name: "show",
                    rawName: "v-show",
                    value: _vm.ferrors.has("personal-info.occupation"),
                    expression: "ferrors.has('personal-info.occupation')"
                  }
                ],
                staticClass: "help-block"
              },
              [_vm._v(_vm._s(_vm.ferrors.first("personal-info.occupation")))]
            )
          ])
        ]
      ),
      _vm._v(" "),
      _c(
        "div",
        {
          staticClass: "form-group",
          class: { "has-error": _vm.ferrors.has("personal-info.interest") }
        },
        [
          _c(
            "label",
            { staticClass: "control-label visible-ie8 visible-ie9" },
            [_vm._v("Interest")]
          ),
          _vm._v(" "),
          _c("div", { staticClass: "input-icon" }, [
            _c("i", { staticClass: "fa fa-heart " }),
            _vm._v(" "),
            _c("input", {
              directives: [
                {
                  name: "validate",
                  rawName: "v-validate",
                  value: "required",
                  expression: "'required'"
                },
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.registration_details.interest,
                  expression: "registration_details.interest"
                }
              ],
              staticClass: "form-control placeholder-no-fix",
              attrs: {
                "data-vv-as": "Interest",
                type: "text",
                placeholder: "Interest",
                name: "interest"
              },
              domProps: { value: _vm.registration_details.interest },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.$set(
                    _vm.registration_details,
                    "interest",
                    $event.target.value
                  )
                }
              }
            }),
            _vm._v(" "),
            _c(
              "span",
              {
                directives: [
                  {
                    name: "show",
                    rawName: "v-show",
                    value: _vm.ferrors.has("personal-info.interest"),
                    expression: "ferrors.has('personal-info.interest')"
                  }
                ],
                staticClass: "help-block"
              },
              [_vm._v(_vm._s(_vm.ferrors.first("personal-info.interest")))]
            )
          ])
        ]
      ),
      _vm._v(" "),
      _c(
        "div",
        {
          staticClass: "form-group",
          class: { "has-error": _vm.ferrors.has("personal-info.contact_no") }
        },
        [
          _c(
            "label",
            { staticClass: "control-label visible-ie8 visible-ie9" },
            [_vm._v("Contacts")]
          ),
          _vm._v(" "),
          _c("div", { staticClass: "input-icon" }, [
            _c("i", { staticClass: "fa fa-mobile-phone " }),
            _vm._v(" "),
            _c("input", {
              directives: [
                {
                  name: "validate",
                  rawName: "v-validate",
                  value: { required: true, regex: /^(09|\+639)\d{9}$/ },
                  expression: "{ required: true, regex: /^(09|\\+639)\\d{9}$/ }"
                },
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.registration_details.contact_no,
                  expression: "registration_details.contact_no"
                }
              ],
              staticClass: "form-control placeholder-no-fix",
              attrs: {
                "data-vv-as": "Contact Number",
                type: "text",
                placeholder: "Contact No.",
                name: "contact_no"
              },
              domProps: { value: _vm.registration_details.contact_no },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.$set(
                    _vm.registration_details,
                    "contact_no",
                    $event.target.value
                  )
                }
              }
            }),
            _vm._v(" "),
            _c(
              "span",
              {
                directives: [
                  {
                    name: "show",
                    rawName: "v-show",
                    value: _vm.ferrors.has("personal-info.contact_no"),
                    expression: "ferrors.has('personal-info.contact_no')"
                  }
                ],
                staticClass: "help-block"
              },
              [_vm._v(_vm._s(_vm.ferrors.first("personal-info.contact_no")))]
            )
          ])
        ]
      ),
      _vm._v(" "),
      _c("div", { staticClass: "form-actions" }, [
        _c(
          "button",
          {
            staticClass: "btn red btn-outline",
            attrs: { type: "button" },
            on: {
              click: function($event) {
                $event.preventDefault()
                _vm.go_back()
              }
            }
          },
          [_vm._v(" Back ")]
        ),
        _vm._v(" "),
        _c(
          "button",
          { staticClass: "btn green pull-right", attrs: { type: "submit" } },
          [_vm._v(" Continue ")]
        )
      ])
    ]
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-e7c4c046", module.exports)
  }
}

/***/ }),

/***/ 181:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(3)
/* script */
var __vue_script__ = __webpack_require__(182)
/* template */
var __vue_template__ = __webpack_require__(183)
/* template functional */
  var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/components/registration/account-details.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {  return key !== "default" && key.substr(0, 2) !== "__"})) {  console.error("named exports are not supported in *.vue files.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-4d62c738", Component.options)
  } else {
    hotAPI.reload("data-v-4d62c738", Component.options)
' + '  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 182:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "AccountDetails",
  props: ['registration_details', 'on_process', 'show_alert', 'is_success', 'message', 'errors'],
  methods: {
    signup: function signup() {
      this.$emit('signup', {});
    },
    go_back: function go_back() {
      this.$emit('go_back', 2);
    }
  }
});

/***/ }),

/***/ 183:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("form", { attrs: { action: "index.html", method: "post" } }, [
    _vm.show_alert
      ? _c("div", { staticClass: "alert alert-danger" }, [
          _c(
            "ul",
            [
              _vm._l(_vm.errors, function(error) {
                return _vm._l(error, function(value) {
                  return _c("li", [
                    _c("span", { domProps: { textContent: _vm._s(value) } })
                  ])
                })
              })
            ],
            2
          )
        ])
      : _vm._e(),
    _vm._v(" "),
    _c("p", [_vm._v(" Enter your account details below: ")]),
    _vm._v(" "),
    _c("div", { staticClass: "form-group" }, [
      _c("label", { staticClass: "control-label visible-ie8 visible-ie9" }, [
        _vm._v("Username")
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "input-icon" }, [
        _c("i", { staticClass: "fa fa-user" }),
        _vm._v(" "),
        _c("input", {
          directives: [
            {
              name: "model",
              rawName: "v-model",
              value: _vm.registration_details.email,
              expression: "registration_details.email"
            }
          ],
          staticClass: "form-control placeholder-no-fix",
          attrs: {
            type: "text",
            autocomplete: "off",
            placeholder: "Email",
            name: "email"
          },
          domProps: { value: _vm.registration_details.email },
          on: {
            input: function($event) {
              if ($event.target.composing) {
                return
              }
              _vm.$set(_vm.registration_details, "email", $event.target.value)
            }
          }
        })
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "form-group" }, [
      _c("label", { staticClass: "control-label visible-ie8 visible-ie9" }, [
        _vm._v("Password")
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "input-icon" }, [
        _c("i", { staticClass: "fa fa-lock" }),
        _vm._v(" "),
        _c("input", {
          directives: [
            {
              name: "model",
              rawName: "v-model",
              value: _vm.registration_details.password,
              expression: "registration_details.password"
            }
          ],
          staticClass: "form-control placeholder-no-fix",
          attrs: {
            type: "password",
            autocomplete: "off",
            id: "register_password",
            placeholder: "Password",
            name: "password"
          },
          domProps: { value: _vm.registration_details.password },
          on: {
            input: function($event) {
              if ($event.target.composing) {
                return
              }
              _vm.$set(
                _vm.registration_details,
                "password",
                $event.target.value
              )
            }
          }
        })
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "form-group" }, [
      _c("label", { staticClass: "control-label visible-ie8 visible-ie9" }, [
        _vm._v("Re-type Your Password")
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "controls" }, [
        _c("div", { staticClass: "input-icon" }, [
          _c("i", { staticClass: "fa fa-check" }),
          _vm._v(" "),
          _c("input", {
            directives: [
              {
                name: "model",
                rawName: "v-model",
                value: _vm.registration_details.password_confirmation,
                expression: "registration_details.password_confirmation"
              }
            ],
            staticClass: "form-control placeholder-no-fix",
            attrs: {
              type: "password",
              autocomplete: "off",
              placeholder: "Re-type Your Password",
              name: "rpassword"
            },
            domProps: { value: _vm.registration_details.password_confirmation },
            on: {
              input: function($event) {
                if ($event.target.composing) {
                  return
                }
                _vm.$set(
                  _vm.registration_details,
                  "password_confirmation",
                  $event.target.value
                )
              }
            }
          })
        ])
      ])
    ]),
    _vm._v(" "),
    _vm._m(0),
    _vm._v(" "),
    _c("div", { staticClass: "form-actions" }, [
      _c(
        "button",
        {
          staticClass: "btn red btn-outline",
          attrs: { disabled: _vm.on_process, type: "button" },
          on: {
            click: function($event) {
              $event.preventDefault()
              _vm.go_back()
            }
          }
        },
        [_vm._v(" Back ")]
      ),
      _vm._v(" "),
      _c(
        "button",
        {
          staticClass: "btn green pull-right",
          staticStyle: { width: "100px" },
          attrs: { type: "submit", disabled: _vm.on_process },
          on: {
            click: function($event) {
              $event.preventDefault()
              _vm.signup($event)
            }
          }
        },
        [
          !_vm.on_process
            ? _c("span", [_vm._v("SIGN UP")])
            : _c("i", { staticClass: "fa fa-spinner fa-pulse fa-fw" })
        ]
      )
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "form-group" }, [
      _c("label", { staticClass: "mt-checkbox mt-checkbox-outline" }, [
        _c("input", { attrs: { type: "checkbox", name: "tnc" } }),
        _vm._v(" I agree to the\n      "),
        _c("a", { attrs: { href: "javascript:;" } }, [
          _vm._v("Terms of Service ")
        ]),
        _vm._v(" &\n      "),
        _c("a", { attrs: { href: "javascript:;" } }, [
          _vm._v("Privacy Policy ")
        ]),
        _vm._v(" "),
        _c("span")
      ]),
      _vm._v(" "),
      _c("div", { attrs: { id: "register_tnc_error" } })
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-4d62c738", module.exports)
  }
}

/***/ }),

/***/ 184:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(3)
/* script */
var __vue_script__ = __webpack_require__(185)
/* template */
var __vue_template__ = __webpack_require__(186)
/* template functional */
  var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/components/registration/note-success.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {  return key !== "default" && key.substr(0, 2) !== "__"})) {  console.error("named exports are not supported in *.vue files.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-257d77d6", Component.options)
  } else {
    hotAPI.reload("data-v-257d77d6", Component.options)
' + '  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 185:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "NoteSuccess",
  props: ['message']
});

/***/ }),

/***/ 186:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("h3", [_vm._v("Thank you for signing up!")]),
    _vm._v(" "),
    _c("p", { domProps: { textContent: _vm._s(_vm.message) } })
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-257d77d6", module.exports)
  }
}

/***/ }),

/***/ 3:
/***/ (function(module, exports) {

/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file.
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier /* server only */
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = injectStyles
  }

  if (hook) {
    var functional = options.functional
    var existing = functional
      ? options.render
      : options.beforeCreate

    if (!functional) {
      // inject component registration as beforeCreate hook
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    } else {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functioal component in vue file
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return existing(h, context)
      }
    }
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ })

/******/ });