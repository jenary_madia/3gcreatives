<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/about', 'HomeController@about')->name('about');
Route::get('/business-opportunities', 'HomeController@services')->name('business-opportunities');
Route::get('/contact', 'HomeController@contact_us')->name('contact');
Route::get('/faq', 'HomeController@faq')->name('faq');
Route::get('/perks-rewards', 'HomeController@perksRewards')->name('perks-rewards');
Route::get('/visa-consultation', 'HomeController@visaConsultation');
Route::post('/contact', 'HomeController@contact')->name('contact');

Route::post('/validate-prestige-card', 'Api\DataController@validatePrestigeCard');

Route::middleware(['auth'])->group(function () {
    Route::get('/dashboard', 'DashboardController@index');

    // API Endpoint
    Route::prefix('api/v1')->group(function () {
        Route::get('/user/profile', 'Api\UserController@getUserProfile');
        Route::post('/user/profile', 'Api\UserController@updateUserProfileInfo');
        Route::post('/user/profile/current-password', 'Api\UserController@getCurrentPassword');
        Route::post('/user/profile/save-new-password', 'Api\UserController@saveNewPassword');
    });

    // User
    Route::prefix('user')->group(function () {
        Route::get('/profile', 'UserController@index')->name('user.profile');
        Route::get('/list', 'UserController@list')->middleware('isAdmin');
        Route::get('/new', 'UserController@new')->middleware('isAdmin');
        Route::post('/new', 'Api\UserController@new');
    });

    // Client
    Route::prefix('client')->group(function () {
        Route::get('/list', 'ClientController@index')->middleware('isAdmin');
    });

    // Transaction
    Route::prefix('transaction')->group(function () {
        Route::get('/list/{client_id?}', 'TransactionsController@transactions')
            ->name('transaction')
            ->middleware('isAdmin');
        Route::get('/new', 'TransactionsController@new')->middleware('isAdmin');
        Route::post('/new', 'Api\TransactionsController@addTransaction');
        Route::post('/update', 'Api\TransactionsController@update');
        Route::post('/rollback', 'Api\TransactionsController@rollback');
        Route::get('/{id}', 'TransactionsController@show')
            ->name('transaction.show')
            ->middleware('isAdmin');
        Route::get('/edit/{transaction_id}', 'TransactionsController@edit')->name('transaction.edit');

    });
});
