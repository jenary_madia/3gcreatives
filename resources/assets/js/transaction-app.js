import addTransaction from './components/transaction/add-transaction.vue';
import editTransaction from './components/transaction/edit-transaction.vue';
import 'vuejs-noty/dist/vuejs-noty.css'
export const transactionApp = new Vue({
    el : '#div-transaction',
    components: {
      addTransaction: addTransaction,
      editTransaction: editTransaction,
    },
    data: {
        step: 1
    },
    mounted() {
        $('#transaction-list').DataTable({
            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ records",
                "infoEmpty": "No records found",
                "infoFiltered": "(filtered1 from _MAX_ total records)",
                "lengthMenu": "Show _MENU_",
                "search": "Search:",
                "zeroRecords": "No matching records found",
                "paginate": {
                    "previous":"Prev",
                    "next": "Next",
                    "last": "Last",
                    "first": "First"
                }
            },

            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"] // change per page values here
            ],
            "aaSorting" : [[0, 'desc']],
            "pageLength": 5,
            "pagingType": "bootstrap_full_number",
            "columnDefs": [
                {
                    "className": "dt-right",
                    //"targets": [2]
                }
            ],
        });
    },
    methods : {
        updateStep(data) {
            Vue.set(this,'step',data);
        },

        rollback(reference_no) {
            var self = this;
            alertify.confirm('Confirmation','Are you sure you want to rollback this transaction?', function(){
                axios.post(base_url+'/transaction/rollback', {
                    reference_no : reference_no
                }, { headers: {
                  "Cache-Control": "no-cache, no-store, must-revalidate",
                  "Pragma": "no-cache",
                  "Expires": "0",
                  "dataType": "json"
                }})
                .then(function (response) {
                    self.$noty.success('Transaction successfully rolled back', {
                        theme: 'mint',
                        killer: true,
                        timeout: 3000,
                        layout: 'topRight'
                    });
                    setTimeout(function(){
                        location.reload();
                    }, 1000)
                })
                .catch(function (error) {
                    console.log(error);
                });
            },function(){

            });
        }
    }
});
