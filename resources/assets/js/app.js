/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
const moment = require('moment');
global.alertify = require('alertifyjs');
window.Vue = require('vue');
import VeeValidate from 'vee-validate'
import VueMask from 'v-mask'
import VueNoty from 'vuejs-noty'

const config = { errorBagName: 'ferrors' }
Vue.use(VeeValidate, config)
Vue.use(VueMask)
Vue.use(VueNoty)

let base_url = document.head.querySelector('meta[name="base-url"]');

if (base_url) {
    window.base_url = base_url.content;
} else {
    console.error('BASE URL NOT FOUND');
}

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Vue.component('example-component', require('./components/ExampleComponent.vue'));

// const app = new Vue({
//     el: '#app'
// });
