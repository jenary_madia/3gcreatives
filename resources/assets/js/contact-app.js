import axios from 'axios'
import swal from 'sweetalert'

export const contactApp = new Vue({
  el: '#contact-container',

  data: {
    contact_form: {
      full_name: '',
      email: '',
      phone: '',
      body_message: ''
    }
  },

  computed: {
    isAllFieldsAreEmpty () {
      return this.contact_form.full_name == '' ||
              this.contact_form.email == '' ||
              this.contact_form.phone == '' ||
              this.contact_form.body_message == '' ||
              this.contact_form.body_message.length < 10
    },
  },

  methods: {
    validateBeforeSubmit () {
      var self = this;

      this.$validator.validateAll()
        .then((response) => {
          this.sendMessage()
          this.clearInputFields()
        })
        .catch(function(e) {
          // Catch errors
        })
    },

    clearInputFields () {
      this.contact_form.full_name == ''
      this.contact_form.email == ''
      this.contact_form.phone == ''
      this.contact_form.body_message == ''

      this.$nextTick(() => {
        this.ferrors.clear()
      })
    },

    sendMessage () {
      axios.post(base_url + '/contact', this.contact_form)
        .then((response) => {
          if (response.data.status) {
            swal({
              title: response.data.message,
              text: 'Successfull',
              icon: 'success'
            })
          }

          this.contact_form.full_name = ''
          this.contact_form.email = ''
          this.contact_form.phone = ''
          this.contact_form.body_message = ''
        })
        .catch(function (error) {
          console.log(error.message)
        })
      }
  },
})
