import PrestigeCard from './components/registration/prestige-card';
import PersonalInfo from './components/registration/personal-info';
import AccountDetails from './components/registration/account-details';
import NoteSuccess from './components/registration/note-success';

export const registrationApp = new Vue({
  el: '#registration-app',

  components: {
    PrestigeCard,
    PersonalInfo,
    AccountDetails,
    NoteSuccess
  },

  data: {
    step: 1,
    registration_details: {
      prestige_card: "",
      first_name: "",
      middle_name: "",
      last_name: "",
      birthdate: "",
      address: "",
      occupation: "",
      interest: "",
      contact_no: "",
      email: "",
      password: "",
      password_confirmation: ""
    },
    on_process: false,
    show_alert: false,
    message: "",
    registration_errors: {}
  },

  methods: {
    proceed(response) {
      Vue.set(this, 'step', response.next_step);
      if (response.next_step == 2) {
        Vue.set(this.registration_details, 'prestige_card', response.prestige_card);
      }
    },

    signup() {
      let self = this;
      Vue.set(this, 'on_process', true);
      axios.post('/register', this.registration_details)
      .then(response => {
          console.log(response.data);
          console.log(typeof response.data);
          let response_data = response.data;
          if (response_data.SUCCESS) {
            self.showMessage(1, response_data.MESSAGE);
          } else {
            self.showMessage(2, '', response_data.ERRORS);
            console.log(response_data.ERRORS);
          }
          Vue.set(this, 'on_process', false);
      })
      .catch(error => {
        Vue.set(this, 'on_process', false);
          self.showError(error.message);
      });
    },

    showMessage(type,message,errors = []) {
      if (type == 1) {  // success
        Vue.set(this, "message", message);
        Vue.set(this, "step", 4);
      } else {
        Vue.set(this, "show_alert", true);
        Vue.set(this, "registration_errors", errors);
      }
    },

    go_back(step) {
      Vue.set(this, "step", step);
    }
  },

  mounted() {

  }
})
