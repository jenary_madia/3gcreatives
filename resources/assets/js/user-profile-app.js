import userProfile from './components/profile/index.vue';

export const userProfileApp = new Vue({
    el: '#profile-container',
    components: {
        userProfile: userProfile
    }
});