@extends('layouts.auth-app')

@section('content')

    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
        {{ csrf_field() }}
        <h3 class="form-title">Login to your account</h3>
        @if (session('confirmation-success'))
            <div class="alert alert-success">
                {{ session('confirmation-success') }}
            </div>
        @endif
        @if (session('confirmation-danger'))
            <div class="alert alert-danger">
                {!! session('confirmation-danger') !!}
            </div>
        @endif
        <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
            <label class="control-label visible-ie8 visible-ie9">Email</label>
            <div class="input-icon">
                <i class="fa fa-user"></i>
                <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" value="{{ old('email') }}" name="email" /> 
                @if ($errors->has('email'))
                    <span class="help-block">{{ $errors->first('email') }}</span>
                @endif
            </div>
        </div>
        <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
            <label class="control-label visible-ie8 visible-ie9">Password</label>
            <div class="input-icon">
                <i class="fa fa-lock"></i>
                <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password" /> 
                @if ($errors->has('password'))
                    <span class="help-block">{{ $errors->first('password') }}</span>
                @endif
            </div>
        </div>
        <div class="form-actions">
            <button type="submit" class="btn green pull-right"> Login </button>
        </div>
        <div class="forget-password">
            <h4>Forgot your password ?</h4>
            <p> no worries, click
                <a href="{!! url('/password/reset') !!}" id="forget-password"> here </a> to reset your password. </p>
        </div>
         <div class="create-account">
            <p> Don't have an account yet ?&nbsp;
                <a href="{!! url('/register') !!}"> Create an account </a>
            </p>
        </div>
    </form>
@endsection