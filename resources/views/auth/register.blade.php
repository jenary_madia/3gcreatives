@extends('layouts.auth-app')

@section('content')
    <div id="registration-app">
        <!-- BEGIN PRESTIGE NUMBER -->
        <prestige-card
            v-if="step == 1"
            v-on:proceed="proceed"
        ></prestige-card> 
        <!-- END PRESTIGE NUMBER -->
        <!-- PERSONAL INFORMATION -->
        <personal-info
            :registration_details = "registration_details"
            v-if="step == 2"
            v-on:proceed="proceed"
            v-on:go_back = "go_back"
        ></personal-info>
        <!-- END PERSONAL INFORMATION -->
        <!-- BEGIN ACCOUNT DETAILS -->
        <account-details
            :registration_details = "registration_details"
            :show_alert = "show_alert"
            :errors = "registration_errors"
            :on_process = "on_process"
            v-on:signup = "signup"
            v-on:go_back = "go_back"
            v-if="step == 3"
        ></account-details>
        <!-- END ACCOUNT DETAILS -->
        <!-- START NOTE SUCCESS DETAILS FORM -->
        <note-success
            :is_success = "is_success"
            :message = "message"
            v-if="step == 4"
        ></note-success>
        <!-- END NOTE SUCCESS DETAILS FORM -->
    </div>
@endsection
@section('scripts_per_module')
    <script>
        window.login_link = "{!! url('login') !!}";
    </script>
    <script src="{!! asset('js/registration-app.js') !!}"></script>
@endsection