<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>3gcreatives Travel &amp; Tours</title>
        <link rel="apple-touch-icon-precomposed" sizes="57x57" href="{!! asset('apple-touch-icon-57x57.png') !!}" />
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{!! asset('apple-touch-icon-114x114.png') !!}" />
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{!! asset('apple-touch-icon-72x72.png') !!}" />
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{!! asset('apple-touch-icon-144x144.png') !!}" />
        <link rel="apple-touch-icon-precomposed" sizes="60x60" href="{!! asset('apple-touch-icon-60x60.png') !!}" />
        <link rel="apple-touch-icon-precomposed" sizes="120x120" href="{!! asset('apple-touch-icon-120x120.png') !!}" />
        <link rel="apple-touch-icon-precomposed" sizes="76x76" href="{!! asset('apple-touch-icon-76x76.png') !!}" />
        <link rel="apple-touch-icon-precomposed" sizes="152x152" href="{!! asset('apple-touch-icon-152x152.png') !!}" />
        <link rel="icon" type="image/png" href="{!! asset('favicon-196x196.png" sizes="196x196') !!}" />
        <link rel="icon" type="image/png" href="{!! asset('favicon-96x96.png" sizes="96x96') !!}" />
        <link rel="icon" type="image/png" href="{!! asset('favicon-32x32.png" sizes="32x32') !!}" />
        <link rel="icon" type="image/png" href="{!! asset('favicon-16x16.png" sizes="16x16') !!}" />
        <link rel="icon" type="image/png" href="{!! asset('favicon-128.png" sizes="128x128') !!}" />
        <meta name="application-name" content="&nbsp;"/>
        <meta name="msapplication-TileColor" content="#FFFFFF" />
        <meta name="msapplication-TileImage" content="{!! asset('mstile-144x144.png') !!}" />
        <meta name="msapplication-square70x70logo" content="{!! asset('mstile-70x70.png') !!}" />
        <meta name="msapplication-square150x150logo" content="{!! asset('mstile-150x150.png') !!}" />
        <meta name="msapplication-wide310x150logo" content="{!! asset('mstile-310x150.png') !!}" />
        <meta name="msapplication-square310x310logo" content="{!! asset('mstile-310x310.png') !!}" />
         <!--Bootstrap Style Sheet-->
        <link rel="stylesheet" href="{!! asset('landing-page/bootstrap/css/bootstrap.css') !!}">
        <!--Font Awesome-->
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
        <!--Custom Style Sheet-->
        <link rel="stylesheet" href="{!! asset('landing-page/css/style.min.css') !!}">
        <!--loader Style Sheet-->
        <link href="{!! asset('landing-page/css/loader.min.css') !!}" rel="stylesheet" type="text/css">
        <!--loader Style Sheet-->
        <link href="{!! asset('landing-page/css/animations.min.css') !!}" rel="stylesheet" type="text/css">
        <!--Datepicker Style Sheet-->
        <link rel="stylesheet" href="{!! asset('landing-page/css/datepicker.min.css') !!}" type="text/css">
        <!--Custom Style Sheet-->
        <link rel="stylesheet" href="{!! asset('landing-page/css/custom.css') !!}" type="text/css">
        <!--Updates Style Sheet-->
        <link rel="stylesheet" href="{!! asset('landing-page/css/updates.css') !!}" type="text/css">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>

    <body style="overflow: auto;">
        <!-- Loader -->
        <div id="preloader-blue" style="display: none;">
            <div id="status-blue">
                <div class="loading-blue">
                    <div class="bullet-blue"></div>
                </div>
            </div>
        </div>
        <!-- End of Loader -->

        <!--Header Section Start-->
        <header class="header-menu" style="height: 662px;">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-xs-12">
                        <div class="header-top clearfix" style="opacity: 1; top: 0px;">
                            <div class="logo">
                                <a href="{{ route('home') }}">
                                    <img src="{!! asset('images/logo-small.png') !!}" class="img-responsive" alt="">
                                </a>
                            </div>
                            <div class="toll">
                                <a style="color: #fff; margin-right: 20px;" href="{{ route('login') }}">Login</a>
                                <a style="color: #fff; margin-right: 20px;" href="{{ route('register') }}">Sign Up</a>
                                <a style="color: #fff; margin-right: 20px;" href="{{ route('about') }}">About</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="banner-outer ">
                <div class="banner-content">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                <div class="banner-text-2">
                                    <div class="section-heading">
                                        <h2>Who <span style="opacity: 1; top: 0px;">We Are</span></h2>
                                    </div>
                                </div>
                                <div class="scrolldown" style="opacity: 1; top: 0px;">
                                    <a href="#overview-section" title="Scroll Down">
                                        <i class="glyphicon glyphicon-chevron-down"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!--Header Section End-->

        <!--What We Do Section-->
        <section id="overview-section">
            <div class="section-heading">
                <h2>Over<span>view</span></h2>
            </div>
            <div class="container">
                <div class="row">
                   <div class="col-lg-12 col-sm-12 col-xs-12">
                        <div class="overview-inner">
                            <div class="overview-content-wrapper">
                                <p>
                                    <i><strong>3G CREATIVES TRAVEL AND TOURS</strong> is peculiar for its friendly operators/desk officers, creative tour packages, innovative online platform system and a touch for open communicationline thru feedbacks on SMS, FB or via VOICE CALL to address all concerns of CLIENT AND WOULD BE CLIENT. The company has the knowledge and expertise how to deal with the budget of client whether it's a BACKPACKER, LEISURE TRIP, BUSINESS, ADVENTURE, HONEYMOONERS AND ELITE VIPs. The company has PRICE MATCH GUARANTEE.</i>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--Testimonial Section End-->

        <!--Our Clients Section-->
        <section id="company-profile-section">
            <div class="section-heading">
                <h2>Company <span>Profile</span></h2>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-sm-12 col-xs-12">
                        <div class="company-profile">
                            <div class="profile-wrapper">
                               <p>
                                  <strong>3G CREATIVES TRAVEL AND TOURS</strong> is the new trend in the tourism industry
                                  wherein the client can be creative in choosing the tour packages client's wants on flexible
                                  terms of having a complete itenerary tour package, hotel accommodations, airfare e-ticket or
                                  even land arrangements on theme parks in a matter of LESS THAN AN HOUR. This innovation is a
                                  challenge for the company to better serve the travel junkies of the millennium that all demands
                                  must be in just a snap of a finger or just in a blink of an eye; the fast movements of technology.
                               </p>

                               <br>

                                <p>
                                    This is made possible because of our software technology and partners with award winning online
                                    platforms travel designer group that engaged on wholesale tour packages. This can guarantee of
                                    affordable yet quality accommodations and tours. Some of globally recognized affiliates of the
                                    company are Asiatravel, Global International B2B system, Via.com, IATI, Abacus and other third
                                    party booking websites. The owner is also a member of one of the most  prestigious timeshare
                                    club membership of resorts and hotel in the Philippines namely Astoria Vacation and Leisure Club Inc.
                                    (AVLCI) and the world's most renounced time share of global resort and hotel club membership,
                                    The RCI. More so, the company has direct connections to tour.
                                </p>

                                <br>

                                <p>
                                    operators on top-finest local destinatination such as Boracay, Bohol, Baguio, Palawan, Baler, Quezon &amp; Bicol.
                                </p>

                                <br>

                                <p>
                                    The company is established in 2016 basically in online. But due to frauds and scams online, the owner
                                    decided to open an office at Puregold Building near PAGCOR CASINO in Guiguinto Bulacan and soon to open
                                    satellite offices in the progressive towns of Bulacan Province and the rest of Central Region.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="faq-section">
            <div class="section-heading">
                <h2>Frequently Ask<span>ed Questions</span></h2>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-xs-12">
                        <div class="faq-wrapper">
                            <h4>
                                <strong>Why should i book a flight with you whereas i can book airfare ticket online directly on their websites?</strong>
                            </h4>
                            <p>
                               3G CREATIVES TRAVEL &amp; TOURS has an advanced knowledge and expertise that online booking cannot do. First is the great savings you will get because we have partnership to wholesale tour operators. Second is that we offer additional benefits that online booking has none. Third is that we shield your tour with a hustle-free land arrangements with free meals, van transfers and unforgettable hospitality. We also assure our clients or would be client for a PRICE MATCH GUARANTEE in comparison to competitors quotation.
                            </p>
                        </div>

                        <div class="faq-wrapper">
                            <h4>
                                <strong>Why should i book a flight with you whereas i can book airfare ticket online directly on their websites?</strong>
                            </h4>
                            <p>
                               3G CREATIVES TRAVEL &amp; TOURS are better connected on access of discounted rate as compared to other website for public viewing. We can offer great deals either domestic and most especially international airlines companies.
                            </p>
                        </div>

                        <div class="faq-wrapper">
                            <h4><strong>Why should i book a hotel accommodation with you where as i can book through other booking websites?</strong></h4>
                            <p>
                                3G CREATIVES TRAVEL &amp; TOURS know the hotels better and give client better deals and benefits. It's because we have more inventories of rooms, no black out dates, less hike on premium holidays or weeks. We do profiling to better understand our client.
                            </p>
                        </div>

                        <div class="faq-wrapper">
                            <h4><strong>Why avail e-voucher for theme park and land arrangement tours with you instead of availing those hotels conceirge.</strong></h4>
                            <p>
                                3G CREATIVES TRAVEL &amp; TOURS has direct contact in the industry connections. We can offer you an array of tour attractions that can easily be paid to us without any hidden charges or even unexpected inconsideration of ground transportation, time constraint in falling in line before entering the park. We have seperate kiosk to avoid those.
                            </p>
                        </div>

                        <div class="faq-wrapper">
                            <h4><strong>How long is your holding period forward a complete tour package?</strong></h4>
                            <p>
                                3G CREATIVES TRAVEL &amp; TOURS Basically offers (5) days holding period. and for the airfare is always subject to change &amp; availability. For safe keeping of target dates. We recommend a 24 Hours Book &amp; Buy scheme.
                            </p>
                        </div>
                    </div>

                    <div class="col-sm-6 col-xs-12">
                        <div class="faq-wrapper" id="second-batch">
                            <h4><strong>What are the availabile mode of payment?</strong></h4>
                            <p>
                                3G CREATIVES TRAVEL &amp; TOURS, accept payment tru BDO &amp; Metrobank Account - online deposit &amp; bank transfers, however, we suggest to visit us on our office at FSI Puregold Building, Guiguinto, Bulacan.
                            </p>
                        </div>

                        <div class="faq-wrapper">
                            <h4><strong>Do you have senior citizen discount?</strong></h4>
                            <p>
                                3G CREATIVES TRAVEL &amp; TOURS have 10% senior citizen discount only on domestic airlines while 500Php. off for a complete tour arrangement.
                            </p>
                        </div>

                        <div class="faq-wrapper">
                            <h4><strong>Are you open for refunds, cancellation or scheduling of travel?</strong></h4>
                            <p>
                                Change of dates due to force majure of domestic travel only is applciable. It should be announced publicly a subject for flight availability.
                            </p>
                        </div>

                        <div class="faq-wrapper">
                            <h4><strong>What tourist destinations are you offering? are their rates competitive?</strong></h4>
                            <p>
                                3G CREATIVES TRAVEL &amp; TOURS offers local tours from Batanes to Surigao, from Hongkong to any part of Asia, Elite tours like, US, Australia, Europe &amp; Maldives and even land tours. like Baguio, Baler, Bicol &amp; other Landmarks in Luzon. We can guarantee the competitiveness rate even compared to travel agencies in Metro Manila.

                            </p>
                        </div>

                        <div class="faq-wrapper">
                            <h4><strong>What is your edge over the other travel agency? </strong></h4>
                            <p>
                                Aside from great value for money and savings we render our clients with personal touch that can customize your travel creative demands. you assured of meaningful trips. Most importantly we have several satellite offices to cater client's accessibility of our affordable travel requirements. We have global affilliations and wholesale suppliers direct here and around the globe.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!--Jquery-->
        <script src="{!! asset('landing-page/js/jquery-2.1.1.min.js') !!}"></script>
        <!--Custom Jquery Scripts-->
        <script src="{!! asset('landing-page/js/modernizr.js') !!}"></script>
        <!--Bootstrap Jquery-->
        {{-- <script src="{!! asset('landing-page/bootstrap/js/bootstrap.js') !!}"></script> --}}
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <!-- Jquery.simplr.jquery.validate.js -->
        <script type="text/javascript" src="{!! asset('landing-page/js/jquery.validate.min.js') !!}"></script>
        <!-- Jquery.simplr.jquery-validate.bootstrap-tooltip.js -->
        <script type="text/javascript" src="{!! asset('landing-page/js/jquery-validate.bootstrap-tooltip.min.js') !!}"></script>
        <!-- Jquery.waypoints.js -->
        <script type="text/javascript" src="{!! asset('landing-page/js/jquery.waypoints.min.js') !!}"></script>
        <!-- Jquery.mousewheel.min.js -->
        <script src="{!! asset('landing-page/js/jquery.mousewheel.min.js') !!}" type="text/javascript" charset="utf-8"></script>
        <!-- Datepicker.js -->
        <script src="{!! asset('landing-page/js/bootstrap-datepicker.min.js') !!}"></script>
        <!-- Custom.js -->
        <script type="text/javascript" src="{!! asset('landing-page/js/custom.js') !!}"></script>
    </body>
</html>
