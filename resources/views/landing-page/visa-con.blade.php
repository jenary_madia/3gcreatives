@extends('layouts.landing-page')
@section('content')
    <style>
        .service-ul li {
            list-style-type: circle !important;
        }
    </style>
    <div class="row">
        <div class="col-md-12 mx-auto">
    
            
            <h2 class="medium question">
                <strong>
                    AUSTRALIA VISA  
                </strong>
            </h2>
            <p class="medium question">
                <strong>
                    Ways to Apply
                </strong>
            </p>
            <ul class="service-ul">
                <li>Determine your eligibility</li>
                <li>Tri-G Travel and Tours will help you to check your eligibility by profile assessment.</li>
                <li> Record your personal reference code</li>
                <li>A Tri-G Travel agent will guide you on how to duly accomplished the visa application form.</li>
                <li>Gather and submit all the documents</li>
                <li>Tri-G Travel and Tours will facilitate all you travel requirements and personal documents for a higher success on visa grant.</li>
                <li>Pay the fee</li>
                <li>Tri-G Travel and Tours will be in charge for your interview appointment and processing of payment in the embassy.</li>
            </ul>
        
            <p class="medium question">
                <strong>
                    Inclusions:
                </strong>
            </p>

            <li>Client Profile Assessment</li>
            <li>Documentary Assistance</li>
            <li>Visa Coaching</li>
            <li>Scheduling of Visa Interview Appointment</li>
            <li>Visa Fee</li>
            <li>Document Handling Service</li>
            <p class="medium question">
                <strong>
                    Payment: Php 18,500.00
                </strong>
            </p>
            <p class="medium question">
                <strong>
                    GENERAL REQUIREMENTS
                </strong>
            </p>

            <ul class="service-ul">
                <li>1. Duly accomplish visa application form (hand written, all capital, do not leave blank spaces, write N/A if no answer)</li>
                <li>2. Philippine Passport valid at least six months prior to scheduled visa appointment</li>
                <li>3. Valid Government issued Identification Card</li>
                <li>4. Proof of income</li>
                <li>5. Proof of sufficient funds</li>
                <li>6. PSA Certificate of Live of Birth</li>
                <li>7. PSA Certificate of Marriage (if married)</li>
            </ul>
            
            <hr>

    
            <h2 class="medium question">
                <strong>
                    CANADIAN VISA  
                </strong>
            </h2>

            <p class="medium question">
                <strong>
                    Ways to Apply
                </strong>
            </p>
            <li>Determine your eligibility</li>
            <li>Tri-G Travel and Tours will help you to check your eligibility by profile assessment.</li>

            <li>Record your personal reference code</li>
            <li>A Tri-G Travel agent will guide you on how to duly accomplished the visa application form.</li>
            <li>Gather and submit all the documents</li>
            <li>Tri-G Travel and Tours will facilitate all you travel requirements and personal documents for a higher success on visa grant.</li>
            <li>Pay the fee</li>
            <li>Tri-G Travel and Tours will be in charge for your interview appointment and processing of payment in the embassy.</li>

            <p class="medium question">
                <strong>
                    Inclusions:
                </strong>
            </p>
            <li>Client Profile Assessment</li>
            <li>Documentary Assistance</li>
            <li>Visa Coaching</li>
            <li>Scheduling of Visa Interview Appointment</li>
            <li>Visa Fee</li>
            <li>Document Handling Service</li>
            <p class="medium question">
                <strong>
                    Payment: Php 12,500.00
                </strong>
            </p>
            <p class="medium question">
                <strong>
                    GENERAL REQUIREMENTS
                </strong>
            </p>
            <li>1. Duly accomplish visa application form (hand written, all capital, do not leave blank spaces, write N/A if no answer)</li>
            <li>2. Philippine Passport valid at least six months prior to scheduled visa appointment</li>
            <li>3. Valid Government issued Identification Card</li>
            <li>4. Proof of income</li>
            <li>5. Proof of sufficient funds</li>
            <li>6. PSA Certificate of Live of Birth</li>
            <li>7. PSA Certificate of Marriage (if married)</li>

            <hr>

            <h2 class="medium question">
                <strong>
                    CHINA VISA  
                </strong>
            </h2>

            <p class="medium question">
                <strong>
                    Ways to Apply
                </strong>
            </p>
            <li>Determine your eligibility</li>
            <li>Tri-G Travel and Tours will help you to check your eligibility by profile assessment.</li>

            <li> Record your personal reference code</li>
            <li>A Tri-G Travel agent will guide you on how to duly accomplished the visa application form.</li>

            <li>Gather and submit all the documents</li>
            <li>Tri-G Travel and Tours will facilitate all you travel requirements and personal documents for a higher success on visa grant.</li>

            <li>Pay the fee</li>
            <li>Tri-G Travel and Tours will be in charge for your interview appointment and processing of payment in the embassy.</li>

            <p class="medium question">
                <strong>
                    Inclusions:
                </strong>
            </p>
            <li>Client Profile Assessment</li>
            <li>Documentary Assistance</li>
            <li>Visa Coaching</li>
            <li>Scheduling of Visa Interview Appointment</li>
            <li>Visa Fee</li>
            <li>Document Handling Service</li>
            
            <p class="medium question">
                <strong>
                    Payment: Php 2,950.00
                </strong>
            </p>
            <p class="medium question">
                <strong>
                    GENERAL REQUIREMENTS
                </strong>
            </p>
            <li>1. Duly accomplish visa application form (hand written, all capital, do not leave blank spaces, write N/A if no answer)</li>
            <li>2. Philippine Passport valid at least six months prior to scheduled visa appointment</li>
            <li>3. Valid Government issued Identification Card</li>
            <li>4. Proof of income</li>
            <li>5. Proof of sufficient funds</li>
            <li>6. PSA Certificate of Live of Birth</li>
            <li>7. PSA Certificate of Marriage (if married)</li>


            <hr>

            
            <h2 class="medium question">
                <strong>
                    JAPAN VISA  
                </strong>
            </h2>
            <p class="medium question">
                <strong>
                    Ways to Apply
                </strong>
            </p>
            <li>Determine your eligibility</li>
            <li>Tri-G Travel and Tours will help you to check your eligibility by profile assessment.</li>

            <li> Record your personal reference code</li>
            <li>A Tri-G Travel agent will guide you on how to duly accomplished the visa application form.</li>

            <li>Gather and submit all the documents</li>
            <li>Tri-G Travel and Tours will facilitate all you travel requirements and personal documents for a higher success on visa grant.</li>

            <li>Pay the fee</li>
            <li>Tri-G Travel and Tours will be in charge for your interview appointment and processing of payment in the embassy.</li>

            <p class="medium question">
                <strong>
                    Inclusions:
                </strong>
            </p>
            <li>Client Profile Assessment</li>
            <li>Documentary Assistance</li>
            <li>Visa Coaching</li>
            <li>Scheduling of Visa Interview Appointment</li>
            <li>Visa Fee</li>
            <li>Document Handling Service</li>

            <p class="medium question">
                <strong>
                    Payment: Php 1,500.00 (no appearance)
                </strong>
            </p>

            
            <p class="medium question">
                <strong>
                    GENERAL REQUIREMENTS
                </strong>
            </p>
            <li>1. Duly accomplish visa application form (hand written, all capital, do not leave blank spaces, write N/A if no answer)</li>
            <li>2. Philippine Passport valid at least six months prior to scheduled visa appointment</li>
            <li>3. Valid Government issued Identification Card</li>
            <li>4. Proof of income</li>
            <li>5. Proof of sufficient funds</li>
            <li>6. PSA Certificate of Live of Birth</li>
            <li>7. PSA Certificate of Marriage (if married)</li>

            <hr>

            <h2 class="medium question">
                <strong>
                    KOREAN VISA  
                </strong>
            </h2>
            <p class="medium question">
                <strong>
                    Ways to Apply
                </strong>
            </p>
            <li>Determine your eligibility</li>
            <li>Tri-G Travel and Tours will help you to check your eligibility by profile assessment.</li>

            <li> Record your personal reference code</li>
            <li>A Tri-G Travel agent will guide you on how to duly accomplished the visa application form.</li>
            <li>Gather and submit all the documents</li>
            <li>Tri-G Travel and Tours will facilitate all you travel requirements and personal documents for a higher success on visa grant.</li>
            <li>Pay the fee</li>
            <li>Tri-G Travel and Tours will be in charge for your interview appointment and processing of payment in the embassy.</li>

            <p class="medium question">
                <strong>
                    Inclusions:
                </strong>
            </p>
            <li>Client Profile Assessment</li>
            <li>Documentary Assistance</li>
            <li>Visa Coaching</li>
            <li>Scheduling of Visa Interview Appointment</li>
            <li>Visa Fee</li>
            <li>Document Handling Service</li>
            <p class="medium question">
                <strong>
                    Payment: Php 950.00
                </strong>
            </p>
            
            <p class="medium question">
                <strong>
                    GENERAL REQUIREMENTS
                </strong>
            </p>
            <li>1. Duly accomplish visa application form (hand written, all capital, do not leave blank spaces, write N/A if no answer)</li>
            <li>2. Philippine Passport valid at least six months prior to scheduled visa appointment</li>
            <li>3. Valid Government issued Identification Card</li>
            <li>4. Proof of income</li>
            <li>5. Proof of sufficient funds</li>
            <li>6. PSA Certificate of Live of Birth</li>
            <li>7. PSA Certificate of Marriage (if married)</li>

            <hr>            

            <h2 class="medium question">
                <strong>
                    NEW ZEALAND VISA  
                </strong>
            </h2>

            <p class="medium question">
                <strong>
                    Ways to Apply
                </strong>
            </p>
            <li>Determine your eligibility</li>
            <li>Tri-G Travel and Tours will help you to check your eligibility by profile assessment.</li>

            <li> Record your personal reference code</li>
            <li>A Tri-G Travel agent will guide you on how to duly accomplished the visa application form.</li>
            <li>Gather and submit all the documents</li>
            <li>Tri-G Travel and Tours will facilitate all you travel requirements and personal documents for a higher success on visa grant.</li>
            <li>Pay the fee</li>
            <li>Tri-G Travel and Tours will be in charge for your interview appointment and processing of payment in the embassy.</li>

            
            <p class="medium question">
                <strong>
                    Inclusions:
                </strong>
            </p>
            <li>Client Profile Assessment</li>
            <li>Documentary Assistance</li>
            <li>Visa Coaching</li>
            <li>Scheduling of Visa Interview Appointment</li>
            <li>Visa Fee</li>
            <li>Document Handling Service</li>
            
            <p class="medium question">
                <strong>
                    Payment: Php 15,000.00
                </strong>
            </p>
            <p class="medium question">
                <strong>
                    GENERAL REQUIREMENTS
                </strong>
            </p>
            <li>1. Duly accomplish visa application form (hand written, all capital, do not leave blank spaces, write N/A if no answer)</li>
            <li>2. Philippine Passport valid at least six months prior to scheduled visa appointment</li>
            <li>3. Valid Government issued Identification Card</li>
            <li>4. Proof of income</li>
            <li>5. Proof of sufficient funds</li>
            <li>6. PSA Certificate of Live of Birth</li>
            <li>7. PSA Certificate of Marriage (if married)</li>

            <hr>
            
            <h2 class="medium question">
                <strong>
                    SCHENGEN VISA  
                </strong>
            </h2>
            <p class="medium question">
                <strong>
                    Ways to Apply
                </strong>
            </p>
            <li>Determine your eligibility</li>
            <li>Tri-G Travel and Tours will help you to check your eligibility by profile assessment.</li>

            <li> Record your personal reference code</li>
            <li>A Tri-G Travel agent will guide you on how to duly accomplished the visa application form.</li>
            <li>Gather and submit all the documents</li>
            <li>Tri-G Travel and Tours will facilitate all you travel requirements and personal documents for a higher success on visa grant.</li>
            <li>Pay the fee</li>
            <li>Tri-G Travel and Tours will be in charge for your interview appointment and processing of payment in the embassy.</li>

            
            <p class="medium question">
                <strong>
                    Inclusions
                </strong>
            </p>
            <li>Client Profile Assessment</li>
            <li>Documentary Assistance</li>
            <li>Visa Coaching</li>
            <li>Scheduling of Visa Interview Appointment</li>
            <li>Visa Fee</li>
            <li>Document Handling Service</li>

            
            <p class="medium question">
                <strong>
                    Payment:  Php 12,500.00
                </strong>
            </p>
            <p class="medium question">
                <strong>
                    GENERAL REQUIREMENTS
                </strong>
            </p>
            <li>1. Duly accomplish visa application form (hand written, all capital, do not leave blank spaces, write N/A if no answer)</li>
            <li>2. Philippine Passport valid at least six months prior to scheduled visa appointment</li>
            <li>3. Valid Government issued Identification Card</li>
            <li>4. Proof of income</li>
            <li>5. Proof of sufficient funds</li>
            <li>6. PSA Certificate of Live of Birth</li>
            <li>7. PSA Certificate of Marriage (if married)</li>

            <hr>

            <h2 class="medium question">
                <strong>
                    UAE VISA  
                </strong>
            </h2>
            <p class="medium question">
                <strong>
                    Ways to Apply
                </strong>
            </p>
            <li>Determine your eligibility</li>
            <li>Tri-G Travel and Tours will help you to check your eligibility by profile assessment.</li>

            <li> Record your personal reference code</li>
            <li>A Tri-G Travel agent will guide you on how to duly accomplished the visa application form.</li>
            <li>Gather and submit all the documents</li>
            <li>Tri-G Travel and Tours will facilitate all you travel requirements and personal documents for a higher success on visa grant.</li>
            <li>Pay the fee</li>
            <li>Tri-G Travel and Tours will be in charge for your interview appointment and processing of payment in the embassy.</li>

            <p class="medium question">
                <strong>
                    Inclusions:
                </strong>
            </p>
            <li>Client Profile Assessment</li>
            <li>Documentary Assistance</li>
            <li>Visa Coaching</li>
            <li>Scheduling of Visa Interview Appointment</li>
            <li>Visa Fee</li>
            <li>Document Handling Service</li>
            <p class="medium question">
                <strong>
                    Payment: Php 12,500.00
                </strong>
            </p>
            
            <p class="medium question">
                <strong>
                    GENERAL REQUIREMENTS
                </strong>
            </p>
            <li>1. Duly accomplish visa application form (hand written, all capital, do not leave blank spaces, write N/A if no answer)</li>
            <li>2. Philippine Passport valid at least six months prior to scheduled visa appointment</li>
            <li>3. Valid Government issued Identification Card</li>
            <li>4. Proof of income</li>
            <li>5. Proof of sufficient funds</li>
            <li>6. PSA Certificate of Live of Birth</li>
            <li>7. PSA Certificate of Marriage (if married)</li>

            <hr>
            
            <h2 class="medium question">
                <strong>
                    UK VISA  
                </strong>
            </h2>
            <p class="medium question">
                <strong>
                    Ways to Apply
                </strong>
            </p>
            <li>Determine your eligibility</li>
            <li>Tri-G Travel and Tours will help you to check your eligibility by profile assessment.</li>

            <li> Record your personal reference code</li>
            <li>A Tri-G Travel agent will guide you on how to duly accomplished the visa application form.</li>

            <li>Gather and submit all the documents</li>
            <li>Tri-G Travel and Tours will facilitate all you travel requirements and personal documents for a higher success on visa grant.</li>

            <li>Pay the fee</li>
            <li>Tri-G Travel and Tours will be in charge for your interview appointment and processing of payment in the embassy.</li>

            <p class="medium question">
                <strong>
                    Inclusions:
                </strong>
            </p>
            <li>Client Profile Assessment</li>
            <li>Documentary Assistance</li>
            <li>Visa Coaching</li>
            <li>Scheduling of Visa Interview Appointment</li>
            <li>Visa Fee</li>
            <li>Document Handling Service</li>
            <p class="medium question">
                <strong>
                    Payment: Php 12,500.00
                </strong>
            </p>
            
            <p class="medium question">
                <strong>
                    GENERAL REQUIREMENTS
                </strong>
            </p>
            <li>1. Duly accomplish visa application form (hand written, all capital, do not leave blank spaces, write N/A if no answer)</li>
            <li>2. Philippine Passport valid at least six months prior to scheduled visa appointment</li>
            <li>3. Valid Government issued Identification Card</li>
            <li>4. Proof of income</li>
            <li>5. Proof of sufficient funds</li>
            <li>6. PSA Certificate of Live of Birth</li>
            <li>7. PSA Certificate of Marriage (if married)</li>

            <hr>

            <h2 class="medium question">
                <strong>
                    US VISA  
                </strong>
            </h2>
            
            <p class="medium question">
                <strong>
                    Ways to Apply
                </strong>
            </p>
                <li>Determine your eligibility</li>
                <li>Tri-G Travel and Tours will help you to check your eligibility by profile assessment.</li>

                <li> Record your personal reference code</li>
                <li>A Tri-G Travel agent will guide you on how to duly accomplished the visa application form.</li>

                <li>Gather and submit all the documents</li>
                <li>Tri-G Travel and Tours will facilitate all you travel requirements and personal documents for a higher success on visa grant.</li>

                <li>Pay the fee</li>
                <li>Tri-G Travel and Tours will be in charge for your interview appointment and processing of payment in the embassy.</li>

            
            <p class="medium question">
                <strong>
                    Inclusions: 
                </strong>
            </p>
                <li>Client Profile Assessment</li>
                <li>Documentary Assistance</li>
                <li>Visa Coaching</li>
                <li>Scheduling of Visa Interview Appointment</li>
                <li>Visa Fee</li>
                <li>Document Handling Service</li>
            <p class="medium question">
                <strong>
                    Payment:  Php 15,000.00
                </strong>
            </p>
            <p class="medium question">
                <strong>
                    GENERAL REQUIREMENTS
                </strong>
            </p>
            <li>1. Duly accomplish visa application form (hand written, all capital, do not leave blank spaces, write N/A if no answer)</li>
            <li>2. Philippine Passport valid at least six months prior to scheduled visa appointment</li>
            <li>3. Valid Government issued Identification Card</li>
            <li>4. Proof of income</li>
            <li>5. Proof of sufficient funds</li>
            <li>6. PSA Certificate of Live of Birth</li>
            <li>7. PSA Certificate of Marriage (if married)</li>



        </div>

    </div>
@endsection
