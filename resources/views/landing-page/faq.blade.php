@extends('layouts.landing-page')
@section('content')
    <div class="row">
        <div class="col-md-12 mx-auto">
            <ul class="faq-ul">
                <li>
                    <p class="medium question">    
                        <strong>
                            Why should i book a flight with you whereas i can book airfare ticket online directly on their websites?
                        </strong>
                    </p>
                    <p class="thin">
                        3G CREATIVES TRAVEL &amp; TOURS has an advanced knowledge
                        and expertise that online booking cannot do. First is the great
                        savings you will get because we have partnership to wholesale tour
                        operators. Second is that we offer additional benefits that online
                        booking has none. Third is that we shield your tour with some
                        hustle-free land arrangements with free meals, van transfers and
                        unforgettable hospitality. We also assure our clients or would be
                        client for a PRICE MATCH GUARANTEE in comparison to
                        competitor’s quotation.
                    </p>
                </li>
                <li>
                    <p class="medium question">
                        <strong>
                            Why should I book a flight with you whereas I can book airfare ticket online directly on their websites?
                        </strong>
                    </p>
                    <p class="thin">
                        3G CREATIVES TRAVEL &amp; TOURS are better connected on
                        access of discounted rate as compared to other website for public
                        viewing. We can offer great deals either domestic and most
                        especially international airlines companies.
                    </p>
                </li>
                <li>
                    <p class="medium question">
                        <strong>
                            Why should I book a flight with you whereas I can book airfare ticket online directly on their websites?
                        </strong>
                    </p>
                    <p class="thin">
                    3G CREATIVES TRAVEL &amp; TOURS know the hotels better and
                    give client better deals and benefits. It&#39;s because we have more
                    inventories of rooms, no blackout dates, less hike on premium
                    holidays or weeks. We do profiling to better understand our client.
                    </p>
                </li>
                <li>
                    <p class="medium question">
                        <strong>
                            Why avail e-voucher for theme park and land arrangement tours with you instead of availing those hotels concierge?
                        </strong>
                    </p>
                    <p class="thin">
                    3G CREATIVES TRAVEL &amp; TOURS has direct contact in the
                    industry connections. We can offer you an array of tour attractions
                    that can easily be paid to us without any hidden charges or even
                    unexpected inconsideration of ground transportation, time constraint
                    in falling in line before entering the park. We have separate kiosk to
                    avoid those.
                    </p>
                </li>
                <li>
                    <p class="medium question">
                        <strong>
                            How long is your holding period forward a complete tour package?
                        </strong>
                    </p>
                    <p class="thin">
                    3G CREATIVES TRAVEL &amp; TOURS Basically offers (5) days
                    holding period. and for the airfare is always subject to change &amp;
                    availability. For safe keeping of target dates. We recommend a 24
                    Hours Book &amp; Buy scheme.
                    </p>
                </li>
                <li>
                    <p class="medium question">
                        <strong>
                            What are the available mode of payment?
                        </strong>
                    </p>
                    <p class="thin">
                    3G CREATIVES TRAVEL &amp; TOURS, accept payment thru BDO &amp;
                    Metrobank Account - online deposit &amp; bank transfers, however, we
                    suggest to visit us on our office at FSI Puregold Building, Guiguinto,
                    Bulacan.
                    </p>
                </li>
                <li>
                    <p class="medium question">
                        <strong>
                            Do you have senior citizen discount?
                        </strong>
                    </p>
                    <p class="thin">
                    3G CREATIVES TRAVEL &amp; TOURS have 10% senior citizen
                    discount only on domestic airlines while 500Php. off for a complete
                    tour arrangement.
                    </p>
                </li>
                <li>
                    <p class="medium question">
                        <strong>
                            Are you open for refunds, cancellation or scheduling of travel?
                        </strong>
                    </p>
                    <p class="thin">
                    Change of dates due to force majeure of domestic travel only is
                    applicable. It should be announced publicly a subject for flight
                    availability.
                    </p>
                </li>
                <li>
                    <p class="medium question">
                        <strong>
                            What tourist destinations are you offering? are their rates competitive?
                        </strong>
                    </p>
                    <p class="thin">

                    3G CREATIVES TRAVEL &amp; TOURS offers local tours from
                    Batanes to Surigao, from Hongkong to any part of Asia, Elite tours
                    like, US, Australia, Europe &amp; Maldives and even land tours. like
                    Baguio, Baler, Bicol &amp; other Landmarks in Luzon. We can
                    guarantee the competitiveness rate even compared to travel
                    agencies in Metro Manila.
                    </p>
                </li>
                <li>
                    <p class="medium question">
                        <strong>
                            What is your edge over the other travel agency?
                        </strong>
                    </p>
                    <p class="thin">
                    Aside from great value for money and savings we render our clients
                    with personal touch that can customize your travel creative
                    demands. you assured of meaningful trips. Most importantly we
                    have several satellite offices to cater client&#39;s accessibility of our
                    affordable travel requirements. We have global affiliations and
                    wholesale suppliers direct here and around the globe.
                    </p>
                </li>
            </ul>
        </div>
    </div>
@endsection
