@extends('layouts.landing-page')
@section('content')
    <div class="row" id="about-content-container">
        <div class="about col-md-12 mx-auto">
            <p class="thin">
                <strong>3G CREATIVES TRAVEL AND TOURS</strong> is the new trend
                in the tourism industry wherein the client can be creative in
                choosing the tour packages client&#39;s wants on flexible terms of
                having a complete itinerary tour package, hotel
                accommodations, airfare e-ticket or even land arrangements on
                theme parks in a matter of LESS THAN AN HOUR. This
                innovation is a challenge for the company to better serve the
                travel junkies of the millennium that all demands must be in just
                a snap of a finger or just in a blink of an eye; the fast
                movements of technology.
            </p>
            <p class="thin">
                This is made possible because of our software
                technology and partners with award winning online platforms
                travel designer group that engaged on wholesale tour
                packages. This can guarantee of affordable yet quality
                accommodations and tours. Some of globally recognized
                affiliates of the company are Asiatravel, Global International
                B2B system, Via.com, IATI, Abacus and other third party
                booking websites. The owner is also a member of one of the
                most prestigious timeshare club membership of resorts and
                hotel in the Philippines namely Astoria Vacation and Leisure
                Club Inc. (AVLCI) and the world&#39;s most renounced time share of
                global resort and hotel club membership, The RCI. More so, the
                company has direct connections to tour operators on top-finest
                local destination such as Boracay, Bohol, Baguio, Palawan,
                Baler, Quezon &amp; Bicol.
            </p>
            <p class="thin">
                The company is established in 2016 basically in online.
                But due to frauds and scams online, the owner decided to open
                an office at Puregold Building near PAGCOR CASINO in
                Guiguinto Bulacan and soon to open satellite offices in the
                progressive towns of Bulacan Province and the rest of Central
                Region.
            </p>
        </div>
    </div>
@endsection
