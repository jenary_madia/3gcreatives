@extends('layouts.landing-page')
@section('content')
    <!-- <div class="row" id="about-content-container">
        <div class="col-md-6">
            <span class="medium">Freelance online marketer</span>
            <br>
            <span class="medium">Hotel accomodation</span>
            <br>
            <span class="medium">Philippine passport</span>
            <br>
            <span class="medium">Satellite office</span>       
        </div>
        <div class="col-md-6">
            <span class="medium">Theme parks</span>
            <br>
            <span class="medium">Ticketing office</span>
            <br>
            <span class="medium">Tours arrangement</span>
            <br>
            <span class="medium">Van rental</span>
        </div>
        <div class="col-md-12">
            <span class="medium">Visa consultation</span> 
        </div>
        <ul style="columns: 2; -webkit-columns: 2; -moz-columns: 2; list-style-position: inside;">
            <li>Australia</li>
            <li>Canada</li>
            <li>China</li>
            <li>Japan</li>
            <li>Korea</li>
            <li>New Zealand</li>
            <li>Schengen</li>
            <li>UAE</li>
            <li>US</li>
            <li>UK</li>
        </ul>

    </div> -->
    <style>
        .service-ul li {
            list-style-type: circle !important;
        }
    </style>
    <div class="row">
        <div class="col-md-12 mx-auto">
            <h1 class="medium question">
                <strong>
                    We are looking for Business Partners!!!
                </strong>
            </h1>

            <p class="medium question">
                <strong>
                    Qualifications
                </strong>
            </p>
            <ul class="service-ul">
                <li>Must have internet connection(DATA/DSL)</li>
                <li>Social media addicts</li>
                <li>Connections and communications</li>
                <li>Credible</li>
            </ul>
            <p class="medium question">
                <strong>
                    Marketing Kit
                </strong>
            </p>
            <ul class="service-ul">
                <li>Six [6] pcs 3G Traveller Prestige Club Card Selling(worth php 3,000) or use the six [6] 3G Traveller Prestige Club Card in exchange of 1500 travel certificate</li>
                <li>One [1] freelance FREE LANCE ONLINE MARKETER VALIDITY CARD</li>
            </ul>
            <p class="medium question">
                <strong>
                    Potentials
                </strong>
            </p>
            <ul class="service-ul">
                <li>Php1000 per referral on international flight complete tour package </li>
                <li>Php500 on domestic complete tour package referral </li>
                <li>Php3000 commission on Big Group (10pax and above) on international complete tour package referral /php2000 on domestic </li>
                <li>Php300 commission on signature perfume and other products </li>
                <li>Php150 commission on van rental </li>
                <li>Php100 commission on airfare ticket </li>
            </ul>

            <p class="medium question">
                <strong>
                    Referral Procedure
                </strong>
            </p>
            <ul class="service-ul">
                <li>Endorse the 3G travel and tours services via online, sms or direct referral</li>
                <li>Give your FOM agent code to your client</li>
                <li>The client must use your agent code  to gain commission and points on your account or bring your client with you at our main office</li>
                <li>An additional php 100 will be given to you</li>
                <li>Give us a call to refer the name of the client to inform us</li>
            </ul> 

            <p class="medium question">
                <strong>
                    Activation
                </strong>
            </p>
            <ul class="service-ul">
                <li>The F.O.M shall present and submit the photocopy of one[1] valid and 1 pc 1x1 or 2x2 photo at the main office of 3G TRAVEL AND TOURS Puregold Bldg. Mc Arthur highway, Tabang, Guiguinto Bulacan,near PAGCOR</li>
                <li>3G Travel and Tours will give back Php150 upon successful activation</li>
            </ul> 

            <p class="medium question">
                <strong>
                    Additional Perks
                </strong>
            </p>
            <ul class="service-ul">
                <li>Free invitation including entrance and foods on Christmas party/general assembly during December.</li>
                <li>Chance to win exciting prices on raffle draw.</li>
            </ul> 

            <p class="medium question">
                <strong>
                    Collection of Commission
                </strong>
            </p>
            <ul class="service-ul">
                <li>Open an ATM account either BDO or METROBANK or collect your commission personally at our main office or via smart padala with the service fee depending on the amount  of commission you wish to collect</li>
                <li>In collection by person, you must present your FOM validation card on our desk officer</li>
                <li>All commissions are tax free </li>
            </ul> 

            <p class="medium question">
                <strong>
                    Expiration
                </strong>
            </p>
            <ul class="service-ul">
                <li>F.O.M must have at least 2 referrals on any 3G CREATIVES TRAVEL AND TOURS services per year</li>
            </ul>
            <br> 
            <hr>
            <br>
            <h1 class="medium question">
                <strong>
                    BELIEVE IT OR NOT?!?
                </strong>
            </h1>
            <p class="medium question">
                <strong>
                     HAVE YOU EVER HEARD OF A BUSINESS WITH LESS CAPITAL BUT MORE GAINS?
                </strong>
                <br>
                <strong>
                     GUARANTEED RETURN OF INVESTMENT IN SHORT SPAN OF 1HOUR TO 1 MONTH ONLY?
                </strong>
            </p>
            <p class="medium question">
                <strong>
                     Sources of Income
                </strong>
            </p>
            <ul class="service-ul">
                <li>up to 25% mark-up on complete tours(domestic & asia pacific)</li>

                <li>up to 100% mark-up on complete tours (USA , Europe, Maldives, Dubai & Japan)</li>

                <li>20% visa processing commission</li>

                <li>Php 200 mark up on domestic airfare and Php 500 on international airfare ticket</li>
                <li>Up to 20% mark up on hotel accommodations (affiliated domestic resorts/accommodation in Baguio, Bataan, Batangas, Bulacan & Zambales)</li>
                <li>Php300 van rentals</li>
                <li>Php 500 commission on signature px products such as perfumes, watches, bags and gadgets</li>
                <li>1000php commission on photo and video coverage</li>
            </ul>
            
            <p class="medium question">
                <strong>
                     Business Opportunities
                </strong>
            </p>
            <ul class="service-ul">
                <li>Double Probability of income</li>

                <li>Convenience and great savings on personal travel</li>

                <li>Global advertisement on our WEBSITE and FB FAN PAGE</li>

                <li>Quickest  Return of Investment</li>
            </ul>

            <p class="medium question">
                <strong>
                    Zero Operation Expenses
                </strong>
            </p>
            <ul class="service-ul">    
                <li>ADDITIONAL BUSINESS PERMITS TO PROCESS</li>
                <li>BIR CPA RETAINERS FEE/ MONTH TO WORRY</li>
                <li>ADDITIONAL MANPOWER</li>
                <li>RENTAL SPACE FEE NEEDED</li>
                <li>RISK OF R.O.I. FAILURE</li>
            </ul>
            <p class="medium question">
                <strong>
                    Vision
                </strong>
            </p>
            <ul class="service-ul">
                <li>To gain more profit</li>
                <li>To establish the trade name in the hospitality industry </li>
                <li>Strengthen the traveling capability of a low to medium to elite travelers</li>
                <li>Best accessibility here in and around the globe</li>
            </ul>

            <p class="medium question">
                <strong>
                    Partnership Inclusions
                </strong>
            </p>
            <ul class="service-ul">
                <li>One (1) Laptop</li>
                <li>COMPLEMENTARY ACCOMMODATION</li>
                <li>4D3N(4 pax) ASTORIA LUXURY ACCOMMODATION (choice of BORACAY, BOHOL, PALAWAN, MAKATI, ORTIGAS) or</li>
                <li>2D1N(4 pax) RESORTS WORLD MANILA HOTEL STAYCATION with 4 CINEMA VOUCHER or</li>
                <li>2D1N(4 pax) CANYON COVE BATANGAS LUXURY ESCAPADE</li>
                <li>10pcs 3G TRAVELER PRESTIGE CLUB CARD(worth 5000php)</li>
                <li>Free system training</li>
                <li>Marketing materials</li>
                <li>1 year subscription/access of APP and Website advertisement</li>
            </ul>


            <p class="medium question">
                <strong>
                    Partnership fee
                </strong>
            </p>
            <ul class="service-ul">
                <li>Php 50,000.00 (SPOT CASH)</li>
                <li>Php 25,000.00 (DOWNPAYMENT)</li>
                <li>3 months to pay in Php 10,000.00 per month (PDC)</li>
                <li>Php 30,000.00 (NO LAPTOP AND NO ACCOMMODATION)</li>
            </ul>

            <p class="medium question">
                <strong>
                    Partnership Requirements
                </strong>
            </p>
            <ul class="service-ul">
                <li>2 gov’t issued ID</li>
                <li>Photocopy of existing mayor’s permit to operate(for existing businesses): (Certificate of ownership/lease for home based partner)</li>
                <li>2pcs photo 1x1 or 2x2</li>
            </ul>

            <p class="medium question">
                <strong>
                    Collect Commission
                </strong>
            </p>
            <ul class="service-ul">
                <li>Must open an account to BPI or METROBANK (ATM or PASSBOOK)</li>
                <li>Commission will be credited after a successful booking transaction/s</li>
                <li>Can visit the main office directly to get the commission</li>
                <li>Commission  has a 3% less for transaction without BIR OFFICIAL RECEIPT</li>
                <li>12% less on commission with BIR OFFICIAL RECEIPT</li>
            </ul>
        </div>

    </div>
@endsection
