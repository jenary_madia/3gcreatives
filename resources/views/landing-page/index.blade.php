@extends('layouts.landing-page')
@section('content')
    <h1>Domestic</h1>
    <br>
    <div class="row">
        <div class="col-lg-4 col-md-4 col-xs-6">
          <a href="#" class="d-block mb-4 h-100">
            <img class="img-fluid img-thumbnail" src="{{ asset('images/400x400/domestic/batanes.png') }}" alt="">
            </a>
        </div>
        <div class="col-lg-4 col-md-4 col-xs-6">
          <a href="#" class="d-block mb-4 h-100">
            <img class="img-fluid img-thumbnail" src="{{ asset('images/400x400/domestic/bohol.png') }}" alt="">
            </a>
        </div>
        <div class="col-lg-4 col-md-4 col-xs-6">
          <a href="#" class="d-block mb-4 h-100">
            <img class="img-fluid img-thumbnail" src="{{ asset('images/400x400/domestic/boracay.png') }}" alt="">
            </a>
        </div>
        <div class="col-lg-4 col-md-4 col-xs-6">
          <a href="#" class="d-block mb-4 h-100">
            <img class="img-fluid img-thumbnail" src="{{ asset('images/400x400/domestic/cebu.png') }}" alt="">
            </a>
        </div>
        <div class="col-lg-4 col-md-4 col-xs-6">
          <a href="#" class="d-block mb-4 h-100">
            <img class="img-fluid img-thumbnail" src="{{ asset('images/400x400/domestic/coron.png') }}" alt="">
            </a>
        </div>
        <div class="col-lg-4 col-md-4 col-xs-6">
          <a href="#" class="d-block mb-4 h-100">
            <img class="img-fluid img-thumbnail" src="{{ asset('images/400x400/domestic/davao.png') }}" alt="">
            </a>
        </div>
        <div class="col-lg-4 col-md-4 col-xs-6">
          <a href="#" class="d-block mb-4 h-100">
            <img class="img-fluid img-thumbnail" src="{{ asset('images/400x400/domestic/el-nido.png') }}" alt="">
            </a>
        </div>
        <div class="col-lg-4 col-md-4 col-xs-6">
          <a href="#" class="d-block mb-4 h-100">
            <img class="img-fluid img-thumbnail" src="{{ asset('images/400x400/domestic/oslob.png') }}" alt="">
            </a>
        </div>
        <div class="col-lg-4 col-md-4 col-xs-6">
          <a href="#" class="d-block mb-4 h-100">
            <img class="img-fluid img-thumbnail" src="{{ asset('images/400x400/domestic/palawan.png') }}" alt="">
            </a>
        </div>
        <div class="col-lg-4 col-md-4 col-xs-6">
            <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
            <!-- ad2 -->
            <ins class="adsbygoogle"
                 style="display:inline-block;width:336px;height:280px"
                 data-ad-client="ca-pub-9761459654318383"
                 data-ad-slot="6151480542"></ins>
            <script>
            (adsbygoogle = window.adsbygoogle || []).push({});
            </script>
        </div>
    
        <!-- <div class="col-lg-4 col-md-4 col-xs-6">
          <a href="#" class="d-block mb-4 h-100">
            <img class="img-fluid img-thumbnail" src="{{ asset('images/400x400/domestic/SIARGAO.png') }}" alt="">
            </a>
        </div> -->
    </div>
    <br>
    <hr>
    <br>
    <h1>International</h1>
    <br>
    <div class="row">
        <div class="col-lg-4 col-md-4 col-xs-6">
          <a href="#" class="d-block mb-4 h-100">
            <img class="img-fluid img-thumbnail" src="{{ asset('images/400x400/international/hong-kong.png') }}" alt="">
            </a>
        </div>
        <div class="col-lg-4 col-md-4 col-xs-6">
          <a href="#" class="d-block mb-4 h-100">
            <img class="img-fluid img-thumbnail" src="{{ asset('images/400x400/international/japan.png') }}" alt="">
            </a>
        </div>
        <div class="col-lg-4 col-md-4 col-xs-6">
          <a href="#" class="d-block mb-4 h-100">
            <img class="img-fluid img-thumbnail" src="{{ asset('images/400x400/international/korea.png') }}" alt="">
            </a>
        </div>
        <div class="col-lg-4 col-md-4 col-xs-6">
          <a href="#" class="d-block mb-4 h-100">
            <img class="img-fluid img-thumbnail" src="{{ asset('images/400x400/international/malaysia.png') }}" alt="">
            </a>
        </div>
        <div class="col-lg-4 col-md-4 col-xs-6">
          <a href="#" class="d-block mb-4 h-100">
            <img class="img-fluid img-thumbnail" src="{{ asset('images/400x400/international/maldives.png') }}" alt="">
            </a>
        </div>
        <div class="col-lg-4 col-md-4 col-xs-6">
          <a href="#" class="d-block mb-4 h-100">
            <img class="img-fluid img-thumbnail" src="{{ asset('images/400x400/international/schengen.png') }}" alt="">
            </a>
        </div>
        <div class="col-lg-4 col-md-4 col-xs-6">
          <a href="#" class="d-block mb-4 h-100">
            <img class="img-fluid img-thumbnail" src="{{ asset('images/400x400/international/las-vegas.png') }}" alt="">
            </a>
        </div>
        <div class="col-lg-4 col-md-4 col-xs-6">
          <a href="#" class="d-block mb-4 h-100">
            <img class="img-fluid img-thumbnail" src="{{ asset('images/400x400/international/singapore.png') }}" alt="">
            </a>
        </div>
        <div class="col-lg-4 col-md-4 col-xs-6">
          <a href="#" class="d-block mb-4 h-100">
            <img class="img-fluid img-thumbnail" src="{{ asset('images/400x400/international/thailand.png') }}" alt="">
            </a>
        </div>
        <div class="col-lg-4 col-md-4 col-xs-6">
            <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
            <!-- ad2 -->
            <ins class="adsbygoogle"
                 style="display:inline-block;width:336px;height:280px"
                 data-ad-client="ca-pub-9761459654318383"
                 data-ad-slot="6151480542"></ins>
            <script>
            (adsbygoogle = window.adsbygoogle || []).push({});
            </script>
        </div>
    </div>
@endsection
