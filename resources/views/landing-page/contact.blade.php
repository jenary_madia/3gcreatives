@extends('layouts.landing-page')
@section('content')
    <div class="row">
        <div class="col-lg-6 mx-auto" id="contact-container">

            <p>Want to get in touch? Fill out the form below to send me a message and I will get back to you as soon as possible!</p>
            <!-- Contact Form - Enter your email address on line 19 of the mail/contact_me.php file to make this form work. -->
            <!-- WARNING: Some web hosts do not allow emails to be sent through forms to common mail hosts like Gmail or Yahoo. It's recommended that you use a private domain email address! -->
            <!-- To use the contact form, your site must be on a live web host with PHP! The form will not work locally! -->
            <form id="contactform" method="POST">
                {{ csrf_field() }}
                <div class="control-group">
                    <div class="form-group floating-label-form-group controls">
                        <label>Name</label>
                        <input type="text" class="form-control" placeholder="Name" id="name" required name="full_name" v-model="contact_form.full_name"
                            v-validate="'required'" :class="{'input': true, 'is-danger': ferrors.has('full_name') }" data-vv-as="Full Name">

                        <div v-show="ferrors.has('full_name')" class="alert alert-danger" role="alert">
                          @{{ ferrors.first('full_name') }}
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <div class="form-group floating-label-form-group controls">
                        <label>Email Address</label>
                        <input type="email" class="form-control" placeholder="Email Address" required name="email" v-model="contact_form.email"
                            v-validate="'required|email'" :class="{'input': true, 'is-danger': ferrors.has('email') }" data-vv-as="Email">

                        <div v-show="ferrors.has('email')" class="alert alert-danger" role="alert">
                          @{{ ferrors.first('email') }}
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <div class="form-group col-xs-12 floating-label-form-group controls">
                        <label>Phone Number</label>
                        <input type="tel" class="form-control" placeholder="Phone Number" id="phone" required name="phone" v-model="contact_form.phone"
                            v-validate="{ required: true, regex: /^(09|\+639)\d{9}$/ }" data-vv-as="Phone Number">

                        <div v-show="ferrors.has('phone')" class="alert alert-danger" role="alert">
                          @{{ ferrors.first('phone') }}
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <div class="form-group floating-label-form-group controls">
                        <label>Message</label>
                        <textarea rows="5" class="form-control" placeholder="Message" id="message" required name="body_message" minlength="10" v-model="contact_form.body_message"
                            v-validate="'required|min:10'" :class="{'input': true, 'is-danger': ferrors.has('body_message') }"
                            data-vv-as="Message"></textarea>

                        <div v-show="ferrors.has('body_message')" class="alert alert-danger" role="alert">
                          @{{ ferrors.first('body_message') }}
                        </div>
                    </div>
                </div>
                <br>
                <div id="success"></div>
                <div class="form-group">
                <button type="button" :disabled="isAllFieldsAreEmpty" class="btn btn-primary" @click.prevent="validateBeforeSubmit">Send</button>
                </div>
            </form>
        </div>
        <div class="col-lg-5 mx-auto" id="contact-container">
            <p>For your immediate concerns, don't hesitate to contact us</p>
            <ul class="list-unstyled">
                <li><span class="contact-icon fa fa-mobile"></span><span>(Globe) 0926-8194-434</span></li>
                <li><span class="contact-icon fa fa-mobile"></span><span>(Sun) 0922-8951-250</span></li>
                <li><span class="contact-icon fa fa-phone"></span><span>(044)306-1840</span></li>
                <li><span class="contact-icon fa fa-facebook"></span><a target="_blank" href="https://www.facebook.com/trigtravelandtours/">trigtravelandtours</a></li>
            </ul>
        </div>
    </div>
@endsection
@section('script_per_page')
    <script type="text/javascript" src="{!! asset('js/contact-app.js') !!}"></script>
@endsection
