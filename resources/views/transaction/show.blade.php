@extends('layouts.app')

@section('content')
<div class="col-md-12" id="div-transaction">
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>Add Transaction

            </h1>
        </div>
        <!-- END PAGE TITLE -->
    </div>
    <!-- END PAGE HEAD-->
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="index.html">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span class="active">View Transaction</span>
        </li>
    </ul>

    <div class="portlet light col-md-8 col-md-offset-2">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-paper-plane font-green-haze"></i>
                <span class="caption-subject bold font-green-haze uppercase"> Transaction Details </span>
            </div>
        </div>
        <div class="portlet-body row">
          <div class="col-md-4">
            <ul class="list-unstyled">
              <li><span>Prestige Card : {{ $transaction->card_no }}</span></li>
              <li><span>Transaction Type : {{ $transaction->name }}</span></li>
              <li><span>Price : {{ $transaction->total_price }}</span></li>
            </ul>
          </div>
          <div class="col-md-4">

          </div>
          <div class="col-md-4">
            <ul class="list-unstyled">
                <li><span>Date From : {{ $transaction->date_from }}</span></li>

                @isset($transaction->date_to)
                <li>
                    <span>Date To : {{ $transaction->date_to }}</span>
                </li>
                @endisset

                <li><span>Destination : {{ $transaction->destination }}</span></li>

                @isset($transaction->hotel_name)
                <li>
                    <span>Hotel : {{ $transaction->hotel_name }}</span>
                </li>
                @endisset

                @isset($transaction->airline_name)
                <li>
                    <span>Airline : {{ $transaction->airline_name }}</span>
                </li>
                @endisset
            </ul>
          </div>
          <div class="col-md-12">
            <h4> Companions : </h4>
              <table class="table table-bordered table-striped table-condensed flip-content table-companions">
                <thead>
                    <tr>
                        <th>First Name</th>
                        <th>Middle Name</th>
                        <th>Last Name</th>
                        <th>Age</th>
                        <th>Contact Number</th>
                        <th>Email</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($companions as $companion)
                        <tr>
                            <td>
                                <input type="text" class="form-control" disabled="true" value="{{ $companion->first_name }}">
                            </td>
                            <td>
                                <input type="text" class="form-control" disabled="true" value="{{ $companion->middle_name }}">
                            </td>
                            <td>
                                <input type="text" class="form-control" disabled="true" value="{{ $companion->last_name }}">
                            </td>
                            <td>
                                <input type="text" class="form-control" disabled="true" value="{{ $companion->age }}">
                            </td>
                            <td>
                                <input type="text" class="form-control" disabled="true" value="{{ $companion->contact_no }}">
                            </td>
                            <td>
                                <input type="text" class="form-control" disabled="true" value="{{ $companion->email }}">
                            </td>
                        </tr>
                    @endforeach
                </tbody>
              </table>
              <div class="form-actions text-center">
                <a href="{{ url()->previous() }}" class="btn default">Back to Transactions List</a>
              </div>
          </div>
        </div>
      </div>
</div>
@endsection

@section('script_per_module')
    <script src="{!! asset('js/transaction-app.js') !!}"></script>
@endsection
