@extends('layouts.app')

@section('content')
<div class="col-md-12" id="div-transaction">
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>Add Transaction

            </h1>
        </div>
        <!-- END PAGE TITLE -->
    </div>
    <!-- END PAGE HEAD-->
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="index.html">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">Transaction List</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span class="active">Add transaction</span>
        </li>
    </ul>
    <div class="portlet light portlet-fit bordered">
        <div class="portlet-body">
            <div class="mt-element-step">
                <div class="row step-thin">
                    <div class="col-md-4 bg-grey mt-step-col" v-bind:class="{ active: step == 1 }">
                        <div class="mt-step-number bg-white font-grey">1</div>
                        <div class="mt-step-title uppercase font-grey-cascade">Details</div>
                        <div class="mt-step-content font-grey-cascade">Fill up transaction details</div>
                    </div>
                    <div class="col-md-4 bg-grey mt-step-col" v-bind:class="{ active: step == 2 }">
                        <div class="mt-step-number bg-white font-grey">2</div>
                        <div class="mt-step-title uppercase font-grey-cascade">Companions</div>
                        <div class="mt-step-content font-grey-cascade">Fill up transaction companions</div>
                    </div>
                    <div class="col-md-4 bg-grey mt-step-col" v-bind:class="{ active: step == 3 }">
                        <div class="mt-step-number bg-white font-grey">3</div>
                        <div class="mt-step-title uppercase font-grey-cascade">Review</div>
                        <div class="mt-step-content font-grey-cascade">Review informations</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <edit-transaction
        :transaction_types = '{{ $transaction_types }}'
        :transaction_details = '{!! $transaction !!}'
        @add_step = "updateStep"
    ></edit-transaction>
</div>
@endsection
@section('script_per_module')
    <script src="{!! asset('js/transaction-app.js') !!}"></script>
@endsection
