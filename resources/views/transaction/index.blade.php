@extends('layouts.app')

@section('content')
<div class="col-md-12" id="div-transaction">
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>Transactions
            </h1>
        </div>
        <!-- END PAGE TITLE -->
    </div>
    <!-- END PAGE HEAD-->
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="index.html">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span class="active">Transaction List</span>
        </li>
    </ul>
    <!-- BEGIN SAMPLE TABLE PORTLET-->
    <div class="portlet box green">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs"></i>Transactions
            </div>
            @if($role_id == 1)
                <div class="tools">
                    <a href="{{ url('/transaction/new') }}" style="color: #fff">
                        <span class="fa fa-plus"></span>
                        New
                    </a>
                </div>
            @endif
        </div>
        <div class="portlet-body flip-scroll">
            <table class="table table-striped table-bordered table-hover table-checkable order-column dataTable no-footer" id="transaction-list" role="grid" aria-describedby="sample_1_info">
                <thead>
                    <tr role="row">
                        <th class="sorting_asc" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" aria-sort="ascending" aria-label=" Username : activate to sort column descending" style="width: 208px;">
                            Reference Number
                        </th>
                        <th class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" aria-label=" Email : activate to sort column ascending" style="width: 150px;">
                            Transaction Type
                        </th>
                        <th class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" aria-label=" Status : activate to sort column ascending" style="width: 169px;">
                            Client Name
                        </th>
                        <th class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" aria-label=" Joined : activate to sort column ascending" style="width: 160px;">
                            Total Price
                        </th>
                        <th class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" aria-label=" Actions : activate to sort column ascending" style="width: 75px;">
                            Total Points
                        </th>
                        <th class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" aria-label=" Actions : activate to sort column ascending" style="width: 165px;">
                            Date Created
                        </th>
                        <th class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" aria-label=" Actions : activate to sort column ascending" style="width: 100px;">
                            Status
                        </th>
                        <th class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" aria-label=" Actions : activate to sort column ascending" style="width: 165px;">
                            Action
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($transactions as $transaction)
                        <tr class="gradeX odd" role="row">
                            <td class="sorting_1"> {{ $transaction->reference_no }} </td>
                            <td>
                                {{ $transaction->type_name }}
                            </td>
                            <td>
                                {{ $transaction->client_name }}
                            </td>
                            <td>
                                <span class="label label-sm label-primary"> {{ $transaction->total_price }} </span>
                            </td>
                            <td>
                                {{ $transaction->total_points }}
                            </td>
                            <td>
                                {{ $transaction->created_at }}
                            </td>
                            <td>
                                @switch($transaction->status)
                                    @case(1)
                                        Processed
                                        @break

                                    @case(5)
                                        Rolled back
                                        @break
                                @endswitch
                            </td>
                            <td>
                                <a href="{{ route('transaction.show', ['id' => $transaction->id]) }}" class="btn blue btn-sm">
                                    <span class="fa fa-eye"></span>
                                </a>
                                @if($transaction->status != 5 && Auth::user()->role_id == 1)
                                    <button @click.prevent="rollback('{{ $transaction->reference_no }}')" class="btn red btn-sm">
                                        <span class="fa fa-remove"></span>
                                    </button>
                                    <a href="{{ route('transaction.edit', ['id' => $transaction->id]) }}" class="btn blue btn-sm">
                                        <span class="fa fa-edit"></span>
                                    </a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
@section('script_per_module')
    <script src="{!! asset('js/transaction-app.js') !!}"></script>
@endsection
