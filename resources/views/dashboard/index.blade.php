@extends('layouts.app')

@section('content')
<!-- BEGIN PAGE HEAD-->
<div class="page-head">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
        <h1>Dashboard
        </h1>
    </div>
    <!-- END PAGE TITLE -->
</div>
<!-- END PAGE HEAD-->
<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
    <li>
        <span class="active">Home</span>
    </li>
</ul>
<!-- END PAGE BREADCRUMB -->
<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
    <div class="dashboard-stat2 bordered">
        <div class="display">
            <div class="number">
                <h3 class="font-blue-soft">
                    <span data-counter="counterup" data-value="276">{{ $total_points }}</span>
                </h3>
                <small>Accumulated points as of now</small>
            </div>
            <div class="icon">
                <i class="icon-user"></i>
            </div>
        </div>
        <div class="progress-info">
            <div class="progress">
                <span style="width: 57%;" class="progress-bar progress-bar-success blue-soft">
                    <span class="sr-only">56% change</span>
                </span>
            </div>
            <div class="status">
                <div class="status-title"> change </div>
                <div class="status-number"> 57% </div>
            </div>
        </div>
    </div>
</div>
@endsection
