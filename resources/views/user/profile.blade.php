@extends('layouts.app')

@section('content')
<div id="profile-container">
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>User Profile
                <small>Profile page layout</small>
            </h1>
        </div>
        <!-- END PAGE TITLE -->
    </div>
    <!-- END PAGE HEAD-->
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="index.html">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span class="active">User Profile</span>
        </li>
    </ul>
    <!-- END PAGE BREADCRUMB -->
    <user-profile></user-profile>
</div>
@endsection

@section('script_per_module')
    <script src="{!! asset('js/user-profile-app.js') !!}"></script>
@endsection
