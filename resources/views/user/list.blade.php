@extends('layouts.app')

@section('content')
<div class="col-md-12" id="div-user">
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>User List
                <small>List of Employees and Agents</small>
            </h1>
        </div>
        <!-- END PAGE TITLE -->
    </div>
    <!-- END PAGE HEAD-->
    <!-- BEGIN SAMPLE TABLE PORTLET-->
    <div class="portlet box green">
        <div class="portlet-title">
            <div class="tools">
                <button type="button" class="btn btn-default" data-toggle="modal" href="#basic"> Add User </button>
            </div>
        </div>
        <div class="portlet-body flip-scroll">
            <table class="table table-striped table-bordered table-hover table-checkable order-column dataTable no-footer" id="transaction-list" role="grid" aria-describedby="sample_1_info">
                <thead>
                    <tr role="row">
                        <th class="sorting_asc" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" aria-sort="ascending" aria-label=" Username : activate to sort column descending" style="width: 208px;"> 
                            Name 
                        </th>
                        <th class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" aria-label=" Email : activate to sort column ascending" style="width: 307px;"> 
                            Email 
                        </th>
                        <!-- <th class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" aria-label=" Status : activate to sort column ascending" style="width: 169px;">  -->
                            <!-- Status  -->
                        <!-- </th> -->
                        <th class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" aria-label=" Status : activate to sort column ascending" style="width: 169px;"> 
                            Date Added 
                        </th>
                        <th class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" aria-label=" Actions : activate to sort column ascending" style="width: 165px;">
                            Actions 
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $user)
                        <tr class="gradeX odd" role="row">
                            <td>
                                {{ $user->full_name }}
                            </td>
                            <td>
                                {{ $user->email }}
                            </td>
                            <!-- <td> -->
                                <!-- <span class="label label-sm label-primary"> Active </span> -->
                            <!-- </td> -->
                            <td> 
                                {{ $user->created_at }}
                            </td>
                            <td>
                                <button> Edit </button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <add-user></add-user>
</div>
@endsection
@section('script_per_module')
    <script src="{!! asset('js/user-app.js') !!}"></script>
@endsection
