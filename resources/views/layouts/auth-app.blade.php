<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8" />
        <title>3GCREATIVE TRAVEL &amp; TOURS</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="Preview page of Metronic Admin Theme #4 for " name="description" />
        <meta content="" name="author" />
        <meta name="csrf-token" content="{!! csrf_token() !!}" id="token">
        <meta name="base-url" content="{!! url('/') !!}" id="base_url">
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="{!! asset('global/plugins/font-awesome/css/font-awesome.min.css') !!}" rel="stylesheet" type="text/css" />
        <link href="{!! asset('global/plugins/simple-line-icons/simple-line-icons.min.css') !!}" rel="stylesheet" type="text/css" />
        <link href="{!! asset('global/plugins/bootstrap/css/bootstrap.min.css') !!}" rel="stylesheet" type="text/css" />
        <link href="{!! asset('global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') !!}" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="{!! asset('global/plugins/select2/css/select2.min.css') !!}" rel="stylesheet" type="text/css" />
        <link href="{!! asset('global/plugins/select2/css/select2-bootstrap.min.css') !!}" rel="stylesheet" type="text/css" />
        <!-- <link href="{!! asset('global/plugins/bootstrap-daterangepicker/daterangepicker.min.css') !!}" rel="stylesheet" type="text/css" /> -->
        <link href="{!! asset('global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') !!}" rel="stylesheet" type="text/css" />
        <!-- <link href="{!! asset('global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css') !!}" rel="stylesheet" type="text/css" /> -->
        <!-- <link href="{!! asset('global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') !!}" rel="stylesheet" type="text/css" /> -->
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="{!! asset('global/css/components.min.css') !!}" rel="stylesheet" id="style_components" type="text/css" />
        <link href="{!! asset('global/css/plugins.min.css') !!}" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="{!! asset('pages/css/login-4.css') !!}" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="{!! asset('layouts/layout4/css/layout.min.css') !!}" rel="stylesheet" type="text/css" />
        <link href="{!! asset('layouts/layout4/css/themes/default.min.css') !!}" rel="stylesheet" type="text/css" id="style_color" />
        <link href="{!! asset('layouts/layout4/css/custom.min.css') !!}" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" /> </head>

    <!-- END HEAD -->

    <body class=" login">
        <!-- BEGIN LOGO -->
        <div class="logo">
            <a href="{{ route('home') }}">
                <img src="{!! url('/images/logo-small.png') !!}" alt="" />
            </a>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <div class="content">
            <!-- BEGIN LOGIN FORM -->
            @yield('content')
            <!-- END LOGIN FORM -->
            <!-- BEGIN FORGOT PASSWORD FORM -->
            <!-- <form class="forget-form" action="index.html" method="post">
                <h3>Forget Password ?</h3>
                <p> Enter your e-mail address below to reset your password. </p>
                <div class="form-group">
                    <div class="input-icon">
                        <i class="fa fa-envelope"></i>
                        <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email" /> </div>
                </div>
                <div class="form-actions">
                    <button type="button" id="back-btn" class="btn red btn-outline">Back </button>
                    <button type="submit" class="btn green pull-right"> Submit </button>
                </div>
            </form> -->
            <!-- END FORGOT PASSWORD FORM -->
        </div>

        <!-- END LOGIN -->
        <!-- BEGIN COPYRIGHT -->
        <div class="copyright"> 2017 &copy; 3G CREATIVES TRAVEL AND TOURS </div>
        <!-- END COPYRIGHT -->
        <!--[if lt IE 9]>
<script src="{!! asset('global/plugins/respond.min.js') !!}"></script>
<script src="{!! asset('global/plugins/excanvas.min.js') !!}"></script>
<script src="{!! asset('global/plugins/ie8.fix.min.js') !!}"></script>
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="{!! asset('global/plugins/jquery.min.js') !!}" type="text/javascript"></script>
        <script src="{!! asset('global/plugins/bootstrap/js/bootstrap.min.js') !!}" type="text/javascript"></script>
        <script src="{!! asset('global/plugins/js.cookie.min.js') !!}" type="text/javascript"></script>
        <script src="{!! asset('global/plugins/jquery-slimscroll/jquery.slimscroll.min.js') !!}" type="text/javascript"></script>
        <script src="{!! asset('global/plugins/jquery.blockui.min.js') !!}" type="text/javascript"></script>
        <script src="{!! asset('global/plugins/bootstrap-switch/js/bootstrap-switch.min.js') !!}" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="{!! asset('global/plugins/jquery-validation/js/jquery.validate.min.js') !!}" type="text/javascript"></script>
        <script src="{!! asset('global/plugins/jquery-validation/js/additional-methods.min.js') !!}" type="text/javascript"></script>
        <script src="{!! asset('global/plugins/select2/js/select2.full.min.js') !!}" type="text/javascript"></script>
        <script src="{!! asset('global/plugins/backstretch/jquery.backstretch.min.js') !!}" type="text/javascript"></script>
        <script src="{!! asset('pages/scripts/login-4.js') !!}" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="{!! asset('global/scripts/app.min.js') !!}" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <script src="{!! asset('global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') !!}" type="text/javascript"></script>
        <script src="{!! asset('js/app.js') !!}"></script>
        @yield('scripts_per_module')
    </body>

</html>
