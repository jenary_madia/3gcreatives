<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{!! csrf_token() !!}" id="token">
    <meta name="base-url" content="{!! url('/') !!}" id="base_url">

    <title>3gcreatives travel and tours</title>

    <!-- Bootstrap core CSS -->
    <link href="{!! asset('landing-page-new/vendor/bootstrap/css/bootstrap.min.css') !!}" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="{!! asset('landing-page-new/vendor/font-awesome/css/font-awesome.min.css') !!}" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Roboto:300,400,500,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="{!! asset('landing-page-new/css/clean-blog.css') !!}" rel="stylesheet">
    <link href="{!! asset('landing-page-new/css/extra.css') !!}" rel="stylesheet">
    <style>
      .dropdown-submenu {
          position: relative;
      }

      .dropdown-submenu .dropdown-menu {
          top: 0;
          left: 100%;
          margin-top: -1px;
          padding: 10px;
      }
      </style>
    <style>

      /* Dropdown button on hover & focus */
      .dropbtn:hover, .dropbtn:focus {
      }

      /* The container <div> - needed to position the dropdown content */
      .dropdown {
          position: relative;
          display: inline-block;
      }

      /* Dropdown Content (Hidden by Default) */
      .dropdown-content {
          display: none;
          position: absolute;
          background-color: #f1f1f1;
          min-width: 160px;
          box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
          z-index: 1;
      }

      /* Links inside the dropdown */
      .dropdown-content a {
          color: black;
          padding: 12px 16px;
          text-decoration: none;
          display: block;
          font-size: 14px;
      }

      /* Change color of dropdown links on hover */
      .dropdown-content a:hover {background-color: #ddd}

      /* Show the dropdown menu (use JS to add this class to the .dropdown-content container when the user clicks on the dropdown button) */
      .show {display:block;}
    </style>
  </head>

  <body>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
      <div class="container">
        <a class="navbar-brand" href="{{ route('home') }}">
          <img src="{!! asset('images/logo-small.png') !!}" style="max-width: 150px;" class="img-responsive" alt="">
        </a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          Menu
          <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="{{ url('/') }}">Home</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown">Services
                <span class="caret"></span></a>
                <ul class="dropdown-menu" style="padding: 10px;">
                  <li><a tabindex="-1" href="{{ url('/visa-consultation') }}">Visa Consultation</a></li>
                  <li><a tabindex="-1" href="">Van Rental</a></li>
                  <li class="dropdown-submenu">
                    <a class="test" tabindex="-1" href="#">AirfareTicketing Office<span class="caret"></span></a>
                    <ul class="dropdown-menu sub-menu" style="margin-left: 9px; text-align: left;">
                      <li><a tabindex="-1" href="#">Domestic</a></li>
                      <li><a tabindex="-1" href="#">International </a></li>
                    </ul>
                  </li>
                  <li><a tabindex="-1" href="#">Tours Arrangement</a></li>
                  <li><a tabindex="-1" href="#">Hotel Accomodation</a></li>
                  <li><a tabindex="-1" href="#">Theme Parks</a></li>
                </ul>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ url('/about') }}">About Us</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ url('/perks-rewards') }}">Perks and Rewards</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ url('/faq') }}">FAQ</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ url('/contact') }}">Inquire</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ url('/business-opportunities') }}">Business Opportunity</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ url('/login') }}">Login</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <!-- Page Header -->
    <header class="masthead" style="background-image: url('../landing-page-new/img/home-bg.jpg')">
      <div class="overlay"></div>
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-10 mx-auto">
            <div class="site-heading">
              <h1>{{ $header }}</h1>
              <span class="subheading">{{ $subheader }}</span>
              <span class="subheading">
                <a href="{{ url('/contact') }}" class="btn btn-lg btn-primary">Inquire now</a>
              </span>
            </div>
          </div>
        </div>
      </div>
    </header>

    <!-- Main Content -->
    <div class="container">
      @yield('content')
    </div>

    <hr>

    <!-- Footer -->
    <footer>
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-10 mx-auto">
            <ul class="list-inline text-center">
              <!-- <li class="list-inline-item">
                <a href="#">
                  <span class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-stack-2x"></i>
                    <i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
                  </span>
                </a>
              </li> -->
              <li class="list-inline-item">
                <a target="_blank" href="https://www.facebook.com/trigtravelandtours/">
                  <span class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-stack-2x"></i>
                    <i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
                  </span>
                </a>
              </li>
              <li class="list-inline-item">
                <a target="_blank" href="https://www.instagram.com/trigcreativeslifestyle/">
                  <span class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-stack-2x"></i>
                    <i class="fa fa-instagram fa-stack-1x fa-inverse"></i>
                  </span>
                </a>
              </li>
            </ul>
            <p class="copyright text-muted">Copyright &copy; 3gcreatives Travel and Tours</p>
          </div>
        </div>
      </div>
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    <!-- Custom scripts for this template -->
    <script src="{!! asset('landing-page-new/js/clean-blog.min.js') !!}"></script>

    <!-- VueJS -->
    <script type="text/javascript" src="{!! asset('js/app.js') !!}"></script>

    <script>
      /* When the user clicks on the button, 
      toggle between hiding and showing the dropdown content */
      function myFunction() {
          document.getElementById("myDropdown").classList.toggle("show");
      }

      // Close the dropdown menu if the user clicks outside of it
      window.onclick = function(event) {
        if (!event.target.matches('.dropbtn')) {

          var dropdowns = document.getElementsByClassName("dropdown-content");
          var i;
          for (i = 0; i < dropdowns.length; i++) {
            var openDropdown = dropdowns[i];
            if (openDropdown.classList.contains('show')) {
              openDropdown.classList.remove('show');
            }
          }
        }
      }
    </script>
    <script>
      $(document).ready(function(){
        $('.dropdown-submenu a.test').on("click", function(e){
          $(".sub-menu").hide();
          $(this).next('ul').toggle();
          e.stopPropagation();
          e.preventDefault();
        });
        $( ".sub-menu" ).on( "mouseleave", function(e) {
            $(this).hide();
        });

        $('.sub-menu').on('mouseenter', function() {
              console.log('#foo is now visible');
        });
        // $( ".dropdown-menu" ).on( "click", function(e) {
        //     $(this).show();
        // });
      });


    </script>
    @yield('script_per_page')

  </body>

</html>
