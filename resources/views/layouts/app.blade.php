<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8" />
        <title>3GCREATIVE TRAVEL &amp; TOURS</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <link rel="apple-touch-icon-precomposed" sizes="57x57" href="{!! asset('apple-touch-icon-57x57.png') !!}" />
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{!! asset('apple-touch-icon-114x114.png') !!}" />
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{!! asset('apple-touch-icon-72x72.png') !!}" />
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{!! asset('apple-touch-icon-144x144.png') !!}" />
        <link rel="apple-touch-icon-precomposed" sizes="60x60" href="{!! asset('apple-touch-icon-60x60.png') !!}" />
        <link rel="apple-touch-icon-precomposed" sizes="120x120" href="{!! asset('apple-touch-icon-120x120.png') !!}" />
        <link rel="apple-touch-icon-precomposed" sizes="76x76" href="{!! asset('apple-touch-icon-76x76.png') !!}" />
        <link rel="apple-touch-icon-precomposed" sizes="152x152" href="{!! asset('apple-touch-icon-152x152.png') !!}" />
        <link rel="icon" type="image/png" href="{!! asset('favicon-196x196.png" sizes="196x196') !!}" />
        <link rel="icon" type="image/png" href="{!! asset('favicon-96x96.png" sizes="96x96') !!}" />
        <link rel="icon" type="image/png" href="{!! asset('favicon-32x32.png" sizes="32x32') !!}" />
        <link rel="icon" type="image/png" href="{!! asset('favicon-16x16.png" sizes="16x16') !!}" />
        <link rel="icon" type="image/png" href="{!! asset('favicon-128.png" sizes="128x128') !!}" />
        <meta name="application-name" content="&nbsp;"/>
        <meta name="msapplication-TileColor" content="#FFFFFF" />
        <meta name="msapplication-TileImage" content="{!! asset('mstile-144x144.png') !!}" />
        <meta name="msapplication-square70x70logo" content="{!! asset('mstile-70x70.png') !!}" />
        <meta name="msapplication-square150x150logo" content="{!! asset('mstile-150x150.png') !!}" />
        <meta name="msapplication-wide310x150logo" content="{!! asset('mstile-310x150.png') !!}" />
        <meta name="msapplication-square310x310logo" content="{!! asset('mstile-310x310.png') !!}" />
        <meta content="Preview page of Metronic Admin Theme #4 for blank page layout" name="description" />
        <meta name="csrf-token" content="{!! csrf_token() !!}" id="token">
        <meta name="base-url" content="{!! url('/') !!}" id="base_url">
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="{!! asset('global/plugins/font-awesome/css/font-awesome.min.css') !!}" rel="stylesheet" type="text/css" />
        <link href="{!! asset('global/plugins/simple-line-icons/simple-line-icons.min.css') !!}" rel="stylesheet" type="text/css" />
        <link href="{!! asset('global/plugins/bootstrap/css/bootstrap.min.css') !!}" rel="stylesheet" type="text/css" />
        <link href="{!! asset('global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') !!}" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="{!! asset('global/css/components.min.css') !!}" rel="stylesheet" id="style_components" type="text/css" />
        <link href="{!! asset('global/css/plugins.min.css') !!}" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="{!! asset('layouts/layout4/css/layout.min.css') !!}" rel="stylesheet" type="text/css" />
        <link href="{!! asset('layouts/layout4/css/themes/default.min.css') !!}" rel="stylesheet" type="text/css" id="style_color" />
        <link href="{!! asset('global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') !!}" rel="stylesheet" type="text/css" />
        <link href="{!! asset('layouts/layout4/css/custom.min.css') !!}" rel="stylesheet" type="text/css" />
        <link href="{!! asset('global/plugins/datatables/datatables.min.css') !!}" rel="stylesheet" type="text/css" />
        <link href="{!! asset('global/plugins/bootstrap-modal/css/bootstrap-modal.css') !!}" rel="stylesheet" type="text/css" />
        <link href="{!! asset('css/app.css') !!}" rel="stylesheet" type="text/css" />
        <link href="{!! asset('css/profile.min.css') !!}" rel="stylesheet" type="text/css" />
        <link href="{!! asset('global/utils/style.css') !!}" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" /> </head>
    <!-- END HEAD -->

    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
        <!-- BEGIN HEADER -->
        @include('partials.top-menu')
        <!-- END HEADER -->
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            @include('partials.sidebar')
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE BASE CONTENT -->
                    @yield('content')
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->

        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2017 &copy; 3G CREATIVES TRAVEL AND TOURS
                <!-- <a target="_blank" href="http://keenthemes.com">Keenthemes</a> &nbsp;|&nbsp; -->
                <!-- <a href="http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes" title="Purchase Metronic just for 27$ and get lifetime updates for free" target="_blank">Purchase Metronic!</a> -->
            </div>
            <div class="scroll-to-top">
                <i class="icon-arrow-up"></i>
            </div>
        </div>
        <!-- END FOOTER -->

        <!--[if lt IE 9]>
        <script src="{!! asset('global/plugins/respond.min.js') !!}"></script>
        <script src="{!! asset('global/plugins/excanvas.min.js') !!}"></script>
        <script src="{!! asset('global/plugins/ie8.fix.min.js') !!}"></script>
        <![endif]-->
        <!-- BEGIN CORE PLUGINS -->
       <!-- BEGIN CORE PLUGINS -->
        <script src="{!! asset('global/plugins/jquery.min.js') !!}" type="text/javascript"></script>
        <script src="{!! asset('global/plugins/bootstrap/js/bootstrap.min.js') !!}" type="text/javascript"></script>
        <script src="{!! asset('global/plugins/js.cookie.min.js') !!}" type="text/javascript"></script>
        <script src="{!! asset('global/plugins/jquery-slimscroll/jquery.slimscroll.min.js') !!}" type="text/javascript"></script>
        <script src="{!! asset('global/plugins/jquery.blockui.min.js') !!}" type="text/javascript"></script>
        <script src="{!! asset('global/plugins/bootstrap-switch/js/bootstrap-switch.min.js') !!}" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="{!! asset('global/plugins/moment.min.js') !!}" type="text/javascript"></script>
        <script src="{!! asset('global/plugins/bootstrap-daterangepicker/daterangepicker.min.js') !!}" type="text/javascript"></script>
        <script src="{!! asset('global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') !!}" type="text/javascript"></script>
        <script src="{!! asset('global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js') !!}" type="text/javascript"></script>
        <script src="{!! asset('global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') !!}" type="text/javascript"></script>
        <script src="{!! asset('global/plugins/clockface/js/clockface.js') !!}" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="{!! asset('global/scripts/app.min.js') !!}" type="text/javascript"></script>
        <script src="{!! asset('layouts/layout4/scripts/layout.min.js') !!}" type="text/javascript"></script>
        <script src="{!! asset('layouts/layout4/scripts/demo.min.js') !!}" type="text/javascript"></script>
        <script src="{!! asset('layouts/global/scripts/quick-sidebar.min.js') !!}" type="text/javascript"></script>
        <script src="{!! asset('layouts/global/scripts/quick-nav.min.js') !!}" type="text/javascript"></script>


        <script src="{!! asset('global/scripts/datatable.js') !!}" type="text/javascript"></script>
        <script src="{!! asset('global/plugins/datatables/datatables.min.js') !!}" type="text/javascript"></script>
        <script src="{!! asset('global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') !!}" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->
        <script src="{!! asset('js/app.js') !!}"></script>
        @yield('script_per_module')
    </body>

</html>
