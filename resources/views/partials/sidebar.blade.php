<div class="page-sidebar-wrapper">
    <!-- BEGIN SIDEBAR -->
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
        <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
        <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
        <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
        <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
        <ul class="page-sidebar-menu   " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
            <li class="nav-item start">
                <li class="nav-item start {{ set_active('dashboard') }}">
                    <a href="{{ url('/dashboard') }}" class="nav-link ">
                        <i class="icon-home"></i>
                        <span class="title">Dashboard</span>
                    </a>
                </li>

                @if($role_id == 1)
                <li class="nav-item start {{ set_active('transaction/list/all') }}">
                    <a href="{{ url('/transaction/list/all') }}" class="nav-link ">
                        <i class="fa fa-book"></i>
                        <span class="title">Transactions</span>
                    </a>
                </li>

                <li class="nav-item start {{ set_active('user/list') }}">
                    <a href="{{ url('/user/list') }}" class="nav-link ">
                        <i class="fa fa-users"></i>
                        <span class="title">User List</span>
                    </a>
                </li>

                <li class="nav-item start {{ set_active('client/list') }}">
                    <a href="{{ url('/client/list') }}" class="nav-link ">
                        <i class="fa fa-briefcase"></i>
                        <span class="title">Client List</span>
                    </a>
                </li>
                @endif

                <li class="nav-item start {{ set_active('transaction/list') }}">
                    <a href="{{ url('/transaction/list') }}" class="nav-link ">
                        <i class="fa fa-history"></i>
                        <span class="title">Transaction History</span>
                    </a>
                </li>
            </li>
        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
</div>
