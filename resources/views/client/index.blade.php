@extends('layouts.app')

@section('content')
<div class="col-md-12" id="client-container">
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>Transactions
            </h1>
        </div>
        <!-- END PAGE TITLE -->
    </div>
    <!-- END PAGE HEAD-->
    <!-- BEGIN PAGE BREADCRUMB -->
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="index.html">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span class="active">Transaction List</span>
        </li>
    </ul>
    <!-- BEGIN SAMPLE TABLE PORTLET-->
    <div class="portlet box green">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs"></i>
                Clients
            </div>
        </div>

        <div class="portlet-body flip-scroll">
            <table class="table table-striped table-bordered table-hover table-checkable order-column dataTable no-footer" id="client-list" role="grid" aria-describedby="sample_1_info">
                <thead>
                    <tr role="row">
                        <th class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" aria-label=" Status : activate to sort column ascending" style="width: 300px;">
                            Client Name
                        </th>
                        <th class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" aria-label=" Status : activate to sort column ascending" style="width: 100px;">
                            Card Number
                        </th>
                        <th class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" aria-label=" Actions : activate to sort column ascending" style="width: 50px;">
                            Total Points
                        </th>
                        <th class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" aria-label=" Actions : activate to sort column ascending" style="width: 165px;">
                            Date Created
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($clients as $client)
                        <tr class="gradeX odd" role="row">
                            <td>
                                {{ $client->full_name }}
                            </td>
                            <td>
                                {{ $client->card_no }}
                            </td>
                            <td>
                                {{ $client->total_points }}
                            </td>
                            <td>
                                {{ $client->created_at }}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
@section('script_per_module')
    <script src="{!! asset('js/client-app.js') !!}"></script>
@endsection
