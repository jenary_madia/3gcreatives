<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class TransactionTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('transaction_types')->insert([
            [
                'name' => 'Van Rental',
                'legend' => 'V',
                'corresponding_points' => 3,
                'additional_points' => 0,
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
            ],
            [
                'name' => 'Hotel Booking',
                'legend' => 'H',
                'corresponding_points' => 3,
                'additional_points' => 0,
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
            ],
            [
                'name' => 'Airfare',
                'legend' => 'A',
                'corresponding_points' => 5,
                'additional_points' => 0,
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
            ],
            [
                'name' => 'Domestic Tour',
                'legend' => 'D',
                'corresponding_points' => 7,
                'additional_points' => 3,
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
            ],
            [
                'name' => 'International Tour',
                'legend' => 'I',
                'corresponding_points' => 10,
                'additional_points' => 3,
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
            ],
            [
                'name' => 'International Complete',
                'legend' => 'IC',
                'corresponding_points' => 15,
                'additional_points' => 3,
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
            ],
            [
                'name' => 'Domestic Complete',
                'legend' => 'DC',
                'corresponding_points' => 12,
                'additional_points' => 3,
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
            ],
        ]);
    }
}
