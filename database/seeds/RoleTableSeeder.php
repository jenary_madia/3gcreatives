<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            [
                "type" => "Admin",
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
            ],
            [
                "type" => "Employee",
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
            ],
            [
                "type" => "Agent",
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
            ],
            [
                "type" => "Client",
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
            ]
        ]);
    }
}
