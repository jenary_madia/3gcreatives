<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $personal_info_id = DB::table('users_personal_info')->insertGetId([
            "first_name" => "Admin",
            "middle_name" => "",
            "last_name" => "Super",
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now()
        ]);

        DB::table('users')->insert([
            'role_id' => 1,
            'email' => 'trigtravelandtoursagency@gmail.com',
            'users_personal_info_id' => $personal_info_id,
            'password' => bcrypt('admin2314'),
            'confirmed' => 1,
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now()
        ]);
    }
}
