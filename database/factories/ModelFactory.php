<?php

use Faker\Generator as Faker;

$factory->define(App\PrestigeCard::class, function (Faker $faker) {
    return [
        'card_no' => $faker->numerify('####-####-####'),
        'status' => 0,
    ];
});
