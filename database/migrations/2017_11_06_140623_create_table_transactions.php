<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('transaction_type_id');
            $table->string('reference_no');
            $table->integer('client_id');
            $table->integer('agent_id')->nullable();
            $table->tinyInteger('multiple_day');
            $table->date('date_from');
            $table->date('date_to')->nullable();
            $table->string('airline_name')->nullable();
            $table->string('hotel_name')->nullable();
            $table->string('destination');
            $table->decimal('total_price',10,2);
            $table->integer('total_points');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
